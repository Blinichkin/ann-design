/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networktraining;

import ann.training.TrainingSettings;
import cann.other.Tools;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.border.Border;

/**
 * Диалоговое окно настроек обучения нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class TrainingSettingsDialog extends JDialog {
	
	//<editor-fold desc="Конструкторы">

	/**
	 * Создает стандартное диалоговое окно.
	 */
	public TrainingSettingsDialog() {
		this(new TrainingSettings(), (Frame) null, false);
	}

	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param trainingSettings Настройки обучения
	 */
	public TrainingSettingsDialog(TrainingSettings trainingSettings) {
		this(trainingSettings, (Frame) null, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param trainingSettings Настройки обучения
	 * @param owner Владелец
	 */
	public TrainingSettingsDialog(TrainingSettings trainingSettings, Frame owner) {
		this(trainingSettings, owner, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param trainingSettings Настройки обучения
	 * @param owner Владелец
	 * @param modal Модальность
	 */
	public TrainingSettingsDialog(TrainingSettings trainingSettings, Frame owner, boolean modal) {
		this(trainingSettings, owner, "Настройки обучения", modal);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param trainingSettings Настройки обучения
	 * @param owner Владелец
	 * @param title Заголовок
	 */
	public TrainingSettingsDialog(TrainingSettings trainingSettings, Frame owner, String title) {
		this(trainingSettings, owner, title, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param trainingSettings Настройки обучения
	 * @param owner Владелец
	 * @param title Заголовок
	 * @param modal Модальность
	 */
	public TrainingSettingsDialog(TrainingSettings trainingSettings, Frame owner, String title, 
			boolean modal) {
		super(owner, title, modal, null);
		this.trainingSettings = trainingSettings;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		pnlTrainingSettings = new JPanel();
		visualTrainingSettings = new VisualTrainingSettings();
		
		pnlBottom = new JPanel();
		btnOk = new JButton();
		btnCancel = new JButton();
		btnReset = new JButton();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		// Настройка pnlTrainingSettings
		{
			Border titleBorder = BorderFactory.createTitledBorder("Настройки обучения сети");
			Border emptyBorder = BorderFactory.createEmptyBorder(10, 10, 0, 10);
			Border border = BorderFactory.createCompoundBorder(emptyBorder, titleBorder);
			
			pnlTrainingSettings.setBorder(border);
		}
		
		// Настройка btnOk
		btnOk.setText("OK");
		
		// Настройка btnCancel
		btnCancel.setText("Отмена");
		
		// Настройка btnReset
		btnReset.setText("Сброс");
		
		// Настройка pnlBottom
		{
			Border border = BorderFactory.createEmptyBorder(5, 5, 5, 5);
			pnlBottom.setBorder(border);
		}
		
		// Настройка диалогового окна
		{
			setIconImage(tools.getIcon("settings.png").getImage());
			setSize(500, 250);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnOk.addActionListener(this::btnOkActionPerformed);
		btnCancel.addActionListener(this::btnCancelActionPerformed);
		btnReset.addActionListener(this::btnResetActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		//Настройка pnlTrainingSettings
		{
			pnlTrainingSettings.setLayout(new BorderLayout());
			
			pnlTrainingSettings.add(visualTrainingSettings, BorderLayout.CENTER);
		}
		
		//Настройка pnlBottom
		{
			pnlBottom.setLayout(new GridBagLayout());
			Insets insets = new Insets(1, 1, 1, 1);
		
			pnlBottom.add(btnOk, new GridBagConstraints(
				1, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
		
			pnlBottom.add(btnCancel, new GridBagConstraints(
				2, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
		
			pnlBottom.add(btnReset, new GridBagConstraints(
				3, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
		}
		
		//Настройка диалогового окна
		{
			setLayout(new BorderLayout());
			
			add(pnlTrainingSettings, BorderLayout.CENTER);
			add(pnlBottom, BorderLayout.SOUTH);
		}
		
		setLocationRelativeTo(null);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет диалоговое окно.
	 */
	private void update() {
		updateVisualTraningSettings();
	}
	
	/**
	 * Обновляет visualTrainingSettings.
	 */
	private void updateVisualTraningSettings() {
		visualTrainingSettings.setTrainingSettings(trainingSettings);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnOkActionPerformed(ActionEvent e) {
		Save();
		dialogResult = true;
		dispose();
	}
	
	private void btnCancelActionPerformed(ActionEvent e) {
		dispose();
	}
	
	private void btnResetActionPerformed(ActionEvent e) {
		trainingSettings = new TrainingSettings();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Сохранение настроек.
	 */
	private void Save() {
		trainingSettings = visualTrainingSettings.getTrainingSettings();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JPanel pnlTrainingSettings;
	private VisualTrainingSettings visualTrainingSettings;

	private JPanel pnlBottom;
	private JButton btnOk;
	private JButton btnReset;
	private JButton btnCancel;
	
	private boolean dialogResult;
	private TrainingSettings trainingSettings;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущие настройки обучения.
	 * 
	 * @return Настройки обучения
	 */
	public TrainingSettings getTrainingSettings() {
		return trainingSettings;
	}
	
	/**
	 * Устанавливает новые настройки обучения.
	 * 
	 * @param trainingSettings Настройки обучения
	 */
	public void setTrainingSettings(TrainingSettings trainingSettings) {
		this.trainingSettings = trainingSettings;
	}
	
	/**
	 * Возвращает результат диалогового окна.
	 * 
	 * @return Результат
	 */
	public boolean isDialogResult() {
		return dialogResult;
	}
	
	//</editor-fold>
	
}
