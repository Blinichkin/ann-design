/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networktraining;

/**
 * Компонент вывода весовых коэффициентов однослойного персептрона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class OutputWeightsSingleLayerPerceptron extends OutputWeights {

	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public OutputWeightsSingleLayerPerceptron() {
		this(null);
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param w Весовые коэффициенты
	 */
	public OutputWeightsSingleLayerPerceptron(double[][] w) {
		super();
		this.w = w;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	@Override
	public void update() {
		updateTblData();
	}
	
	/**
	 * Обновляет tblData.
	 */
	private void updateTblData() {
		if (w == null) return;
		
		updateData();
		tableModel.setDataVector(data, headers);
	}
	
	/**
	 * Обновляет данные.
	 */
	private void updateData() {
		int rowCount = w.length;
		int colCount = 2;
		data = new Object[rowCount][colCount];
		
		int index = 0;
		for (int i = 0; i < w.length; i++) {
			for (int j = 0; j < w[i].length; j++) {
				data[index][0] = "W[ " + i + " ][ " + j + " ]";
				data[index][1] = w[i][j];
				index++;
			}
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private double[][] w;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение весовых коэффициентов.
	 * 
	 * @return Весовые коэффициенты
	 */
	public double[][] getW() {
		return w;
	}

	/**
	 * Устанавливает новое значение весовых коэффициентов.
	 * 
	 * @param w Весовые коэффициенты
	 */
	public void setW(double[][] w) {
		this.w = w;
	}
	
	//</editor-fold>
	
}
