/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networktraining;

import ann.structure.NetworkType;
import java.awt.BorderLayout;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Абстрактный компонент вывода весовых коэффициентов.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public abstract class OutputWeights extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public OutputWeights() {
		init();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		tblData = new JTable();
		tableModel = new DefaultTableModel();
		headers = new Object[] { "Элемент", "Значение" };
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка tblData
		{
			tblData.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			tblData.setModel(tableModel);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка компонента
		{
			setLayout(new BorderLayout());
			add(new JScrollPane(tblData), BorderLayout.CENTER);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	public abstract void update();
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Получает компонент вывода весовых коэффициентов в зависимости от типа сети.
	 * 
	 * @param networkType Тип сети
	 * @return Компонент вывода весовых коэффициентов
	 */
	public static OutputWeights getWeights(NetworkType networkType) {
		switch (networkType) {
			case SingleLayerPerceptron:
				return new OutputWeightsSingleLayerPerceptron();
			case MultiLayerPerceptron: 
				return new OutputWeightsMultiLayerPerceptron();
			default:
				return null;
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JTable tblData;
	protected DefaultTableModel tableModel;
	protected Object[][] data;
	protected Object[] headers;
	
	//</editor-fold>
	
}
