/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networktraining;

import ann.training.TrainingSettings;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;

/**
 * Компонент для визуального изменения настроек обучения.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class VisualTrainingSettings extends JComponent {

	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public VisualTrainingSettings() {
		this(new TrainingSettings());
	}

	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param trainingSettings Настройки обучения
	 */
	public VisualTrainingSettings(TrainingSettings trainingSettings) {
		this.trainingSettings = trainingSettings;
		
		init();
		update();
	}
	
	//</editor-fold>
	
		//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		initComponents();
		settingsComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		lblA = new JLabel();
		spnA = new JSpinner();
		lblError = new JLabel();
		spnError = new JSpinner();
		lblNumberEpochs = new JLabel();
		spnNumberEpochs = new JSpinner();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingsComponents() {
		// Настройка lblA
		lblA.setText("<html>Скорость:</html>");
		
		// Настройка spnA
		{
			double value = trainingSettings.getA().getValue();
			double minValue = trainingSettings.getA().getMinValue();
			double maxValue = trainingSettings.getA().getMaxValue();
			double stepSize = 0.1;
			SpinnerNumberModel model = new SpinnerNumberModel(value, minValue, maxValue, stepSize);
			
			spnA.setModel(model);
		}
		
		// Настройка lblError
		lblError.setText("<html>Допустимая ошибка:</html>");
		
		// Настройка spnError
		{
			double value = trainingSettings.getError().getValue();
			double minValue = trainingSettings.getError().getMinValue();
			double maxValue = trainingSettings.getError().getMaxValue();
			double stepSize = 0.001;
			SpinnerNumberModel model = new SpinnerNumberModel(value, minValue, maxValue, stepSize);
			
			spnError.setModel(model);
		}
		
		// Настройка lblNumberEpochs
		lblNumberEpochs.setText("<html>Количество эпох:</html>");
		
		// Настройка spnNumberEpochs
		{
			int value = trainingSettings.getNumberEpochs().getValue();
			int minValue = trainingSettings.getNumberEpochs().getMinValue();
			int maxValue = trainingSettings.getNumberEpochs().getMaxValue();
			int stepSize = 1000;
			SpinnerNumberModel model = new SpinnerNumberModel(value, minValue, maxValue, stepSize);
			
			spnNumberEpochs.setModel(model);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		spnA.addChangeListener(this::spnAStateChanged);
		spnError.addChangeListener(this::spnErrorStateChanged);
		spnNumberEpochs.addChangeListener(this::spnNumberEpochsStateChanged);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		setLayout(new GridBagLayout());
		Insets insets = new Insets(2, 2, 2, 2);
		
		add(lblA, new GridBagConstraints(
			0, 0, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnA, new GridBagConstraints(
			1, 0, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblError, new GridBagConstraints(
			0, 1, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnError, new GridBagConstraints(
			1, 1, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblNumberEpochs, new GridBagConstraints(
			0, 2, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnNumberEpochs, new GridBagConstraints(
			1, 2, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateSpnA();
		updateSpnError();
		updateSpnNumberEpochs();
	}
	
	/**
	 * Обновляет spnA.
	 */
	private void updateSpnA() {
		double value = trainingSettings.getA().getValue();
		spnA.setValue(value);
	}
	
	/**
	 * Обновляет spnError.
	 */
	private void updateSpnError() {
		double value = trainingSettings.getError().getValue();
		spnError.setValue(value);
	}
	
	/**
	 * Обновляет spnNumberEpochs.
	 */
	private void updateSpnNumberEpochs() {
		int value = trainingSettings.getNumberEpochs().getValue();
		spnNumberEpochs.setValue(value);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void spnAStateChanged(ChangeEvent e) {
		double value = (double)spnA.getValue();
		trainingSettings.setA(value);
	}
	
	private void spnErrorStateChanged(ChangeEvent e) {
		double value = (double)spnError.getValue();
		trainingSettings.setError(value);
	}
	
	private void spnNumberEpochsStateChanged(ChangeEvent e) {
		int value = (int)spnNumberEpochs.getValue();
		trainingSettings.setNumberEpochs(value);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private TrainingSettings trainingSettings;
	
	private JLabel lblA;
	private JSpinner spnA;
	private JLabel lblError;
	private JSpinner spnError;
	private JLabel lblNumberEpochs;
	private JSpinner spnNumberEpochs;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение настроек обучения.
	 * 
	 * @return Настройки обучения
	 */
	public TrainingSettings getTrainingSettings() {
		return trainingSettings;
	}
	
	/**
	 * Устанавливает новое значение настроек обучения.
	 * 
	 * @param trainingSettings Настройки обучения
	 */
	public void setTrainingSettings(TrainingSettings trainingSettings) {
		this.trainingSettings = trainingSettings;
		
		update();
	}
	
	//</editor-fold>
	
}
