/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import ann.*;
import ann.structure.TypeData;
import cann.data.*;
import cann.dataupload.IODataUnload;
import java.awt.BorderLayout;
import java.awt.event.*;
import jhelper.ArrayConverter;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Компонент для тестирования нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNCalc extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNCalc() {
		this(new SingleLayerPerceptron(), new DataStore());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 * @param dataStore Хранилище данных
	 */
	public CANNCalc(NeuralNetwork neuralNetwork, DataStore dataStore) {
		this.neuralNetwork = neuralNetwork;
		this.dataStore = dataStore;
		
		init();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		controlPanel = new CANNControlPanel();
		dataUnload = new IODataUnload();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка controlPanel
		{
			Border border = new EmptyBorder(0, 5, 0, 5);
			
			controlPanel.setBorder(border);
		}
		
		// Настройка dataUnload
		{
			Border border = new EmptyBorder(5, 5, 5, 5);
			
			dataUnload.setBorder(border);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		controlPanel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switch (e.getActionCommand()) {
					case "Play":
						start();
						break;
					case "Stop":
						stop();
						break;
					case "Settings":
						JOptionPane.showMessageDialog(null, 
								"Настройки для данного типа сети отсутствуют", "Сообщение", 
								JOptionPane.INFORMATION_MESSAGE);
						break;
				}
			}
		});
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		setLayout(new BorderLayout());
		
		add(controlPanel, BorderLayout.NORTH);
		add(dataUnload, BorderLayout.CENTER);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Запуск тестирования.
	 */
	private void start() {
		controlPanel.setIndeterminate(true);
		worker = new SwingWorker<Void, Integer>() {
			@Override
			protected Void doInBackground() throws Exception {
				calc();
				
				return null;
			}
			
			@Override
			protected void done() {
				controlPanel.setIndeterminate(false);
			}
		};
		
		worker.execute();
	}
	
	/**
	 * Выполняет вычисление.
	 */
	private void calc() {
		DataStore dataStore = dataUnload.getDataStore();
		DataTable dataTable = dataStore.getInputData();
		Object[][] inputData = dataTable.getData();
				
		switch (neuralNetwork.getNetworkType()) {
			case HopfieldNetwork:
				initHopfield();
				break;
			case HammingNetwork:
				initHamming();
				break;
			case KohonenNetwork:
				initKohonen();
				break;
		}
				
		Object[][] outputData = neuralNetwork.calc(inputData);
		DataTable newDataTable = new DataTable(TypeData.Binary, new String[outputData[0].length], 
				outputData);
		dataStore.setOutputData(newDataTable);
		dataUnload.setDataStore(dataStore);
	}
	
	/**
	 * Прерывает вычисление.
	 */
	private void stop() {
		worker.cancel(true);
	}
	
	/**
	 * Инициализация сети Хопфилда.
	 */
	private void initHopfield() {
		HopfieldNetwork hopfieldNetwork = (HopfieldNetwork) neuralNetwork;
		DataTable dataTable = dataStore.getInputData();
		int[][] data = ArrayConverter.objectToInt(dataTable.getData());
		hopfieldNetwork.initWeights(data);
	}
	
	/**
	 * Инициализация сети Хэмминга.
	 */
	private void initHamming() {
		HammingNetwork hammingNetwork = (HammingNetwork) neuralNetwork;
		DataTable dataTable = dataStore.getInputData();
		int[][] data = ArrayConverter.objectToInt(dataTable.getData());
		hammingNetwork.initWeights(data);
	}
	
	/**
	 * Инициализация сети Кохонена.
	 */
	private void initKohonen() {
		KohonenNetwork kohonenNetwork = (KohonenNetwork) neuralNetwork;
		kohonenNetwork.initWeights();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private NeuralNetwork neuralNetwork;
	private DataStore dataStore;
	private SwingWorker<Void, Integer> worker;
	
	private CANNControlPanel controlPanel;
	private IODataUnload dataUnload;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущую нейронную сеть.
	 * 
	 * @return Нейронная сеть
	 */
	public NeuralNetwork getNeuralNetwork() {
		return neuralNetwork;
	}
	
	/**
	 * Устанавливает новую нейронную сеть.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 */
	public void setNeuralNetwork(NeuralNetwork neuralNetwork) {
		this.neuralNetwork = neuralNetwork;
	}
	
	/**
	 * Получает текущее хранилище данных.
	 * 
	 * @return Хранилище данных
	 */
	public DataStore getDataStore() {
		return dataStore;
	}

	/**
	 * Устанавливает новое хранилище данных.
	 * 
	 * @param dataStore Хранилище данных
	 */
	public void setDataStore(DataStore dataStore) {
		this.dataStore = dataStore;
	}
	
	//</editor-fold>
	
}
