/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.data;

import ann.structure.TypeData;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

/**
 * Таблица данных для нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class DataTable implements Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает пустую таблицу данных.
	 */
	public DataTable() {
		this(TypeData.Analog, null, null);
	}
	
	/**
	 * Создает таблицу данных используя полученные значения.
	 * 
	 * @param typeData Тип данных
	 * @param headers Заголовки
	 * @param data Данные
	 */
	public DataTable(TypeData typeData, String[] headers, Object[][] data) {
		this.typeData = typeData;
		this.headers = headers;
		this.data = data;
	}
	
	/**
	 * Создает таблицу данных используя полученные значения.
	 * 
	 * @param dataTable Таблица данных
	 */
	public DataTable(DataTable dataTable) {
		this(dataTable.getTypeData(), dataTable.getHeaders(), dataTable.getData());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		DataTable dataTable = (DataTable) super.clone();
		
		dataTable.setHeaders(headers.clone());
		dataTable.setData(data.clone());
		
		return dataTable;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		DataTable dataTable = (DataTable) object;
		
		return typeData == dataTable.getTypeData()
				&& Objects.equals(headers, dataTable.getHeaders())
				&& Objects.equals(data, dataTable.getData());
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(typeData, headers, data);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName()+ "[" 
				+ typeData.toString() + ","
				+ Arrays.toString(headers) + "," 
				+ Arrays.toString(data) + "]";
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private TypeData typeData;
	private String[] headers;
	private Object[][] data;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение типа данных.
	 * 
	 * @return Тип данных
	 */
	public TypeData getTypeData() {
		return typeData;
	}
	
	/**
	 * Устанавливает новое значение типа данных.
	 * 
	 * @param typeData Тип данных
	 */
	public void setTypeData(TypeData typeData) {
		this.typeData = typeData;
	}
	
	/**
	 * Получает текущий массив заголовков. 
	 * 
	 * @return Заголовки
	 */
	public String[] getHeaders() {
		return headers;
	}
	
	/**
	 * Устанавливает новый массив заголовков.
	 * 
	 * @param headers Заголовки
	 */
	public void setHeaders(String[] headers) {
		this.headers = headers;
	}

	/**
	 * Получает текущие данные таблицы.
	 * 
	 * @return Данные
	 */
	public Object[][] getData() {
		return data;
	}

	/**
	 * Устанавливает новые данные таблицы.
	 * 
	 * @param data Данные
	 */
	public void setData(Object[][] data) {
		this.data = data;
	}
	
	/**
	 * Получает текущее значение количества колонок таблицы.
	 * 
	 * @return Количество колонок
	 */
	public int getColumnCount() {
		if (data == null || data.length == 0)
			return 0;
		
		return data[0].length;
	}
	
	/**
	 * Получает текущее значение количества строк таблицы.
	 * 
	 * @return Количество строк
	 */
	public int getRowCount() {
		if (data == null)
			return 0;
		
		return data.length;
	}
	
	//</editor-fold>

}
