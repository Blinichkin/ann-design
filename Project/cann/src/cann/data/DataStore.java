/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.data;

import java.util.Objects;

/**
 * Класс для хранения данных.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class DataStore {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает пустое хранилище данных.
	 */
	public DataStore() {
		this(new DataTable(), new DataTable());
	}
	
	/**
	 * Создает хранилище данных используя полученные значения.
	 * 
	 * @param inputData Таблица входных данных
	 * @param outputData Таблица выходных данных
	 */
	public DataStore(DataTable inputData, DataTable outputData) {
		this.inputData = inputData;
		this.outputData = outputData;
	}
	
	/**
	 * Создает хранилище данных используя полученные значения.
	 * 
	 * @param dataStore Хранилище данных
	 */
	public DataStore(DataStore dataStore) {
		inputData = dataStore.getInputData();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		DataStore dataStore = (DataStore) super.clone();
		
		dataStore.setInputData((DataTable) inputData.clone());
		dataStore.setOutputData((DataTable) outputData.clone());
		
		return dataStore;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		DataStore dataStore = (DataStore) object;
		
		return Objects.equals(inputData, dataStore.getInputData())
				&& Objects.equals(outputData, dataStore.getOutputData());
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(inputData, outputData);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName()+ "[" 
				+ inputData.toString() + "," 
				+ outputData.toString() + "]";
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private DataTable inputData;
	private DataTable outputData;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущую таблицу входных данных.
	 * 
	 * @return Таблица данных
	 */
	public DataTable getInputData() {
		return inputData;
	}
	
	/**
	 * Устанавливает новую таблицу входных данных.
	 * 
	 * @param inputData Таблица данных
	 */
	public void setInputData(DataTable inputData) {
		this.inputData = inputData;
	}
	
	/**
	 * Получает текущую таблицу выходных данных.
	 * 
	 * @return Таблица данных
	 */
	public DataTable getOutputData() {
		return outputData;
	}
	
	/**
	 * Устанавливает новую таблицу выходных данных.
	 * 
	 * @param outputData Таблица данных
	 */
	public void setOutputData(DataTable outputData) {
		this.outputData = outputData;
	}
	
	//</editor-fold>
	
}
