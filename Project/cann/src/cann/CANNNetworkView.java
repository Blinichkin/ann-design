/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import cann.networkview.ICANNNetworkView;
import ann.*;
import cann.networkview.*;
import cann.networkview.frame.NetworkFrame;
import cann.other.Tools;
import cann.paintnetwork.SettingsDialog;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.logging.*;
import javax.swing.Box.Filler;
import javax.swing.*;

/**
 * Компонент для визуального представления структуры нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNNetworkView extends JComponent implements ICANNNetworkView {

	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNNetworkView() {
		this(new SingleLayerPerceptron());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 */
	public CANNNetworkView(NeuralNetwork neuralNetwork) {
		this.neuralNetwork = neuralNetwork;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	public void initComponents() {
		btnUpdate = new JButton();
		btnSettings = new JButton();
		tbTools = new JToolBar();
		
		btnCenter = new JButton();
		btnUp = new JButton();
		btnDown = new JButton();
		btnLeft = new JButton();
		btnRight = new JButton();
		lblSize = new JLabel();
		cobSize = new JComboBox();
		tbMove = new JToolBar();
		
		paintNetwork = new CANNPaintNetwork();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		// Настройка btnUpdate
		{
			btnUpdate.setText("Обновить");
			btnUpdate.setToolTipText("Обновляет изображение сети");
		}
		
		// Настройка btnSettings
		{
			btnSettings.setIcon(tools.getIcon("settings.png"));
			btnSettings.setToolTipText("Открывает окно с настройками отображения");
		}
		
		// Настройка tbTools
		tbTools.setName("Инструменты");
		
		// Настройка btnCenter
		{
			btnCenter.setIcon(tools.getIcon("center.png"));
			btnCenter.setToolTipText("Перемещает в центр изображения");
		}
		
		// Настройка btnUp
		{
			btnUp.setIcon(tools.getIcon("up.png"));
			btnUp.setToolTipText("Перемещает вверх");
		}
		
		// Настройка btnDown
		{
			btnDown.setIcon(tools.getIcon("down.png"));
			btnDown.setToolTipText("Перемещает вниз");
		}
		
		// Настройка btnLeft
		{
			btnLeft.setIcon(tools.getIcon("left.png"));
			btnLeft.setToolTipText("Перемещает влево");
		}
		
		// Настройка btnRight
		{
			btnRight.setIcon(tools.getIcon("right.png"));
			btnRight.setToolTipText("Перемещает вправо");
		}
		
		// Настройка lblSize
		lblSize.setText("Масштаб: ");
		
		// Настройка cobSize
		{
			for (SizeType networkType: SizeType.values())
				cobSize.addItem(networkType.getName());
		}
		
		// Настройка tbMove
		tbMove.setName("Перемещение");
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnUpdate.addActionListener(this::btnUpdateActionPerformed);
		btnSettings.addActionListener(this::btnSettingsActionPerformed);
		btnCenter.addActionListener(this::btnCenterActionPerformed);
		btnUp.addActionListener(this::btnUpActionPerformed);
		btnDown.addActionListener(this::btnDownActionPerformed);
		btnLeft.addActionListener(this::btnLeftActionPerformed);
		btnRight.addActionListener(this::btnRightActionPerformed);
		cobSize.addActionListener(this::cobSizeActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка tbTools
		{
			tbTools.add(btnUpdate);
			tbTools.addSeparator();
			tbTools.add(btnSettings);
		}
		
		// Настройка tbMove
		{
			tbMove.add(btnCenter);
			tbMove.addSeparator();
			tbMove.add(btnUp);
			tbMove.add(btnDown);
			tbMove.add(btnLeft);
			tbMove.add(btnRight);
		
			Filler filler = new Filler(
				new Dimension(0, 0), 
				new Dimension(500, 0), 
				new Dimension(Integer.MAX_VALUE, 0)
			);
			tbMove.add(filler);
			tbMove.add(lblSize);
			tbMove.add(cobSize);
		}
		
		// Настройка компонента
		{
			setLayout(new BorderLayout());
			
			add(tbTools, BorderLayout.NORTH);
			add(tbMove, BorderLayout.SOUTH);
			add(paintNetwork, BorderLayout.CENTER);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnUpdateActionPerformed(ActionEvent e) {
		paintNetwork.repaint();
	}
	
	private void btnSettingsActionPerformed(ActionEvent e) {
		openSettings();
	}
	
	private void btnCenterActionPerformed(ActionEvent e) {
		paintNetwork.moveCenter();
	}
	
	private void btnUpActionPerformed(ActionEvent e) {
		paintNetwork.moveUp();
	}
	
	private void btnDownActionPerformed(ActionEvent e) {
		paintNetwork.moveDown();
	}
	
	private void btnLeftActionPerformed(ActionEvent e) {
		paintNetwork.moveLeft();
	}
	
	private void btnRightActionPerformed(ActionEvent e) {
		paintNetwork.moveRight();
	}
	
	private void cobSizeActionPerformed(ActionEvent e) {
		SizeType size = SizeType.values()[cobSize.getSelectedIndex()]; 
		paintNetwork.getNetworkFrame().getSettings().setSizeType(size);
		paintNetwork.repaint();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateNetworkView();
	}
	
	/**
	 * Обновляет networkView.
	 */
	private void updateNetworkView() {
		NetworkFrame networkFrame = NetworkFrame.getNetworkFrame(neuralNetwork.getNetworkType());
		networkFrame.setNetworkModel(neuralNetwork.getNetworkModel());
		paintNetwork.setNetworkFrame(networkFrame);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Открывает диалоговое окно настроек.
	 */
	private void openSettings() {
		Settings settings = new Settings();
		try {
			settings = (Settings) paintNetwork.getNetworkFrame().getSettings().clone();
		} catch (CloneNotSupportedException ex) {
			Logger.getLogger(SettingsDialog.class.getName()).log(Level.SEVERE, null, ex);
		}
		SettingsDialog settingsDialog = new SettingsDialog(settings, null, true);
		settingsDialog.setSettings(settings);
		settingsDialog.setVisible(true);
		
		if (settingsDialog.isDialogResult()) {
			paintNetwork.getNetworkFrame().setSettings(settingsDialog.getSettings());
			paintNetwork.repaint();
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private NeuralNetwork neuralNetwork;
	
	private JButton btnUpdate;
	private JButton btnSettings;
	private JToolBar tbTools;
	
	private JButton btnCenter;
	private JButton btnUp;
	private JButton btnDown;
	private JButton btnLeft;
	private JButton btnRight;
	private JLabel lblSize;
	private JComboBox cobSize;
	private JToolBar tbMove;
	
	private CANNPaintNetwork paintNetwork;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущую нейронную сеть.
	 * 
	 * @return Нейронная сеть
	 */
	public NeuralNetwork getNeuralNetwork() {
		return neuralNetwork;
	}

	/**
	 * Устанавливает новую нейронную сеть.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 */
	public void setNeuralNetwork(NeuralNetwork neuralNetwork) {
		this.neuralNetwork = neuralNetwork;
		
		update();
	}
	
	//</editor-fold>
    
}
