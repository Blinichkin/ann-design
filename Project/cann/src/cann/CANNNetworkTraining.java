/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import ann.*;
import ann.training.TrainingSettings;
import cann.data.*;
import cann.networktraining.*;
import java.awt.BorderLayout;
import java.awt.event.*;
import java.util.logging.*;
import jhelper.ArrayConverter;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Компонент для обучения нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNNetworkTraining extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNNetworkTraining() {
		this(new SingleLayerPerceptron(), new DataStore(), new TrainingSettings());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 * @param dataStore Хранилище данных
	 * @param trainingSettings Настройки обучения
	 */
	public CANNNetworkTraining(NeuralNetwork neuralNetwork, DataStore dataStore, 
			TrainingSettings trainingSettings) {
		this.neuralNetwork = neuralNetwork;
		this.dataStore = dataStore;
		this.trainingSettings = trainingSettings;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		controlPanel = new CANNControlPanel();
		pnlWeights = new JPanel();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка controlPanel
		{
			Border border = new EmptyBorder(0, 5, 0, 5);
		
			controlPanel.setBorder(border);
		}
		
		// Настройка pnlWeights
		{
			Border titleBorder = BorderFactory.createTitledBorder("Весовые коэффициенты:");
			Border emptyBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
			Border border = BorderFactory.createCompoundBorder(emptyBorder, titleBorder);
			
			pnlWeights.setBorder(border);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		controlPanel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switch (e.getActionCommand()) {
					case "Play":
						if (check()) { 
							start(); 
						}
						break;
					case "Stop":
						stop();
						break;
					case "Settings":
						openSettings();
						break;
				}
			}
		});
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlWeights
		pnlWeights.setLayout(new BorderLayout());
		
		// Настройка компонента
		{
			setLayout(new BorderLayout());
		
			add(controlPanel, BorderLayout.NORTH);
			add(pnlWeights, BorderLayout.CENTER);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateWeights();
	}
	
	/**
	 * Обновляет weights.
	 */
	private void updateWeights() {
		weights = OutputWeights.getWeights(neuralNetwork.getNetworkType());
		pnlWeights.add(weights, BorderLayout.CENTER);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Запуск обучения.
	 */
	private void start() {
		controlPanel.setIndeterminate(true);
		worker = new SwingWorker<Void, Integer>() {
			@Override
			protected Void doInBackground() throws Exception {
				training();
				
				return null;
			}
			
			@Override
			protected void done() {
				controlPanel.setIndeterminate(false);
				weights.update();
				JOptionPane.showMessageDialog(controlPanel, "Обучение сети завершено", "Сообщение", 
						JOptionPane.INFORMATION_MESSAGE);
			}
		};
		
		worker.execute();
	}
	
	/**
	 * Обучение нейронной сети.
	 */
	private void training() {
		double[][] x = ArrayConverter.objectToDouble(dataStore.getInputData().getData());
		double[][] y = ArrayConverter.objectToDouble(dataStore.getOutputData().getData());
		
		switch (neuralNetwork.getNetworkType()) {
			case SingleLayerPerceptron:
				OutputWeightsSingleLayerPerceptron weightsSingleLayerPerceptron = 
						(OutputWeightsSingleLayerPerceptron) weights;
				SingleLayerPerceptron singleLayerPerceptron = 
						(SingleLayerPerceptron) neuralNetwork;
				singleLayerPerceptron.training(x, y, trainingSettings);
				weightsSingleLayerPerceptron.setW(singleLayerPerceptron.getW());
				break;
			case MultiLayerPerceptron:
				OutputWeightsMultiLayerPerceptron weightMultiLayerPerceptron = 
						(OutputWeightsMultiLayerPerceptron) weights;
				MultiLayerPerceptron multiLayerPerceptron = 
						(MultiLayerPerceptron) neuralNetwork;
				multiLayerPerceptron.training(x, y, trainingSettings);
				weightMultiLayerPerceptron.setW(multiLayerPerceptron.getW());
				break;
		}
	}
	
	/**
	 * Проверка данных для обучения.
	 * 
	 * @return Результат
	 */
	private boolean check() {
		checkDataStore();
		
		return checkInputData(dataStore.getInputData()) 
				&& checkOutputData(dataStore.getOutputData());
	}
	
	/**
	 * Проверка хранилища данных.
	 */
	private void checkDataStore() {
		if (dataStore == null)
			throw new NullPointerException("Хранилище данных не найдено.");
		
		if (dataStore.getInputData() == null)
			throw new NullPointerException("Таблица входных данных не найдена.");
		
		if (dataStore.getOutputData() == null)
			throw new NullPointerException("Таблица выходных данных не найдена.");
	}
	
	/**
	 * Проверка входных данных.
	 * 
	 * @param dataTable Таблица данных
	 * @return Результат
	 */
	private boolean checkInputData(DataTable dataTable) {
		if (dataTable.getData() == null) {
			JOptionPane.showMessageDialog(this, "Входные данные для обучения сети не загружены", 
					"Ошибка", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if (dataTable.getColumnCount() != 
				neuralNetwork.getNetworkModel().getNumberInputs().getValue()) {
			JOptionPane.showMessageDialog(this, 
					"Количество входов сети не соответствует количеству столбцов входных данных", 
					"Ошибка", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Проверка выходных данных.
	 * 
	 * @param dataTable Таблица данных
	 * @return Результат
	 */
	private boolean checkOutputData(DataTable dataTable) {
		if (dataTable.getData() == null) {
			JOptionPane.showMessageDialog(this, "Выходные данные для обучения сети не загружены", 
					"Ошибка", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if (dataTable.getColumnCount() != 
				neuralNetwork.getNetworkModel().getNumberOutputs().getValue()) {
			JOptionPane.showMessageDialog(this, 
					"Количество выходов сети не соответствует количеству столбцов выходных данных", 
					"Ошибка", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Прерывает обучение.
	 */
	private void stop() {
		worker.cancel(true);
	}
	
	/**
	 * Открывает настройки.
	 */
	private void openSettings() {
		TrainingSettings trainingSettings = new TrainingSettings();
		try {
			trainingSettings = (TrainingSettings) this.trainingSettings.clone();
		} catch (CloneNotSupportedException ex) {
			Logger.getLogger(TrainingSettingsDialog.class.getName()).log(Level.SEVERE, null, ex);
		}
		TrainingSettingsDialog trainingSettingsDialog = 
				new TrainingSettingsDialog(trainingSettings, null, true);
		trainingSettingsDialog.setVisible(true);
		
		if (trainingSettingsDialog.isDialogResult()) {
			this.trainingSettings = trainingSettingsDialog.getTrainingSettings();
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private NeuralNetwork neuralNetwork;
	private DataStore dataStore;
	private TrainingSettings trainingSettings;
	private SwingWorker<Void, Integer> worker;
	
	private CANNControlPanel controlPanel;
	private JPanel pnlWeights;
	private OutputWeights weights;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущую нейронную сеть.
	 * 
	 * @return Нейронная сеть
	 */
	public NeuralNetwork getNeuralNetwork() {
		return neuralNetwork;
	}
	
	/**
	 * Устанавливает новую нейронную сеть.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 */
	public void setNeuralNetwork(NeuralNetwork neuralNetwork) {
		this.neuralNetwork = neuralNetwork;
		
		update();
	}
	
	/**
	 * Получает текущее хранилище данных.
	 * 
	 * @return Хранилище данных
	 */
	public DataStore getDataStore() {
		return dataStore;
	}

	/**
	 * Устанавливает новое хранилище данных.
	 * 
	 * @param dataStore Хранилище данных
	 */
	public void setDataStore(DataStore dataStore) {
		this.dataStore = dataStore;
	}
	
	//</editor-fold>
	
}
