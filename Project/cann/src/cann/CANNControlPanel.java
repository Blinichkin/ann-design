/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import cann.controlpanel.AbstractControlPanel;
import cann.other.Tools;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Компонент управления.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNControlPanel extends AbstractControlPanel {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNControlPanel() {
		init();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		btnPlay = new JButton();
		btnStop = new JButton();
		btnSettings = new JButton();
		progressBar = new JProgressBar();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		// Настройка btnPlay
		{
			btnPlay.setActionCommand("Play");
			btnPlay.setIcon(tools.getIcon("play.png"));
			btnPlay.setToolTipText("Запуск");
		}
		
		// Настройка btnStop
		{
			btnStop.setActionCommand("Stop");
			btnStop.setEnabled(false);
			btnStop.setIcon(tools.getIcon("stop.png"));
			btnStop.setToolTipText("Стоп");
		}
		
		// Настройка btnSettings
		{
			btnSettings.setActionCommand("Settings");
			btnSettings.setIcon(tools.getIcon("settings.png"));
			btnSettings.setToolTipText("Настройки");
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnPlay.addActionListener(this::btnPlayActionPerformed);
		btnStop.addActionListener(this::btnStopActionPerformed);
		btnSettings.addActionListener(this::btnSettingsActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		setLayout(new GridBagLayout());
		Insets insets = new Insets(1, 1, 1, 1);
		
		add(btnPlay, new GridBagConstraints(
			0, 0, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.NONE, 
			insets, 0, 0)
		);
		add(btnStop, new GridBagConstraints(
			1, 0, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.NONE, 
			insets, 0, 0)
		);
		add(btnSettings, new GridBagConstraints(
			2, 0, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.NONE, 
			insets, 0, 0)
		);
		add(progressBar, new GridBagConstraints(
			3, 0, 1, 1, 1, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnPlayActionPerformed(ActionEvent e) {
		progressBar.setValue(MIN_PROGRESS);
		fireActionPerformed(e);
	}
	
	private void btnStopActionPerformed(ActionEvent e) {
		fireActionPerformed(e);
	}
	
	private void btnSettingsActionPerformed(ActionEvent e) {
		fireActionPerformed(e);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JButton btnPlay;
	private JButton btnStop;
	private JButton btnSettings;
	private JProgressBar progressBar;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение неопределенности.
	 * 
	 * @return Состояние
	 */
	public boolean isIndeterminate() {
		return progressBar.isIndeterminate();
	}
	
	/**
	 * Устанавливает новое значение неопределенности.
	 * 
	 * @param value Состояние
	 */
	public void setIndeterminate(boolean value) {
		btnPlay.setEnabled(!value);
		btnStop.setEnabled(value);
		progressBar.setIndeterminate(value);
	}
	
	//</editor-fold>
	
}
