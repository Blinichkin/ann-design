/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import cann.networkview.ImageSettings;
import cann.networkview.frame.NetworkFrame;
import cann.networkview.Background;
import cann.networkview.Settings;
import cann.networkview.frame.SingleLayerPerceptronFrame;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JComponent;

/**
 * Компонент для визуального представления структуры нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNPaintNetwork extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	public CANNPaintNetwork() {
		this(new SingleLayerPerceptronFrame());
	}
	
	public CANNPaintNetwork(NetworkFrame networkFrame) {
		this.networkFrame = networkFrame;
		
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	public void update() {
		updateSize();
		
		createImages();
		updateImage();
	}
	
	/**
	 * Обновляет размер
	 */
	private void updateSize() {
		int width = getWidth();
		int height = getHeight();
		
		networkFrame.getImageSettings().setWidth(width);
		networkFrame.getImageSettings().setHeight(height);
	}
	
	/**
	 * Обновляет изображение
	 */
	private void updateImage() {
		if (backImage == null || frontImage == null) return;
		Background.paint(backImage, networkFrame.getSettings(), networkFrame.getImageSettings());
		
		networkFrame.init();
		networkFrame.calc();
		networkFrame.paint(frontImage);
		
		Graphics2D g2D = image.createGraphics();
		g2D.drawImage(backImage, 0, 0, null);
		g2D.drawImage(frontImage, 0, 0, null);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Создает изображения.
	 */
	private void createImages() {
		backImage = createImage(backImage, networkFrame.getImageSettings());
		frontImage = createImage(frontImage, networkFrame.getImageSettings());
		image = createImage(image, networkFrame.getImageSettings());
	}
	
	/**
	 * Создает изображение.
	 * 
	 * @param image Изображение
	 * @return Изображение
	 */
	private BufferedImage createImage(BufferedImage image, ImageSettings imageSettings) {
		int width = imageSettings.getWidth();
		int height = imageSettings.getHeight();
		
		if (image == null) {
			if (width <= 0 || height <= 0) {
				image = null;
			} else {
				image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			}
		} else {
			if (width != image.getWidth() || height != image.getHeight())
				image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		}
		
		return image;
	}
	
	@Override
	public void paint(Graphics g) {
		update();
		
		if (image != null)
			g.drawImage(image, 0, 0, null);

		g.setColor(new Color(150, 150, 150));
		g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
	}
	
	/**
	 * Перемещает в центр.
	 */
	public void moveCenter() {
		networkFrame.getImageSettings().getOffset().x = 0;
		networkFrame.getImageSettings().getOffset().y = 0;
		
		update();
		repaint();
	}
	
	/**
	 * Перемещает вверх.
	 */
	public void moveUp() {
		Settings settings = networkFrame.getSettings();
		networkFrame.getImageSettings().getOffset().y += 
				settings.getBackdrop().getGrid().getInterval(settings.getSizeType()) / 2;
		
		update();
		repaint();
	}
	
	/**
	 * Перемещает вниз.
	 */
	public void moveDown() {
		Settings settings = networkFrame.getSettings();
		networkFrame.getImageSettings().getOffset().y -= 
				settings.getBackdrop().getGrid().getInterval(settings.getSizeType()) / 2;
		
		update();
		repaint();
	}
	
	/**
	 * Перемещает влево.
	 */
	public void moveLeft() {
		Settings settings = networkFrame.getSettings();
		networkFrame.getImageSettings().getOffset().x += 
				settings.getBackdrop().getGrid().getInterval(settings.getSizeType()) / 2;
		
		update();
		repaint();
	}
	
	/**
	 * Перемещает вправо.
	 */
	public void moveRight() {
		Settings settings = networkFrame.getSettings();
		networkFrame.getImageSettings().getOffset().x -= 
				settings.getBackdrop().getGrid().getInterval(settings.getSizeType()) / 2;
		
		update();
		repaint();
	}

	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private NetworkFrame networkFrame;
	private BufferedImage backImage;
	private BufferedImage frontImage;
	private BufferedImage image;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущий каркас сети.
	 * 
	 * @return Каркас сети
	 */
	public NetworkFrame getNetworkFrame() {
		return networkFrame;
	}

	/**
	 * Устанавливает новый каркас сети.
	 * 
	 * @param networkFrame Каркас сети
	 */
	public void setNetworkFrame(NetworkFrame networkFrame) {
		this.networkFrame = networkFrame;
	}
	
	/**
	 * Получает текущее изображение фона.
	 * 
	 * @return Изображение
	 */
	public BufferedImage getBackImage() {
		return backImage;
	}
	
	/**
	 * Получает текущее изображение нейронной сети.
	 * 
	 * @return Изображение
	 */
	public BufferedImage getFrontImage() {
		return frontImage;
	}
	
	public BufferedImage getImage() {
		return image;
	}
	
	//</editor-fold>
	
}
