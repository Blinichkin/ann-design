/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.builder;

import cann.data.DataTable;
import cann.builder.databuilder.TableDesignerDialog;
import cann.other.Tools;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import jhelper.DataProcessor;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Компонент построителя таблиц.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNDataBuilder extends CANNBuilder {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNDataBuilder() {
		this(new DataTable());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param dataTable Таблица данных
	 */
	public CANNDataBuilder(DataTable dataTable) {
		super.getLblTitle().setText("data.dat");
		this.dataTable = dataTable;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		tbTools = new JToolBar();
		btnDesigner = new JButton();
		
		btnOpen = new JButton();
		btnSave = new JButton();
		btnSaveAs = new JButton();
		
		tblData = new JTable();
		tableModel = new DefaultTableModel();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		// Настройка btnDesigner
		btnDesigner.setText("Конструктор");
		
		// Настройка btnOpen
		btnOpen.setIcon(tools.getIcon("folder2.png"));
		
		// Настройка btnSave
		btnSave.setIcon(tools.getIcon("save.png"));
		
		// Настройка btnSaveAs
		btnSaveAs.setIcon(tools.getIcon("saveAs.png"));
		
		// Настройка tblData
		{
			tblData.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			tblData.setModel(tableModel);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnDesigner.addActionListener(this::btnDesignerActionPerformed);
		btnOpen.addActionListener(this::btnOpenActionPerformed);
		btnSave.addActionListener(this::btnSaveActionPerformed);
		btnSaveAs.addActionListener(this::btnSaveAsActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка tbTools
		{
			tbTools.add(btnDesigner);
			tbTools.addSeparator();
			tbTools.add(btnOpen);
			tbTools.add(btnSave);
			tbTools.add(btnSaveAs);
		}
		
		// Настройка компонента
		{
			setLayout(new BorderLayout());
			
			add(tbTools, BorderLayout.NORTH);
			add(new JScrollPane(tblData), BorderLayout.CENTER);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateTblData();
	}
	
	/**
	 * Обновляет tableModel
	 */
	private void updateTblData() {
		tableModel.setDataVector(dataTable.getData(), dataTable.getHeaders());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnDesignerActionPerformed(ActionEvent e) {
		saveData();
		openDesigner();
	}
	
	private void btnOpenActionPerformed(ActionEvent e) {
		open();
	}
	
	private void btnSaveActionPerformed(ActionEvent e) {
		save();
	}
	
	private void btnSaveAsActionPerformed(ActionEvent e) {
		saveAs();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Открывает конструктор таблиц.
	 */
	private void openDesigner() {
		TableDesignerDialog tableDesigner = new TableDesignerDialog(dataTable, null, true);
		tableDesigner.setVisible(true);
			
		if (tableDesigner.isResultDialog()) {
			dataTable = tableDesigner.getDataTable();
			updateTblData();
		}
	}
	
	@Override
	public void open() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Выбор файла");
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileNameExtensionFilter filter = 
				new FileNameExtensionFilter("Таблица данных (*.dat)", "dat");
		fileChooser.setFileFilter(filter);
		int result = fileChooser.showOpenDialog(null);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			filePath = fileChooser.getSelectedFile().toString();
			super.getLblTitle().setText(fileChooser.getSelectedFile().getName());
			dataTable = (DataTable) DataProcessor.deserialization(filePath);
			updateTblData();
		}
	}
	
	@Override
	public void save() {
		if (filePath == null)
			saveAs();
		else {
			saveData();
			DataProcessor.serialization(filePath, dataTable);
		}
	}
	
	@Override
	public void saveAs() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Сохранение файла");
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileNameExtensionFilter filter = 
				new FileNameExtensionFilter("Таблица данных (*.dat)", "dat");
		fileChooser.setFileFilter(filter);
		int result = fileChooser.showSaveDialog(null);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			filePath = fileChooser.getSelectedFile().toString();
			String fileName = fileChooser.getSelectedFile().getName();
			
			if (!filePath.endsWith(".dat")) {
				filePath += ".dat";
				fileName += ".dat";
			}
			
			saveData();
			DataProcessor.serialization(filePath, dataTable);
			super.getLblTitle().setText(fileName);
		}
	}
	
	/**
	 * Сохранение данных.
	 */
	private void saveData() {
		int rowCount = tblData.getRowCount();
		int columnCount = tblData.getColumnCount();
		Object[][] data = new Object[rowCount][columnCount];
		
		for (int i = 0; i < rowCount; i++)
            for (int j = 0; j < columnCount; j++)
                data[i][j] = tblData.getValueAt(i, j);
		
		dataTable.setData(data);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private DataTable dataTable;
	
	private JToolBar tbTools;
	private JButton btnDesigner;
	
	private JButton btnOpen;
	private JButton btnSave;
	private JButton btnSaveAs;
	
	private JTable tblData;
	private DefaultTableModel tableModel;
	
	//</editor-fold>
	
}
