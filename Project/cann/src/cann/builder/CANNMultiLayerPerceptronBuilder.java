/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.builder;

import ann.MultiLayerPerceptron;
import cann.CANNCalc;
import cann.CANNNetworkTraining;
import cann.CANNVisualNetwork;
import cann.CANNDataUpload;
import cann.dataupload.IODataUpload;
import cann.data.DataStore;

/**
 * Компонент построителя многослойного персептрона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNMultiLayerPerceptronBuilder extends CANNNetworkBuilder {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNMultiLayerPerceptronBuilder() {
		this(new MultiLayerPerceptron());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param network Многослойный персептрон
	 */
	public CANNMultiLayerPerceptronBuilder(MultiLayerPerceptron network) {
		this(network, new DataStore());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param network Многослойный персептрон
	 * @param dataStore Хранилище данных
	 */
	public CANNMultiLayerPerceptronBuilder(MultiLayerPerceptron network, DataStore dataStore) {
		super(network, dataStore);
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	@Override
	protected void init() {
		super.init();
		
		initComponents();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		visualNetwork = new CANNVisualNetwork();
		dataNetwork = new IODataUpload();
		networkTraining = new CANNNetworkTraining();
		calc = new CANNCalc();
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlCenter
		{
			pnlCenter.add(visualNetwork, "0");
			pnlCenter.add(dataNetwork, "1");
			pnlCenter.add(networkTraining, "2");
			pnlCenter.add(calc, "3");
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	@Override
	protected void update() {
		updateVisualNetwork();
		updateDataNetwork();
		updateNetworkTraining();
		updateCalc();
	}
	
	/**
	 * Обновляет visualNetwork
	 */
	private void updateVisualNetwork() {
		visualNetwork.setNeuralNetwork(getNeuralNetwork());
	}
	
	/**
	 * Обновляет dataNetwork
	 */
	private void updateDataNetwork() {
		dataNetwork.setDataStore(getDataStore());
	}
	
	/**
	 * Обновляет networkTraining
	 */
	private void updateNetworkTraining() {
		networkTraining.setNeuralNetwork(getNeuralNetwork());
		networkTraining.setDataStore(getDataStore());
	}
	
	/**
	 * Обновляет calc
	 */
	private void updateCalc() {
		calc.setNeuralNetwork(getNeuralNetwork());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private CANNVisualNetwork visualNetwork;
	private CANNDataUpload dataNetwork;
	private CANNNetworkTraining networkTraining;
	private CANNCalc calc;
	
	//</editor-fold>
	
}
