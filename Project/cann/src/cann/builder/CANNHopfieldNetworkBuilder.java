/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.builder;

import ann.HopfieldNetwork;
import cann.CANNCalc;
import cann.CANNVisualNetwork;
import cann.CANNDataUpload;
import cann.dataupload.IDataUpload;
import cann.data.DataStore;

/**
 * Компонент построителя сети Хопфилда.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNHopfieldNetworkBuilder extends CANNNetworkBuilder {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNHopfieldNetworkBuilder() {
		this(new HopfieldNetwork());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param network Сеть Хопфилда
	 */
	public CANNHopfieldNetworkBuilder(HopfieldNetwork network) {
		this(network, new DataStore());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param network Сеть Хопфилда
	 * @param dataStore Хранилище данных
	 */
	public CANNHopfieldNetworkBuilder(HopfieldNetwork network, DataStore dataStore) {
		super(network, dataStore);
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	@Override
	protected void init() {
		super.init();
		
		initComponents();
		settingsComponents();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		visualNetwork = new CANNVisualNetwork();
		dataNetwork = new IDataUpload();
		calc = new CANNCalc();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingsComponents() {
		// Настройка btnNetworkTraining
		btnNetworkTraining.setEnabled(false);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlCenter
		{
			pnlCenter.add(visualNetwork, "0");
			pnlCenter.add(dataNetwork, "1");
			pnlCenter.add(calc, "3");
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	@Override
	protected void update() {
		updateVisualNetwork();
		updateDataNetwork();
		updateCalc();
	}
	
	/**
	 * Обновляет visualNetwork.
	 */
	private void updateVisualNetwork() {
		visualNetwork.setNeuralNetwork(getNeuralNetwork());
	}
	
	/**
	 * Обновляет dataNetwork.
	 */
	private void updateDataNetwork() {
		dataNetwork.setDataStore(getDataStore());
	}
	
	/**
	 * Обновляет calc.
	 */
	private void updateCalc() {
		calc.setNeuralNetwork(getNeuralNetwork());
		calc.setDataStore(getDataStore());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private CANNVisualNetwork visualNetwork;
	private CANNDataUpload dataNetwork;
	private CANNCalc calc;
	
	//</editor-fold>
	
}
