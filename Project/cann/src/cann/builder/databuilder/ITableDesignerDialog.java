/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.builder.databuilder;

/**
 * Интерфейс диалогового окна конструктора таблиц.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public interface ITableDesignerDialog {
	
	//<editor-fold desc="Константы">
	
	/** Стандартное значение количества колонок */
	public static final int COLUMN_COUNT = 0;
	
	/** Минимальное значение количества колонок */
	public static final int MIN_COLUMN_COUNT = 0;
	
	/** Максимальное значение количества колонок */
	public static final int MAX_COLUMN_COUNT = Integer.MAX_VALUE;
	
	/** Размер шага количества колонок */
	public static final int STEP_SIZE_COLUMN_COUNT = 1;
	
	/** Стандартное значение количества строк */
	public static final int ROW_COUNT = 0;
	
	/** Минимальное значение количества строк */
	public static final int MIN_ROW_COUNT = 0;
	
	/** Максимальное значение количества строк */
	public static final int MAX_ROW_COUNT = Integer.MAX_VALUE;
	
	/** Размер шага количества строк */
	public static final int STEP_SIZE_ROW_COUNT = 1;
	
	/** Ширина диалогового окна конструктора таблиц */
	public static final int WIDTH_TABLE_DESIGNER = 500;
	
	/** Высота диалогового окна конструктора таблиц */
	public static final int HEIGHT_TABLE_DESIGNER = 250;
	
	/** Название иконки диалогового окна конструктора таблиц */
	public static final String ICON_TABLE_DESIGNER = "settings.png";
	
	//</editor-fold>
}
