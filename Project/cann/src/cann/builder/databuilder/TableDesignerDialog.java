/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.builder.databuilder;

import cann.data.DataTable;
import ann.structure.TypeData;
import cann.other.Tools;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.border.Border;

/**
 * Диалоговое окно конструктора таблиц.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class TableDesignerDialog extends JDialog implements ITableDesignerDialog {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартное диалоговое окно.
	 */
	public TableDesignerDialog() {
		this(new DataTable(), (Frame) null, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param dataTable Таблица данных
	 */
	public TableDesignerDialog(DataTable dataTable) {
		this(dataTable, (Frame) null, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param dataTable Таблица данных
	 * @param owner Владелец
	 */
	public TableDesignerDialog(DataTable dataTable, Frame owner) {
		this(dataTable, owner, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param dataTable Таблица данных
	 * @param owner Владелец
	 * @param modal Модальность
	 */
	public TableDesignerDialog(DataTable dataTable, Frame owner, boolean modal) {
		this(dataTable, owner, "Конструктор таблиц", modal);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param dataTable Таблица данных
	 * @param owner Владелец
	 * @param title Заголовок
	 */
	public TableDesignerDialog(DataTable dataTable, Frame owner, String title) {
		this(dataTable, owner, title, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param dataTable Таблица данных
	 * @param owner Владелец
	 * @param title Заголовок
	 * @param modal Модальность
	 */
	public TableDesignerDialog(DataTable dataTable, Frame owner, String title, boolean modal) {
		super(owner, title, modal, null);
		this.dataTable = dataTable;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		pnlTableSettings = new JPanel();
		lblTypeData = new JLabel();
		cobTypeData = new JComboBox();
		lblColumnCount = new JLabel();
		spnColumnCount = new JSpinner();
		lblRowCount = new JLabel();
		spnRowCount = new JSpinner();
		
		pnlBottom = new JPanel();
		btnOk = new JButton();
		btnCancel = new JButton();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		// Настройка pnlTableSettings
		{
			Border titleBorder = BorderFactory.createTitledBorder("Настройки таблицы");
			Border emptyBorder = BorderFactory.createEmptyBorder(10, 16, 0, 16);
			Border border = BorderFactory.createCompoundBorder(emptyBorder, titleBorder);
			
			pnlTableSettings.setBorder(border);
		}
		
		// Настройка lblTypeData
		lblTypeData.setText("Тип данных:");
		
		// Настройка cobTypeData
		{
			for (TypeData typeData: TypeData.values())
				cobTypeData.addItem(typeData);
		}
		
		// Настройка lblColumnCount
		lblColumnCount.setText("Количество столбцов:");
		
		// Настройка spnColumnCount
		{
			SpinnerNumberModel model = new SpinnerNumberModel(
				COLUMN_COUNT,
				MIN_COLUMN_COUNT, 
				MAX_COLUMN_COUNT,
				STEP_SIZE_COLUMN_COUNT
			);
			spnColumnCount.setModel(model);
		}
		
		// Настройка lblRowCount
		lblRowCount.setText("Количество строк:");
		
		// Настройка spnRowCount
		{
			SpinnerNumberModel model = new SpinnerNumberModel(
				ROW_COUNT,
				MIN_ROW_COUNT, 
				MAX_ROW_COUNT,
				STEP_SIZE_ROW_COUNT
			);
			spnRowCount.setModel(model);
		}
		
		// Настройка pnlBottom
		{
			Border border = BorderFactory.createEmptyBorder(5, 5, 5, 5);
			pnlBottom.setBorder(border);
		}
		
		// Настройка btnOk
		btnOk.setText("ОК");
		
		// Настройка btnCancel
		btnCancel.setText("Отмена");
		
		// Настройка диалогового окна
		{
			setIconImage(tools.getIcon(ICON_TABLE_DESIGNER).getImage());
			setSize(WIDTH_TABLE_DESIGNER, HEIGHT_TABLE_DESIGNER);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnOk.addActionListener(this::btnOkActionPerformed);
		btnCancel.addActionListener(this::btnCancelActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		Insets insets = new Insets(1, 1, 1, 1);
		
		// Настройка pnlTableSettings
		{
			pnlTableSettings.setLayout(new GridBagLayout());
		
			pnlTableSettings.add(
				lblTypeData, new GridBagConstraints(0, 0, 1, 1, 0, 0, 
				GridBagConstraints.EAST, GridBagConstraints.BOTH, 
				insets, 0, 0)
			);
			pnlTableSettings.add(
				cobTypeData, new GridBagConstraints(1, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				insets, 0, 0)
			);
			pnlTableSettings.add(
				lblColumnCount, new GridBagConstraints(0, 1, 1, 1, 0, 0, 
				GridBagConstraints.EAST, GridBagConstraints.BOTH, 
				insets, 0, 0)
			);
			pnlTableSettings.add(
				spnColumnCount, new GridBagConstraints(1, 1, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				insets, 0, 0)
			);
			pnlTableSettings.add(
				lblRowCount, new GridBagConstraints(0, 2, 1, 1, 0, 0, 
				GridBagConstraints.EAST, GridBagConstraints.BOTH, 
				insets, 0, 0)
			);
			pnlTableSettings.add(
				spnRowCount, new GridBagConstraints(1, 2, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				insets, 0, 0)
			);
		}
		
		// Настройка pnlBottom
		{
			pnlBottom.setLayout(new GridBagLayout());
			
			pnlBottom.add(
				btnOk, new GridBagConstraints(1, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
			pnlBottom.add(
				btnCancel, new GridBagConstraints(2, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
		}
		
		// Настройка диалогового окна
		{
			setLayout(new BorderLayout());
			
			add(pnlTableSettings, BorderLayout.CENTER);
			add(pnlBottom, BorderLayout.SOUTH);
		}
		
		setLocationRelativeTo(null);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет диалоговое окно.
	 */
	private void update() {
		updateCobTypeData();
		updateSpnColumnCount();
		updateSpnRowCount();
	}
	
	/**
	 * Обновляет cobTypeData.
	 */
	private void updateCobTypeData() {
		TypeData typeData = dataTable.getTypeData();
		cobTypeData.setSelectedItem(typeData);
	}
	
	/**
	 * Обновляет spnColumnCount.
	 */
	private void updateSpnColumnCount() {
		int value = dataTable.getColumnCount();
		spnColumnCount.setValue(value);
	}
	
	/**
	 * Обновляет spnRowCount.
	 */
	private void updateSpnRowCount() {
		int value = dataTable.getRowCount();
		spnRowCount.setValue(value);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnOkActionPerformed(ActionEvent e) {
		save();
		resultDialog = true;
		dispose();
	}
	
	private void btnCancelActionPerformed(ActionEvent e) {
		resultDialog = false;
		dispose();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Сохранение данных.
	 */
	private void save() {
		saveTypeData();
		saveHeaders();
		saveData();
	}
	
	/**
	 * Сохранение типа данных.
	 */
	private void saveTypeData() {
		TypeData typeData = (TypeData) cobTypeData.getSelectedItem();
		dataTable.setTypeData(typeData);
	}
	
	/**
	 * Сохранение заголовков.
	 */
	private void saveHeaders() {	
		int columnCount = (int) spnColumnCount.getValue();
		String[] headers = new String[columnCount];
		
		dataTable.setHeaders(headers);
	}
	
	/**
	 * Сохранение данных.
	 */
	private void saveData() {
		if (!checkSizeTable()) return;
		
		Object[][] data = createArray();
			
		if (checkDataTable())
			saveArray(data);
			
		dataTable.setData(data);
	}
	
	/**
	 * Проверка размера новой таблицы со старой.
	 * 
	 * @return Возвращет true, если таблицы не равны, иначе false.
	 */
	private boolean checkSizeTable() {
		int columnCount = (int) spnColumnCount.getValue();
		int rowCount = (int) spnRowCount.getValue();
		
		return dataTable == null 
				|| columnCount != dataTable.getColumnCount() 
				|| rowCount != dataTable.getRowCount();
	}
	
	/**
	 * Проверка существования данных в таблице.
	 * 
	 * @return Возвращает true, если данных не существует, иначе false.
	 */
	private boolean checkDataTable() {
		return dataTable != null && dataTable.getData() != null;
	}
	
	/**
	 * Создает массив необходимый для хранения данных таблицы.
	 * 
	 * @return Массив
	 */
	private Object[][] createArray() {
		int columnCount = (int) spnColumnCount.getValue();
		int rowCount = (int) spnRowCount.getValue();
		
		return new Object[rowCount][columnCount];
	}
	
	/**
	 * Сохраняет данные старого массива в новом массиве.
	 * 
	 * @param data Массив данных
	 */
	private void saveArray(Object[][] data) {
		int columnCount = Math.min(data[0].length, dataTable.getColumnCount());
		int rowCount = Math.min(data.length, dataTable.getRowCount());
		
		for (int i = 0; i < rowCount; i++)
			for (int j = 0; j < columnCount; j++)
				data[i][j] = dataTable.getData()[i][j];
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private DataTable dataTable;
	private boolean resultDialog;
	
	private JPanel pnlTableSettings;
	private JLabel lblTypeData;
	private JComboBox<TypeData> cobTypeData;
	private JLabel lblColumnCount;
	private JSpinner spnColumnCount;
	private JLabel lblRowCount;
	private JSpinner spnRowCount;
	
	private JPanel pnlBottom;
	private JButton btnOk;
	private JButton btnCancel;
	
	//</editor-fold>
	
	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущую таблицу даннных.
	 * 
	 * @return Таблица данных
	 */
	public DataTable getDataTable() {
		return dataTable;
	}

	/**
	 * Устанавливает новую таблицу данных.
	 * 
	 * @param dataTable Таблица данных
	 */
	public void setDataTable(DataTable dataTable) {
		this.dataTable = dataTable;
	}
	
	/**
	 * Возвращает результат диалогового окна.
	 * 
	 * @return Результат
	 */
	public boolean isResultDialog() {
		return resultDialog;
	}
	
	//</editor-fold>
	
}
