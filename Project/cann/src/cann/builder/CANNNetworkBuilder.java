/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.builder;

import ann.NeuralNetwork;
import ann.SingleLayerPerceptron;
import ann.structure.NetworkType;
import cann.data.DataStore;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import jhelper.DataProcessor;

/**
 * Компонент построителя нейронных сетей.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public abstract class CANNNetworkBuilder extends CANNBuilder {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNNetworkBuilder() {
		this(new SingleLayerPerceptron(), new DataStore());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 * @param dataStore Хранилище данных
	 */
	public CANNNetworkBuilder(NeuralNetwork neuralNetwork, DataStore dataStore) {
		super.getLblTitle().setText("network.ann");
		
		this.neuralNetwork = neuralNetwork;
		this.dataStore = dataStore;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	protected void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		pnlTop = new JPanel();
		btnNetworkStructure = new JButton();
		btnDataNetwork = new JButton();
		btnNetworkTraining = new JButton();
		btnCalcNetwork = new JButton();
		
		pnlCenter = new JPanel();
		cardLayout = new CardLayout();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка pnlTop
		{
			Border border = new EmptyBorder(2, 2, 2, 2);
			pnlTop.setBorder(border);
		}
		
		// Настройка pnlCenter
		{
			Border border = new EmptyBorder(2, 2, 2, 2);
			pnlCenter.setBorder(border);
		}
		
		// Настройка btnNetworkStructure
		btnNetworkStructure.setText("Проектирование");
		
		// Настройка btnDataNetwork
		btnDataNetwork.setText("Данные");
		
		// Настройка btnNetworkTraining
		btnNetworkTraining.setText("Обучение");
		
		// Настройка btnCalcNetwork
		btnCalcNetwork.setText("Вычисление");
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnNetworkStructure.addActionListener(this::btnNetworkStructureActionPerformed);
		btnDataNetwork.addActionListener(this::btnDataNetworkActionPerformed);
		btnNetworkTraining.addActionListener(this::btnNetworkTrainingActionPerformed);
		btnCalcNetwork.addActionListener(this::btnCalcNetworkActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlTop
		{
			pnlTop.setLayout(new GridBagLayout());
			Insets insets = new Insets(1, 1, 1, 1);
			
			pnlTop.add(btnNetworkStructure, new GridBagConstraints(
				1, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
			pnlTop.add(btnDataNetwork, new GridBagConstraints(
				2, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
			pnlTop.add(btnNetworkTraining, new GridBagConstraints(
				3, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
			pnlTop.add(btnCalcNetwork, new GridBagConstraints(
				4, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
		}
		
		// Настройка pnlCenter
		pnlCenter.setLayout(cardLayout);

		// Настройка компонента
		{
			setLayout(new BorderLayout());
			
			add(pnlTop, BorderLayout.NORTH);
			add(pnlCenter, BorderLayout.CENTER);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	protected abstract void update();
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnNetworkStructureActionPerformed(ActionEvent e) {
		numberLayer = 0;
		showPanel();
	}
	
	private void btnDataNetworkActionPerformed(ActionEvent e) {
		numberLayer = 1;
		showPanel();
	}
	
	private void btnNetworkTrainingActionPerformed(ActionEvent e) {
		numberLayer = 2;
		showPanel();
	}
	
	private void btnCalcNetworkActionPerformed(ActionEvent e) {
		numberLayer = 3;
		showPanel();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Создает построитель сетей заданного типа
	 * 
	 * @param networkType Тип сети
	 * @return Построитель сетей
	 */
	public static CANNNetworkBuilder getNetworkBuilder(NetworkType networkType) {
		CANNNetworkBuilder networkBuilder = null;
		
		switch (networkType) {
			case SingleLayerPerceptron:
				networkBuilder = new CANNSingleLayerPerceptronBuilder();
				break;
			case MultiLayerPerceptron:
				networkBuilder = new CANNMultiLayerPerceptronBuilder();
				break;
			case HopfieldNetwork:
				networkBuilder = new CANNHopfieldNetworkBuilder();
				break;
			case HammingNetwork:
				networkBuilder = new CANNHammingNetworkBuilder();
				break;
			case KohonenNetwork:
				networkBuilder = new CANNKohonenNetworkBuilder();
				break;
			default:
				throw new IllegalArgumentException("Неизвестный тип сети");
		}
		
		return networkBuilder;
	}
	
	/**
	 * Отображает панель.
	 */
	private void showPanel() {
		cardLayout.show(pnlCenter, String.valueOf(numberLayer));
	}
	
	@Override
	public void open() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Выбор файла");
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileNameExtensionFilter filter = 
				new FileNameExtensionFilter("Нейронная сеть (*.ann)", "ann");
		fileChooser.setFileFilter(filter);
		int result = fileChooser.showOpenDialog(null);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			filePath = fileChooser.getSelectedFile().toString();
			super.getLblTitle().setText(fileChooser.getSelectedFile().getName());
			neuralNetwork = (NeuralNetwork) DataProcessor.deserialization(filePath);
			update();
		}
	}

	@Override
	public void save() {
		if (filePath == null)
			saveAs();
		else {
			DataProcessor.serialization(filePath, neuralNetwork);
		}
	}

	@Override
	public void saveAs() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Сохранение файла");
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileNameExtensionFilter filter = 
				new FileNameExtensionFilter("Нейронная сеть (*.ann)", "ann");
		fileChooser.setFileFilter(filter);
		int result = fileChooser.showSaveDialog(null);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			filePath = fileChooser.getSelectedFile().toString();
			String fileName = fileChooser.getSelectedFile().getName();
			
			if (!filePath.endsWith(".ann")) {
				filePath += ".ann";
				fileName += ".ann";
			}
			
			DataProcessor.serialization(filePath, neuralNetwork);
			super.getLblTitle().setText(fileName);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private NeuralNetwork neuralNetwork;
	private DataStore dataStore;
	
	private JPanel pnlTop;
	protected JButton btnNetworkStructure;
	protected JButton btnDataNetwork;
	protected JButton btnNetworkTraining;
	protected JButton btnCalcNetwork;
	
	protected JPanel pnlCenter;
	private int numberLayer;
	private CardLayout cardLayout;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущую нейронную сеть.
	 * 
	 * @return Нейронная сеть
	 */
	public NeuralNetwork getNeuralNetwork() {
		return neuralNetwork;
	}
	
	/**
	 * Устанавливает новую нейронную сеть.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 */
	public void setNeuralNetwork(NeuralNetwork neuralNetwork) {
		this.neuralNetwork = neuralNetwork;
		
		update();
	}
	
	/**
	 * Получает текущее хранилище данных.
	 * 
	 * @return Хранилище данных
	 */
	public DataStore getDataStore() {
		return dataStore;
	}
	
	/**
	 * Устанавливает новое хранилище данных.
	 * 
	 * @param dataStore Хранилище данных
	 */
	public void setDataStore(DataStore dataStore) {
		this.dataStore = dataStore;
	}
	
	//</editor-fold>
	
}
