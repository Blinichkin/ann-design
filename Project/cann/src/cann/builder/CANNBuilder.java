/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.builder;

import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 * Компонент построителя.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public abstract class CANNBuilder extends JComponent implements ICANNBuilder {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNBuilder() {
		lblTitle = new JLabel();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	protected String filePath;
	private JLabel lblTitle;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущий заголовок.
	 * 
	 * @return Компонент JLabel
	 */
	public JLabel getLblTitle() {
		return lblTitle;
	}
	
	/**
	 * Устанавливает новый заголовок.
	 * 
	 * @param lblTitle Компонент JLabel
	 */
	public void setLblTitle(JLabel lblTitle) {
		this.lblTitle = lblTitle;
	}
	
	//</editor-fold>
	
}
