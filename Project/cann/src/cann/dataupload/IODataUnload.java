/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.dataupload;

import cann.CANNDataUpload;
import cann.data.DataStore;
import cann.CANNFileLoader;
import cann.CANNFileUnloader;
import java.awt.BorderLayout;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Компонент для выгрузки входных и выходных данных для ИНС.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class IODataUnload extends CANNDataUpload {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает компонент используя стандартные значения.
	 */
	public IODataUnload() {
		this(new DataStore());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param dataStore Хранилище данных
	 */
	public IODataUnload(DataStore dataStore) {
		super(dataStore);
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	@Override
	protected void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		splitPane = new JSplitPane();
		inputLoader = new CANNFileLoader();
		outputUnloader = new CANNFileUnloader();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка splitPane
		{
			splitPane.setResizeWeight(0.5);
			splitPane.setOneTouchExpandable(true);
		}
		
		Border border = BorderFactory.createEtchedBorder();
		Border emptyBorder = new EmptyBorder(5, 5, 5, 5);
		
		// Настройка inputLoader
		{
			Border brdLeftPanel = new CompoundBorder(emptyBorder, 
				BorderFactory.createTitledBorder(border, "Входные данные:"));
			inputLoader.setBorder(brdLeftPanel);
		}
		
		// Настройка outputLoader
		{
			Border brdRightPanel = new CompoundBorder(emptyBorder, 
				BorderFactory.createTitledBorder(border, "Выходные данные:"));
			outputUnloader.setBorder(brdRightPanel);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка splitPane
		{
			splitPane.setLeftComponent(inputLoader);
			splitPane.setRightComponent(outputUnloader);
		}
		
		// Настройка компонента
		{
			setLayout(new BorderLayout());
			add(splitPane, BorderLayout.CENTER);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	@Override
	protected void update() {
		updateInputLoader();
		updateOutputLoader();
	}
	
	/**
	 * Обновляет inputLoader.
	 */
	private void updateInputLoader() {
		inputLoader.setDataTable(getDataStore().getInputData());
	}
	
	/**
	 * Обновляет outputLoader.
	 */
	private void updateOutputLoader() {
		outputUnloader.setDataTable(getDataStore().getOutputData());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JSplitPane splitPane;
	private CANNFileLoader inputLoader;
	private CANNFileUnloader outputUnloader;
	
	//</editor-fold>
	
}
