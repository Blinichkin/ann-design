/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.dataupload;

import cann.CANNDataUpload;
import cann.data.DataStore;
import cann.CANNFileLoader;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.border.Border;

/**
 * Компонент загрузки данных для ИНС.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class IDataUpload extends CANNDataUpload {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает компонент используя стандартные значения.
	 */
	public IDataUpload() {
		this(new DataStore());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param dataStore Хранилище данных
	 */
	public IDataUpload(DataStore dataStore) {
		super(dataStore);
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	@Override
	protected void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		inputLoader = new CANNFileLoader();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка inputLoader
		{
			Border border = BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(), "Входные данные:");
			inputLoader.setBorder(border);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка компонента
		{
			setLayout(new BorderLayout());
			add(inputLoader, BorderLayout.CENTER);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	@Override
	protected void update() {
		updateInputLoader();
	}
	
	/**
	 * Обновляет inputLoader.
	 */
	private void updateInputLoader() {
		inputLoader.setDataTable(getDataStore().getInputData());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private CANNFileLoader inputLoader;
	
	//</editor-fold>
	
}
