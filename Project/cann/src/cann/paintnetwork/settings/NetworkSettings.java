/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.paintnetwork.settings;

import cann.networkview.settings.Network;
import cann.networkview.settings.Link;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.event.ChangeEvent;

/**
 * Компонент для визуального изменения настроек сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class NetworkSettings extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public NetworkSettings() {
		this(new Network());
	}
	
	/**
	 * Создает компонент используя полученные данные.
	 * 
	 * @param network Настройки сети
	 */
	public NetworkSettings(Network network) {
		this.network = network;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		lblNetwork = new JLabel();
		lblColorLink = new JLabel();
		btnColorLink = new JButton();
		lblThickness = new JLabel();
		spnThickness = new JSpinner();
		lblArrowhead = new JLabel();
		chbVisible = new JCheckBox();
		
		lblHorizontalSpan = new JLabel();
		spnHorizontalSpan = new JSpinner();
		lblVerticalSpan = new JLabel();
		spnVerticalSpan = new JSpinner();
		lblNumberNeurons = new JLabel();
		spnNumberNeurons = new JSpinner();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка lblNetwork
		{
			lblNetwork.setHorizontalAlignment(JLabel.CENTER);
			lblNetwork.setOpaque(true);
			lblNetwork.setText("<html>Настройки сети</html>");
		}
		
		// Настройка lblColorLink
		lblColorLink.setText("<html>Цвет связи:</html>");
		
		// Настройка btnColorLink
		btnColorLink.setText("	");
		
		// Настройка lblThickness
		lblThickness.setText("<html>Толщина связи:</html>");
		
		// Настройка spnThickness
		{
			SpinnerNumberModel model = new SpinnerNumberModel(Link.THICKNESS, Link.MIN_THICKNESS, 
					Link.MAX_THICKNESS, Link.STEP_SIZE_THICKNESS);
			spnThickness.setModel(model);
		}
		
		// Настройка lblArrowhead
		lblArrowhead.setText("<html>Видимость направления:</html>");
		
		// Настройка lblHorizontalSpan
		lblHorizontalSpan.setText("<html>Промежуток по горизонтали:</html>");
		
		// Настройка spnHorizontalSpan
		{
			SpinnerNumberModel model = new SpinnerNumberModel(
					Network.HORIZONTAL_SPAN, Network.MIN_HORIZONTAL_SPAN, 
					Network.MAX_HORIZONTAL_SPAN, Network.STEP_SIZE_HORIZONTAL_SPAN);
			spnHorizontalSpan.setModel(model);
		}
		
		// Настройка lblVerticalSpan
		lblVerticalSpan.setText("<html>Промежуток по вертикали:</html>");
		
		// Настройка spnVerticalSpan
		{
			SpinnerNumberModel model = new SpinnerNumberModel(
					Network.VERTICAL_SPAN, Network.MIN_VERTICAL_SPAN, 
					Network.MAX_VERTICAL_SPAN, Network.STEP_SIZE_VERTICAL_SPAN);
			spnVerticalSpan.setModel(model);
		}
		
		// Настройка lblNumberNeurons
		lblNumberNeurons.setText("<html>Количество отображаемых нейронов:</html>");
		
		// Настройка spnNumberNeurons
		{
			SpinnerNumberModel model = new SpinnerNumberModel(
					Network.NUMBER_NEURONS, Network.MIN_NUMBER_NEURONS, 
					Network.MAX_NUMBER_NEURONS, Network.STEP_SIZE_NUMBER_NEURONS);
			spnNumberNeurons.setModel(model);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnColorLink.addActionListener(this::btnColorLinkActionPerformed);
		spnThickness.addChangeListener(this::spnWidthStateChanged);
		spnHorizontalSpan.addChangeListener(this::spnHorizontalSpanStateChanged);
		spnVerticalSpan.addChangeListener(this::spnVerticalSpanStateChanged);
		spnNumberNeurons.addChangeListener(this::spnNumberNeuronsStateChanged);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		setLayout(new GridBagLayout());
		Insets insets = new Insets(2, 2, 2, 2);
		
		add(lblNetwork, new GridBagConstraints(
			0, 0, 2, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		
		add(lblColorLink, new GridBagConstraints(
			0, 1, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(btnColorLink, new GridBagConstraints(
			1, 1, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblThickness, new GridBagConstraints(
			0, 2, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnThickness, new GridBagConstraints(
			1, 2, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblArrowhead, new GridBagConstraints(
			0, 3, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(chbVisible, new GridBagConstraints(
			1, 3, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		
		add(lblHorizontalSpan, new GridBagConstraints(
			0, 4, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnHorizontalSpan, new GridBagConstraints(
			1, 4, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblVerticalSpan, new GridBagConstraints(
			0, 5, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnVerticalSpan, new GridBagConstraints(
			1, 5, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblNumberNeurons, new GridBagConstraints(
			0, 6, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnNumberNeurons, new GridBagConstraints(
			1, 6, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateBtnColorLink();
		updateSpnThickness();
		updateChbVisible();
		updateSpnHorizontalSpan();
		updateSpnVerticalSpan();
		updateSpnNumberNeurons();
	}
	
	/**
	 * Обновляет btnColorLink.
	 */
	private void updateBtnColorLink() {
		Color background = network.getLink().getColor();
		btnColorLink.setBackground(background);
	}
	
	/**
	 * Обновляет spnThickness.
	 */
	private void updateSpnThickness() {
		int value = network.getLink().getWidth();
		spnThickness.setValue(value);
	}
	
	/**
	 * Обновляет chbVisible.
	 */
	private void updateChbVisible() {
		boolean selected = network.getLink().isVisible();
		chbVisible.setSelected(selected);
	}
	
	/**
	 * Обновляет spnHorizontalSpan.
	 */
	private void updateSpnHorizontalSpan() {
		int value = network.getHorizontalSpan();
		spnHorizontalSpan.setValue(value);
	}
	
	/**
	 * Обновляет spnVerticalSpan.
	 */
	private void updateSpnVerticalSpan() {
		int value = network.getVerticalSpan();
		spnVerticalSpan.setValue(value);
	}
	
	/**
	 * Обновляет spnNumberNeurons.
	 */
	private void updateSpnNumberNeurons() {
		int value = network.getNumberNeurons();
		spnNumberNeurons.setValue(value);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnColorLinkActionPerformed(ActionEvent e) {
		Color background = JColorChooser.showDialog(
				this, "Цвет связей", network.getLink().getColor());
		
		network.getLink().setColor(background);
		updateBtnColorLink();
	}
	
	private void spnWidthStateChanged(ChangeEvent e) {
		int value = (int) spnThickness.getValue();
		network.getLink().setWidth(value);
	}
	
	private void spnHorizontalSpanStateChanged(ChangeEvent e) {
		int value = (int) spnHorizontalSpan.getValue();
		network.setHorizontalSpan(value);
	}
	
	private void spnVerticalSpanStateChanged(ChangeEvent e) {
		int value = (int) spnVerticalSpan.getValue();
		network.setVerticalSpan(value);
	}
	
	private void spnNumberNeuronsStateChanged(ChangeEvent e) {
		int value = (int) spnNumberNeurons.getValue();
		network.setNumberNeurons(value);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private Network network;
	
	private JLabel lblNetwork;
	private JLabel lblColorLink;
	private JButton btnColorLink;
	private JLabel lblThickness;
	private JSpinner spnThickness;
	private JLabel lblArrowhead;
	private JCheckBox chbVisible;
	
	private JLabel lblHorizontalSpan;
	private JSpinner spnHorizontalSpan;
	private JLabel lblVerticalSpan;
	private JSpinner spnVerticalSpan;
	private JLabel lblNumberNeurons;
	private JSpinner spnNumberNeurons;

	//</editor-fold>

	//<editor-fold desc="Доступы">

	/**
	 * Получает текущие настройки сети.
	 * 
	 * @return Настройки сети
	 */
	public Network getNetwork() {
		return network;
	}

	/**
	 * Устанавливает новые настройки сети.
	 * 
	 * @param network Настройки сети
	 */
	public void setNetwork(Network network) {
		this.network = network;
		
		update();
	}
	
	//</editor-fold>
	
}
