/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.paintnetwork.settings;

import cann.networkview.settings.Text;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.event.ChangeEvent;

/**
 * Компонент для визуального изменения настроек текста.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class TextSettings extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public TextSettings() {
		this(new Text());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param text Настройки текста
	 */
	public TextSettings(Text text) {
		this.text = text;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		lblText = new JLabel();
		lblVisible = new JLabel();
		chbVisible = new JCheckBox();
		lblSize = new JLabel();
		spnSize = new JSpinner();
		lblColor = new JLabel();
		btnColor = new JButton();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка lblText
		{
			lblText.setHorizontalAlignment(JLabel.CENTER);
			lblText.setOpaque(true);
			lblText.setText("<html>Настройки текста</html>");
		}
		
		// Настройка lblVisible
		lblVisible.setText("<html>Видимость:</html>");
		
		// Настройка lblSize
		lblSize.setText("<html>Размер:</html>");
		
		// Настройка spnSize
		{
			SpinnerNumberModel model = new SpinnerNumberModel(Text.SIZE, Text.MIN_SIZE, 
					Text.MAX_SIZE, Text.STEP_SIZE_SIZE);
			spnSize.setModel(model);
		}
		
		// Настройка lblColor
		lblColor.setText("<html>Цвет:</html>");
		
		// Настройка btnColor
		btnColor.setText("	");
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		chbVisible.addActionListener(this::chbVisibleActionPerformed);
		spnSize.addChangeListener(this::spnSizeStateChanged);
		btnColor.addActionListener(this::btnColorActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		setLayout(new GridBagLayout());
		Insets insets = new Insets(2, 2, 2, 2);
		
		add(lblText, new GridBagConstraints(
			0, 0, 2, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblVisible, new GridBagConstraints(
			0, 1, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(chbVisible, new GridBagConstraints(
			1, 1, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblSize, new GridBagConstraints(
			0, 2, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnSize, new GridBagConstraints(
			1, 2, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblColor, new GridBagConstraints(
			0, 3, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(btnColor, new GridBagConstraints(
			1, 3, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateChbVisible();
		updateSpnSize();
		updateBtnColor();
	}
	
	/**
	 * Обновляет chbVisible.
	 */
	private void updateChbVisible() {
		boolean selected = text.isVisible();
		chbVisible.setSelected(selected);
	}
	
	/**
	 * Обновляет spnSize.
	 */
	private void updateSpnSize() {
		int value = text.getSize();
		spnSize.setValue(value);
	}
	
	/**
	 * Обновляет btnColor.
	 */
	private void updateBtnColor() {
		Color background = text.getColor();
		btnColor.setBackground(background);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void chbVisibleActionPerformed(ActionEvent e) {
		boolean selected = chbVisible.isSelected();
		text.setVisible(selected);
	}
	
	private void spnSizeStateChanged(ChangeEvent e) {
		int value = (int) spnSize.getValue();
		text.setSize(value);
	}
	
	private void btnColorActionPerformed(ActionEvent e) {
		Color background = JColorChooser.showDialog(
				this, "Цвет текста", text.getColor());
		
		text.setColor(background);
		updateBtnColor();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private Text text;
	
	private JLabel lblText;
	private JLabel lblVisible;
	private JCheckBox chbVisible;
	private JLabel lblSize;
	private JSpinner spnSize;
	private JLabel lblColor;
	private JButton btnColor;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">

	/**
	 * Получает текущие настройки текста.
	 * 
	 * @return Настройки текста
	 */
	public Text getText() {
		return text;
	}

	/**
	 * Устанавливает новые настройки текста.
	 * 
	 * @param text Настройки текста
	 */
	public void setText(Text text) {
		this.text = text;
		
		update();
	}
	
	//</editor-fold>
	
}
