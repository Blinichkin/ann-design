/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.paintnetwork.settings;

import cann.networkview.settings.Connector;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;

/**
 * Компонент для визуального изменения настроек соединителя.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class ConnectorSettings extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public ConnectorSettings() {
		this(new Connector());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param connector Настройки соединителя
	 */
	public ConnectorSettings(Connector connector) {
		this.connector = connector;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		lblConnectorSettings = new JLabel();
		
		lblSize = new JLabel();
		lblColor = new JLabel();
	
		spnSize = new JSpinner();
		btnColor = new JButton();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка lblConnectorSettings
		{
			lblConnectorSettings.setHorizontalAlignment(JLabel.CENTER);
			lblConnectorSettings.setOpaque(true);
			lblConnectorSettings.setText("<html>Настройки соединителя</html>");
		}
		
		// Настройка lblSize
		lblSize.setText("<html>Размер:</html>");
		
		// Настройка lblColor
		lblColor.setText("<html>Цвет:</html>");
		
		// Настройка spnSize
		{
			SpinnerNumberModel model = new SpinnerNumberModel(Connector.SIZE, Connector.MIN_SIZE, 
					Connector.MAX_SIZE, Connector.STEP_SIZE_SIZE);
			spnSize.setModel(model);
		}
		
		// Настройка btnBackColor
		btnColor.setText("	");
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		spnSize.addChangeListener(this::spnSizeStateChanged);
		btnColor.addActionListener(this::btnColorActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		setLayout(new GridBagLayout());
		Insets insets = new Insets(1, 1, 1, 1);
		
		add(lblConnectorSettings, new GridBagConstraints(
			0, 0, 2, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		
		add(lblSize, new GridBagConstraints(
			0, 1, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnSize, new GridBagConstraints(
			1, 1, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblColor, new GridBagConstraints(
			0, 2, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(btnColor, new GridBagConstraints(
			1, 2, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateSpnSize();
		updateBtnColor();
	}
	
	/**
	 * Обновляет spnSize.
	 */
	private void updateSpnSize() {
		int value = connector.getSize();
		spnSize.setValue(value);
	}
	
	/**
	 * Обновляет btnColor.
	 */
	private void updateBtnColor() {
		Color background = connector.getColor();
		btnColor.setBackground(background);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void spnSizeStateChanged(ChangeEvent e) {
		int value = (int) spnSize.getValue();
		connector.setSize(value);
	}
	
	private void btnColorActionPerformed(ActionEvent e) {
		Color color = JColorChooser.showDialog(
			this, "Цвет соединителя", connector.getColor()
		);
		
		connector.setColor(color);
		updateBtnColor();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private Connector connector;
	
	private JLabel lblConnectorSettings;
	
	private JLabel lblSize;
	private JLabel lblColor;
	
	private JSpinner spnSize;
	private JButton btnColor;

	//</editor-fold>

	//<editor-fold desc="Доступы">

	/**
	 * Получает текущие настройки соединителя.
	 * 
	 * @return Настройки соединителя
	 */
	public Connector getConnector() {
		return connector;
	}

	/**
	 * Устанавливает новые настройки соединителя.
	 * 
	 * @param connector Настройки соединителя
	 */
	public void setConnector(Connector connector) {
		this.connector = connector;
		
		update();
	}
	
	//</editor-fold>
	
}
