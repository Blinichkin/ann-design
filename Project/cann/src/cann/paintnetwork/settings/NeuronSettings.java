/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.paintnetwork.settings;

import cann.networkview.settings.Neuron;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.event.ChangeEvent;

/**
 * Компонент для визуального изменения настроек нейрона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class NeuronSettings extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public NeuronSettings() {
		this(new Neuron());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param neuron Настройки нейрона
	 */
	public NeuronSettings(Neuron neuron) {
		this.neuron = neuron;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		lblNeuronSettings = new JLabel();
		
		lblSize = new JLabel();
		lblBackColor = new JLabel();
		lblFrontColor = new JLabel();
	
		spnSizeNeuron = new JSpinner();
		btnBackColor = new JButton();
		btnFrontColor = new JButton();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка lblNeuronSettings
		{
			lblNeuronSettings.setHorizontalAlignment(JLabel.CENTER);
			lblNeuronSettings.setOpaque(true);
			lblNeuronSettings.setText("<html>Настройки нейрона</html>");
		}
		
		// Настройка lblSize
		lblSize.setText("<html>Размер:</html>");
		
		// Настройка lblBackColor
		lblBackColor.setText("<html>Цвет заливки:</html>");
		
		// Настройка lblFrontColor
		lblFrontColor.setText("<html>Цвет контура:</html>");
		
		// Настройка spnSizeNeuron
		{
			SpinnerNumberModel model = new SpinnerNumberModel(Neuron.SIZE, Neuron.MIN_SIZE, 
					Neuron.MAX_SIZE, Neuron.STEP_SIZE_SIZE);
			spnSizeNeuron.setModel(model);
		}
		
		// Настройка btnBackColor
		btnBackColor.setText("	");
		
		// Настройка btnFrontColor
		btnFrontColor.setText("	");
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		spnSizeNeuron.addChangeListener(this::spnSizeNeuronStateChanged);
		btnBackColor.addActionListener(this::btnBackColorNeuronActionPerformed);
		btnFrontColor.addActionListener(this::btnFrontColorNeuronActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		setLayout(new GridBagLayout());
		Insets insets = new Insets(1, 1, 1, 1);
		
		add(lblNeuronSettings, new GridBagConstraints(
			0, 0, 2, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		
		add(lblSize, new GridBagConstraints(
			0, 1, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnSizeNeuron, new GridBagConstraints(
			1, 1, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblBackColor, new GridBagConstraints(
			0, 2, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(btnBackColor, new GridBagConstraints(
			1, 2, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblFrontColor, new GridBagConstraints(
			0, 3, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(btnFrontColor, new GridBagConstraints(
			1, 3, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateSpnSizeNeuron();
		updateBtnBackColorNeuron();
		updateBtnFrontColorNeuron();
	}
	
	/**
	 * Обновляет spnSizeNeuron.
	 */
	private void updateSpnSizeNeuron() {
		int value = neuron.getSize();
		spnSizeNeuron.setValue(value);
	}
	
	/**
	 * Обновляет btnBackColor.
	 */
	private void updateBtnBackColorNeuron() {
		Color background = neuron.getBackColor();
		btnBackColor.setBackground(background);
	}
	
	/**
	 * Обновляет btnFrontColor.
	 */
	private void updateBtnFrontColorNeuron() {
		Color background = neuron.getFrontColor();
		btnFrontColor.setBackground(background);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void spnSizeNeuronStateChanged(ChangeEvent e) {
		int value = (int) spnSizeNeuron.getValue();
		neuron.setSize(value);
	}
	
	private void btnBackColorNeuronActionPerformed(ActionEvent e) {
		Color color = JColorChooser.showDialog(
			this, "Задний цвет нейрона", neuron.getBackColor()
		);
		
		neuron.setBackColor(color);
		updateBtnBackColorNeuron();
	}
	
	private void btnFrontColorNeuronActionPerformed(ActionEvent e) {
		Color color = JColorChooser.showDialog(
			this, "Передний цвет нейрона", neuron.getFrontColor()
		);
		
		neuron.setFrontColor(color);
		updateBtnFrontColorNeuron();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private Neuron neuron;
	
	private JLabel lblNeuronSettings;
	
	private JLabel lblSize;
	private JLabel lblBackColor;
	private JLabel lblFrontColor;
	
	private JSpinner spnSizeNeuron;
	private JButton btnBackColor;
	private JButton btnFrontColor;

	//</editor-fold>

	//<editor-fold desc="Доступы">

	/**
	 * Получает текущие настройки нейрона.
	 * 
	 * @return Настройки нейрона
	 */
	public Neuron getNeuron() {
		return neuron;
	}

	/**
	 * Устанавливает новые настройки нейрона.
	 * 
	 * @param neuron Настройки нейрона
	 */
	public void setNeuron(Neuron neuron) {
		this.neuron = neuron;
		
		update();
	}
	
	//</editor-fold>
	
}
