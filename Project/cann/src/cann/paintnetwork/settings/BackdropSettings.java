/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.paintnetwork.settings;

import cann.networkview.settings.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.event.ChangeEvent;

/**
 * Компонент для визуального изменения настроек фона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class BackdropSettings extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public BackdropSettings() {
		this(new Backdrop());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param backdrop Настройки фона
	 */
	public BackdropSettings(Backdrop backdrop) {
		this.backdrop = backdrop;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		lblBackground = new JLabel();
		lblBackgroundColor = new JLabel();
		btnBackgroundColor = new JButton();
		
		lblGrid = new JLabel();
		lblColorGrid = new JLabel();
		btnColorGrid = new JButton();
		lblInterval = new JLabel();
		spnInterval = new JSpinner();
		lblLineThickness = new JLabel();
		spnLineThickness = new JSpinner();
		lblVisibleGrid = new JLabel();
		chbVisibleGrid = new JCheckBox();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка lblBackground
		{
			lblBackground.setHorizontalAlignment(JLabel.CENTER);
			lblBackground.setOpaque(true);
			lblBackground.setText("<html>Настройки фона</html>");
		}
		
		// Настройка lblBackgroundColor
		lblBackgroundColor.setText("<html>Цвет:</html>");
		
		// Настройка btnBackground
		btnBackgroundColor.setText("	");
		
		// Настройка lblGrid
		{
			lblGrid.setHorizontalAlignment(JLabel.CENTER);
			lblGrid.setOpaque(true);
			lblGrid.setText("<html>Настройки сетки</html>");
		}
		
		// Настройка lblColorGrid
		lblColorGrid.setText("<html>Цвет:</html>");
	
		// Настройка btnColorGrid
		btnColorGrid.setText("	");
	
		// Настройка lblInterval
		lblInterval.setText("<html>Интервал:</html>");
		
		// Настройка spnInterval
		{
			SpinnerNumberModel model = new SpinnerNumberModel(Grid.INTERVAL, Grid.MIN_INTERVAL, 
					Grid.MAX_INTERVAL, Grid.STEP_SIZE_INTERVAL);
			spnInterval.setModel(model);
		}
		
		// Настройка lblLineThickness
		lblLineThickness.setText("<html>Толщина:</html>");
	
		// Настройка spnLineThickness
		{
			SpinnerNumberModel model = new SpinnerNumberModel(Grid.THICKNESS, Grid.MIN_THICKNESS, 
					Grid.MAX_THICKNESS, Grid.STEP_SIZE_THICKNESS);
			spnLineThickness.setModel(model);
		}
		
		// Настройка lblVisibleGrid
		lblVisibleGrid.setText("<html>Видимость:</html>");
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnBackgroundColor.addActionListener(this::btnBackgroundColorActionPerformed);
		btnColorGrid.addActionListener(this::btnColorGridActionPerformed);
		spnInterval.addChangeListener(this::spnIntervalStateChanged);
		spnLineThickness.addChangeListener(this::spnLineThicknessStateChanged);
		chbVisibleGrid.addActionListener(this::chbVisibleGridActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		setLayout(new GridBagLayout());
		Insets insets = new Insets(2, 2, 2, 2);
		
		add(lblBackground, new GridBagConstraints(
			0, 0, 2, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblBackgroundColor, new GridBagConstraints(
			0, 1, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(btnBackgroundColor, new GridBagConstraints(
			1, 1, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		
		add(lblGrid, new GridBagConstraints(
			0, 2, 2, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblColorGrid, new GridBagConstraints(
			0, 3, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(btnColorGrid, new GridBagConstraints(
			1, 3, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblInterval, new GridBagConstraints(
			0, 4, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnInterval, new GridBagConstraints(
			1, 4, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblLineThickness, new GridBagConstraints(
			0, 5, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(spnLineThickness, new GridBagConstraints(
			1, 5, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(lblVisibleGrid, new GridBagConstraints(
			0, 6, 1, 1, 0, 0, 
			GridBagConstraints.WEST, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(chbVisibleGrid, new GridBagConstraints(
			1, 6, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateBtnBackgroundColor();
		updateBtnColorGrid();
		updateSpnInterval();
		updateSpnLineWidth();
		updateChbVisibleGrid();
	}
	
	/**
	 * Обновляет btnBackgroundColor.
	 */
	private void updateBtnBackgroundColor() {
		Color background = backdrop.getColor();
		btnBackgroundColor.setBackground(background);
	}
	
	/**
	 * Обновляет btnColorGrid.
	 */
	private void updateBtnColorGrid() {
		Color background = backdrop.getGrid().getColor();
		btnColorGrid.setBackground(background);
	}
	
	/**
	 * Обновляет spnInterval.
	 */
	private void updateSpnInterval() {
		int value = backdrop.getGrid().getInterval();
		spnInterval.setValue(value);
	}
	
	/**
	 * Обновляет spnLineThickness.
	 */
	private void updateSpnLineWidth() {
		double value = backdrop.getGrid().getThickness();
		spnLineThickness.setValue(value);
	}
	
	/**
	 * Обновляет chbVisibleGrid.
	 */
	private void updateChbVisibleGrid() {
		boolean selected = backdrop.getGrid().isVisible();
		chbVisibleGrid.setSelected(selected);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnBackgroundColorActionPerformed(ActionEvent e) {
		Color background = JColorChooser.showDialog(
			this, "Цвет фона", backdrop.getColor()
		);
		
		backdrop.setColor(background);
		updateBtnBackgroundColor();
	}
	
	private void btnColorGridActionPerformed(ActionEvent e) {
		Color background = JColorChooser.showDialog(
			this, "Цвет сетки", backdrop.getGrid().getColor()
		);
		
		backdrop.getGrid().setColor(background);
		updateBtnColorGrid();
	}
	
	private void spnIntervalStateChanged(ChangeEvent e) {
		int value = (int) spnInterval.getValue();
		backdrop.getGrid().setInterval(value);
	}
	
	private void spnLineThicknessStateChanged(ChangeEvent e) {
		float value = ((Double) spnLineThickness.getValue()).floatValue();
		backdrop.getGrid().setThickness(value);
	}
	
	private void chbVisibleGridActionPerformed(ActionEvent e) {
		boolean selected = chbVisibleGrid.isSelected();
		backdrop.getGrid().setVisible(selected);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private Backdrop backdrop;
	
	private JLabel lblBackground;
	private JLabel lblBackgroundColor;
	private JButton btnBackgroundColor;
	
	private JLabel lblGrid;
	private JLabel lblColorGrid;
	private JButton btnColorGrid;
	private JLabel lblInterval;
	private JSpinner spnInterval;
	private JLabel lblLineThickness;
	private JSpinner spnLineThickness;
	private JLabel lblVisibleGrid;
	private JCheckBox chbVisibleGrid;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">

	/**
	 * Получает текущие настройки фона.
	 * 
	 * @return Настройки фона
	 */
	public Backdrop getBackdrop() {
		return backdrop;
	}

	/**
	 * Устанавливает новые настройки фона.
	 * 
	 * @param backdrop Настройки фона
	 */
	public void setBackdrop(Backdrop backdrop) {
		this.backdrop = backdrop;
		
		update();
	}
	
	//</editor-fold>

}
