/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.paintnetwork;

import cann.paintnetwork.settings.*;
import cann.networkview.settings.*;
import cann.networkview.Settings;
import cann.other.Tools;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Диалоговое окно настроек.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class SettingsDialog extends JDialog {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартное диалоговое окно.
	 */
	public SettingsDialog() {
		this(new Settings(), (Frame) null, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param settings Настройки
	 */
	public SettingsDialog(Settings settings) {
		this(settings, (Frame) null, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param settings Настройки
	 * @param owner Владелец
	 */
	public SettingsDialog(Settings settings, Frame owner) {
		this(settings, owner, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param settings Настройки
	 * @param owner Владелец
	 * @param modal Модальность
	 */
	public SettingsDialog(Settings settings, Frame owner, boolean modal) {
		this(settings, owner, "Настройки", modal);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param settings Настройки
	 * @param owner Владелец
	 * @param title Заголовок
	 */
	public SettingsDialog(Settings settings, Frame owner, String title) {
		this(settings, owner, title, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param settings Настройки
	 * @param owner Владелец
	 * @param title Заголовок
	 * @param modal Модальность
	 */
	public SettingsDialog(Settings settings, Frame owner, String title, boolean modal) {
		super(owner, title, modal, null);
		this.settings = settings;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		tabbedPane = new JTabbedPane();
		backdropSettings = new BackdropSettings();
		networkSettings = new NetworkSettings();
		connectorSettings = new ConnectorSettings();
		neuronSettings = new NeuronSettings();
		textSettings = new TextSettings();
		
		pnlBottom = new JPanel();
		btnOk = new JButton();
		btnCancel = new JButton();
		btnReset = new JButton();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		// Настройка btnOk
		btnOk.setText("OK");
		
		// Настройка btnCancel
		btnCancel.setText("Отмена");
		
		// Настройка btnReset
		btnReset.setText("Сброс");
		
		// Настройка pnlBottom
		{
			Border border = new EmptyBorder(10, 10, 10, 10);
			pnlBottom.setBorder(border);
		}
		
		// Настройка tabbedPane
		{
			Border border = new EmptyBorder(10, 10, 0, 10);
			
			tabbedPane.addTab("Фон", backdropSettings);
			tabbedPane.addTab("Сеть", networkSettings);
			tabbedPane.addTab("Соединитель", connectorSettings);
			tabbedPane.addTab("Нейрон", neuronSettings);
			tabbedPane.addTab("Текст", textSettings);
			tabbedPane.setBorder(border);
			tabbedPane.setTabPlacement(JTabbedPane.LEFT);
		}
		
		// Настройка диалогового окна
		{
			setIconImage(tools.getIcon("settings.png").getImage());
			setSize(500, 400);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnOk.addActionListener(this::btnOkActionPerformed);
		btnCancel.addActionListener(this::btnCancelActionPerformed);
		btnReset.addActionListener(this::btnResetActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		//Настройка pnlBottom
		{
			pnlBottom.setLayout(new GridBagLayout());
			Insets insets = new Insets(1, 1, 1, 1);
		
			pnlBottom.add(btnOk, new GridBagConstraints(
				1, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
		
			pnlBottom.add(btnCancel, new GridBagConstraints(
				2, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
		
			pnlBottom.add(btnReset, new GridBagConstraints(
				3, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
		}
		
		//Настройка диалогового окна
		{
			setLayout(new BorderLayout());

			add(tabbedPane, BorderLayout.CENTER);
			add(pnlBottom, BorderLayout.SOUTH);
		}
		
		setLocationRelativeTo(null);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateBackdropSettings();
		updateNetworkSettings();
		updateConnectorSettings();
		updateNeuronSettings();
		updateTextSettings();
	}
	
	/**
	 * Обновляет backdropSettings.
	 */
	private void updateBackdropSettings() {
		Backdrop backdrop = settings.getBackdrop();
		backdropSettings.setBackdrop(backdrop);
	}
	
	/**
	 * Обновляет networkSettings.
	 */
	private void updateNetworkSettings() {
		Network network = settings.getNetwork();
		networkSettings.setNetwork(network);
	}
	
	/**
	 * Обновляет connectorSettings.
	 */
	private void updateConnectorSettings() {
		Connector connector = settings.getConnector();
		connectorSettings.setConnector(connector);
	}
	
	/**
	 * Обновляет neuronSettings.
	 */
	private void updateNeuronSettings() {
		Neuron neuron = settings.getNeuron();
		neuronSettings.setNeuron(neuron);
	}
	
	/**
	 * Обновляет textSettings.
	 */
	private void updateTextSettings() {
		Text text = settings.getText();
		textSettings.setText(text);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnOkActionPerformed(ActionEvent e) {
		Save();
		dialogResult = true;
		dispose();
	}
	
	private void btnCancelActionPerformed(ActionEvent e) {
		dispose();
	}
	
	private void btnResetActionPerformed(ActionEvent e) {
		settings = new Settings();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Сохранение настроек.
	 */
	private void Save() {
		settings.setBackdrop(backdropSettings.getBackdrop());
		settings.setNetwork(networkSettings.getNetwork());
		settings.setConnector(connectorSettings.getConnector());
		settings.setNeuron(neuronSettings.getNeuron());
		settings.setText(textSettings.getText());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JTabbedPane tabbedPane;
	private BackdropSettings backdropSettings;
	private NetworkSettings networkSettings;
	private ConnectorSettings connectorSettings;
	private NeuronSettings neuronSettings;
	private TextSettings textSettings;

	private JPanel pnlBottom;
	private JButton btnOk;
	private JButton btnReset;
	private JButton btnCancel;
	
	private boolean dialogResult;
	private Settings settings;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущие настройки.
	 * 
	 * @return Настройки
	 */
	public Settings getSettings() {
		return settings;
	}
	
	/**
	 * Устанавливает новые настройки.
	 * 
	 * @param settings 
	 */
	public void setSettings(Settings settings) {
		this.settings = settings;
		
		update();
	}
	
	/**
	 * Получает текущее значение результата диалогового окна.
	 * 
	 * @return Результат диалогового окна
	 */
	public boolean isDialogResult() {
		return dialogResult;
	}
	
	//</editor-fold>
	
}
