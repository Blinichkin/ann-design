/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import ann.NeuralNetwork;
import ann.structure.NetworkType;
import cann.builder.*;
import cann.data.DataTable;
import cann.other.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import jhelper.DataProcessor;

/**
 * Компонент для визуальной работы с нейронными сетями.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNWorkNetwork extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNWorkNetwork() {
		init();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		toolBar = new JToolBar();
		btnCreateData = new JButton();
		btnCreateNetwork = new JButton();
		btnOpen = new JButton();
		btnSave = new JButton();
		btnSaveAs = new JButton();
		
		tabbedPane = new JTabbedPane();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		// Настройка btnCreateData
		{
			btnCreateData.setIcon(tools.getIcon("data.png"));
			btnCreateData.setToolTipText("Создать данные");
		}
		
		// Настройка btnCreateNetwork
		{
			btnCreateNetwork.setIcon(tools.getIcon("center.png"));
			btnCreateNetwork.setToolTipText("Создать сеть");
		}
		
		// Настройка btnOpen
		{
			btnOpen.setIcon(tools.getIcon("folder2.png"));
			btnOpen.setToolTipText("Открыть файл");
		}
		
		// Настройка btnSave
		{
			btnSave.setIcon(tools.getIcon("save.png"));
			btnSave.setToolTipText("Сохранить файл");
		}
		
		// Настройка btnSaveAs
		{
			btnSaveAs.setIcon(tools.getIcon("saveAs.png"));
			btnSaveAs.setToolTipText("Сохранить файл как ...");
		}
		
		// Настройка toolBar
		toolBar.setOrientation(JToolBar.VERTICAL);
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnCreateData.addActionListener(this::btnCreateDataActionPerformed);
		btnCreateNetwork.addActionListener(this::btnCreateNetworkActionPerformed);
		btnOpen.addActionListener(this::btnOpenActionPerformed);
		btnSave.addActionListener(this::btnSaveActionPerformed);
		btnSaveAs.addActionListener(this::btnSaveAsActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка toolBar
		{
			toolBar.add(btnCreateData);
			toolBar.add(btnCreateNetwork);
			toolBar.add(btnOpen);
			toolBar.addSeparator();
			toolBar.add(btnSave);
			toolBar.add(btnSaveAs);
		}
		
		// Настройка компонента
		{
			setLayout(new BorderLayout());
			
			add(toolBar, BorderLayout.WEST);
			add(tabbedPane, BorderLayout.CENTER);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnCreateDataActionPerformed(ActionEvent e) {
		createData();
	}
	
	private void btnCreateNetworkActionPerformed(ActionEvent e) {
		createNetwork();
	}
	
	private void btnOpenActionPerformed(ActionEvent e) {
		open();
	}
	
	private void btnSaveActionPerformed(ActionEvent e) {
		save();
	}
	
	private void btnSaveAsActionPerformed(ActionEvent e) {
		saveAs();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Создает данные.
	 */
	public void createData() {
		createDataBuilder(new DataTable(), null);
	}
	
	/**
	 * Создает новую вкладку для работы с данными.
	 */
	private void createDataBuilder(DataTable dataTable, String fileName) {
		CANNDataBuilder dataBuilder = new CANNDataBuilder(dataTable);
		if (fileName != null)
			dataBuilder.getLblTitle().setText(fileName);
		Header header = new Header(tabbedPane, dataBuilder.getLblTitle(), dataBuilder);
		tabbedPane.add(dataBuilder);
		
		int index = tabbedPane.indexOfComponent(dataBuilder);
		tabbedPane.setTabComponentAt(index, header);
		tabbedPane.setSelectedIndex(index);
	}
	
	/**
	 * Открывает диалоговое окно с выбором типа сети.
	 */
	public void createNetwork() {
		NetworkType networkType = NetworkType.SingleLayerPerceptron;
		CANNNetworkDialog networkDialog = new CANNNetworkDialog(networkType, (Frame) null, true);
		networkDialog.setVisible(true);
		
		if (networkDialog.isResultDialog()) {
			networkType = networkDialog.getNetworkType();
			NeuralNetwork neuralNetwork = NeuralNetwork.create(networkType);
			createNetworkBuilder(neuralNetwork, null);
		}
	}
	
	/**
	 * Создает новую вкладку для работы с нейронной сетью.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 */
	private void createNetworkBuilder(NeuralNetwork neuralNetwork, String fileName) {
		NetworkType networkType = neuralNetwork.getNetworkType();
		CANNNetworkBuilder networkBuilder = CANNNetworkBuilder.getNetworkBuilder(networkType);
		if (fileName != null)
			networkBuilder.getLblTitle().setText(fileName);
		Header header = new Header(tabbedPane, networkBuilder.getLblTitle(), networkBuilder);
			
		networkBuilder.setNeuralNetwork(neuralNetwork);
		tabbedPane.add(networkBuilder);
			
		int index = tabbedPane.indexOfComponent(networkBuilder);
		tabbedPane.setTabComponentAt(index, header);
		tabbedPane.setSelectedIndex(index);
	}
	
	/**
	 * Открывает диалоговое окно с выбором файлов.
	 */
	public void open() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Выбор файла");
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int result = fileChooser.showOpenDialog(null);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			String filePath = fileChooser.getSelectedFile().toString();
			String fileName = fileChooser.getSelectedFile().getName();
			
			if (filePath.endsWith(".dat")) {
				DataTable dataTable = (DataTable) DataProcessor.deserialization(filePath);
				createDataBuilder(dataTable, fileName);
			} else if (filePath.endsWith(".ann")) {
				NeuralNetwork neuralNetwork = 
						(NeuralNetwork) DataProcessor.deserialization(filePath);
				createNetworkBuilder(neuralNetwork, fileName);
			} else {
				JOptionPane.showMessageDialog(this, "Файл не распознан", "Ошибка", 
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	/**
	 * Сохраняет файл.
	 */
	public void save() {
		CANNBuilder builder = (CANNBuilder) tabbedPane.getSelectedComponent();
		builder.save();
	}
	
	/**
	 * Сохраняет файл как ...
	 */
	public void saveAs() {
		CANNBuilder builder = (CANNBuilder) tabbedPane.getSelectedComponent();
		builder.saveAs();
	}
	
	/**
	 * Сохраняет все файлы.
	 */
	public void saveAll() {
		for (Component component : tabbedPane.getComponents()) {
			CANNBuilder builder = (CANNBuilder) component;
			builder.save();
		}
	}
	
	/**
	 * Закрывает вкладку.
	 */
	public void close() {
		int index = tabbedPane.getSelectedIndex();
		
		if (index >= 0)
			tabbedPane.remove(index);
	}
	
	/**
	 * Закрывает все вкладки.
	 */
	public void closeAll() {
		tabbedPane.removeAll();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JToolBar toolBar;
	private JButton btnCreateData;
	private JButton btnCreateNetwork;
	private JButton btnOpen;
	private JButton btnSave;
	private JButton btnSaveAs;
	
	private JTabbedPane tabbedPane;
	
	//</editor-fold>
	
}
