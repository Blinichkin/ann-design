/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import cann.other.Tools;
import cann.data.DataTable;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import jhelper.DataProcessor;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Компонент для загрузки данных.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNFileLoader extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNFileLoader() {
		this(new DataTable());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param dataTable Таблица данных
	 */
	public CANNFileLoader(DataTable dataTable) {
		this.dataTable = dataTable;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		tfPath = new JTextField();
		btnChoose = new JButton();
		btnUpload = new JButton();
		
		tableModel = new DefaultTableModel();
		tblData = new JTable();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		// Настройка btnChoose
		{
			btnChoose.setIcon(tools.getIcon("folder.png"));
			btnChoose.setToolTipText("Выбор файла");
		}
		
		// Настройка btnUpload
		{
			btnUpload.setIcon(tools.getIcon("upload.png"));
			btnUpload.setToolTipText("Загрузка файла");
		}
		
		// Настройка tblData
		{
			tblData.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			tblData.setEnabled(false);
			tblData.setModel(tableModel);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnChoose.addActionListener(this::btnChooseActionPerformed);
		btnUpload.addActionListener(this::btnUploadActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		setLayout(new GridBagLayout());
		Insets insets = new Insets(2, 2, 2, 2);
			
		add(tfPath, new GridBagConstraints(
			0, 0, 1, 1, 1, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
		add(btnChoose, new GridBagConstraints(
			1, 0, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.NONE, 
			insets, 0, 0)
		);
		add(btnUpload, new GridBagConstraints(
			2, 0, 1, 1, 0, 0, 
			GridBagConstraints.CENTER, GridBagConstraints.NONE, 
			insets, 0, 0)
		);
		
		add(new JScrollPane(tblData), new GridBagConstraints(
			0, 1, 3, 1, 1, 1, 
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
			insets, 0, 0)
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateTblData();
	}
	
	/**
	 * Обновляет tblData.
	 */
	private void updateTblData() {
		if (dataTable != null)
			tableModel.setDataVector(dataTable.getData(), dataTable.getHeaders());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnChooseActionPerformed(ActionEvent e) {
		chooseFile();
		fileLoading();
	}
	
	private void btnUploadActionPerformed(ActionEvent e) {
		fileLoading();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выбор файла.
	 */
	private void chooseFile() {
		JFileChooser fileChooser = new JFileChooser();
		int result = fileChooser.showOpenDialog(this);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			String fillPath = fileChooser.getSelectedFile().getPath();
			tfPath.setText(fillPath);
		}
    }
	
	/**
	 * Загрузка файла.
	 */
	private void fileLoading() {
		String path = tfPath.getText();
		if (check(path)) return;
		
		DataTable data = (DataTable) DataProcessor.deserialization(path);
		dataTable.setTypeData(data.getTypeData());
		dataTable.setHeaders(data.getHeaders());
		dataTable.setData(data.getData());
		updateTblData();
    }
	
	/**
	 * Проверка.
	 * 
	 * @param path Путь к файлу
	 * @return Результат
	 */
	private boolean check(String path) {
		if (path.equals("")) {
			JOptionPane.showMessageDialog(this, "Задан пустой путь к файлу", "Ошибка", 
					JOptionPane.ERROR_MESSAGE);
			return true;
		}
		
		File file = new File(path);
		if (!file.exists()) {
			JOptionPane.showMessageDialog(this, "Файл не найден", "Ошибка", 
					JOptionPane.ERROR_MESSAGE);
			return true;
		}
		
		return false;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private DataTable dataTable;
	
	private JTextField tfPath;
	private JButton btnChoose;
	private JButton btnUpload;
	
	private DefaultTableModel tableModel;
	private JTable tblData;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущие данные сети.
	 * 
	 * @return Таблица данных
	 */
	public DataTable getDataTable() {
		return dataTable;
	}
	
	/**
	 * Устанавливает новые данные сети.
	 * 
	 * @param dataTable Таблица данных
	 */
	public void setDataTable(DataTable dataTable) {
		this.dataTable = dataTable;
		
		updateTblData();
	}
	
	//</editor-fold>
	
}
