/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.editornetwork;

import ann.model.HopfieldNetworkModel;
import java.awt.*;
import jhelper.model.IntModel;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;

/**
 * Компонент для визуального редактирования модели сети Хопфилда.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class EditorHopfieldNetworkModel extends EditorModel {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public EditorHopfieldNetworkModel() {
		this(new HopfieldNetworkModel());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param model Модель сети Хопфилда
	 */
	public EditorHopfieldNetworkModel(HopfieldNetworkModel model) {
		super(model);
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	@Override
	protected void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		pnlNumberNeurons = new JPanel();
		snmNumberNeurons = new SpinnerNumberModel();
		spnNumberNeurons = new JSpinner();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка pnlNumberNeurons
		{
			Border border = BorderFactory.createTitledBorder("Количество нейронов:");
			
			pnlNumberNeurons.setBorder(border);
		}
		
		// Настройка spnNumberNeurons
		spnNumberNeurons.setModel(snmNumberNeurons);
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		snmNumberNeurons.addChangeListener(this::spnNumberNeuronsStateChanged);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlNumberNeurons
		{
			pnlNumberNeurons.setLayout(new BorderLayout());
			
			pnlNumberNeurons.add(spnNumberNeurons, BorderLayout.CENTER);
		}
		
		// Настройка компонента
		{
			setLayout(new GridBagLayout());
			
			add(pnlNumberNeurons, new GridBagConstraints(
				0, 1, 1, 1, 1, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	@Override
	protected void update() {
		updateSnmNumberNeurons();
	}
	
	/**
	 * Обновляет snmNumberNeurons.
	 */
	private void updateSnmNumberNeurons() {
		HopfieldNetworkModel model = (HopfieldNetworkModel) getNetworkModel();
		IntModel numberNeurons = model.getNumberNeurons();
		
		int value = numberNeurons.getValue();
		int minValue = numberNeurons.getMinValue();
		int maxValue = numberNeurons.getMaxValue();
		
		snmNumberNeurons.setValue(value);
		snmNumberNeurons.setMinimum(minValue);
		snmNumberNeurons.setMaximum(maxValue);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void spnNumberNeuronsStateChanged(ChangeEvent e) {
		int value = (int) spnNumberNeurons.getValue();
		HopfieldNetworkModel model = (HopfieldNetworkModel) getNetworkModel();
		model.setNumberNeurons(value);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JPanel pnlNumberNeurons;
	private SpinnerNumberModel snmNumberNeurons;
	private JSpinner spnNumberNeurons;
	
	//</editor-fold>
	
}
