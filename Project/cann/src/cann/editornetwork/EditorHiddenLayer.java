/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.editornetwork;

import ann.hiddenlayer.HiddenLayer;
import java.awt.*;
import java.awt.event.ActionEvent;
import jhelper.model.IntModel;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;

/**
 * Компонент для визуального редактирования скрытого слоя.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class EditorHiddenLayer extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public EditorHiddenLayer() {
		this(new HiddenLayer());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param hiddenLayer Скрытый слой
	 */
	public EditorHiddenLayer(HiddenLayer hiddenLayer) {
		this.hiddenLayer = hiddenLayer;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		pnlNumberLayers = new JPanel();
		snmNumberLayers = new SpinnerNumberModel();
		spnNumberLayers = new JSpinner();
		
		pnlNumberLayer = new JPanel();
		cobNumberLayer = new JComboBox<>();
		
		pnlNumberNeurons = new JPanel();
		snmNumberNeurons = new SpinnerNumberModel();
		spnNumberNeurons = new JSpinner();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка pnlNumberLayers
		{
			Border border = BorderFactory.createTitledBorder("Количество слоев:");
			
			pnlNumberLayers.setBorder(border);
		}
		
		// Настройка spnNumberLayers
		spnNumberLayers.setModel(snmNumberLayers);
		
		// Настройка pnlNumberLayer
		{
			Border border = BorderFactory.createTitledBorder("Номер слоя:");
			
			pnlNumberLayer.setBorder(border);
		}
		
		// Настройка cobNumberLayer
		cobNumberLayer.setMaximumRowCount(0);
		
		// Настройка pnlNumberNeurons
		{
			Border border = BorderFactory.createTitledBorder("Количество нейронов:");
			
			pnlNumberNeurons.setBorder(border);
		}
		
		// Настройка spnNumberNeurons
		spnNumberNeurons.setModel(snmNumberNeurons);
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		snmNumberLayers.addChangeListener(this::spnNumberLayersStateChanged);
		cobNumberLayer.addActionListener(this::cobNumberLayerActionPerformed);
		snmNumberNeurons.addChangeListener(this::spnNumberNeuronsStateChanged);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlNumberLayers
		{
			pnlNumberLayers.setLayout(new BorderLayout());
			
			pnlNumberLayers.add(spnNumberLayers, BorderLayout.CENTER);
		}
		
		// Настройка pnlNumberLayer
		{
			pnlNumberLayer.setLayout(new BorderLayout());
			
			pnlNumberLayer.add(cobNumberLayer, BorderLayout.CENTER);
		}
		
		// Настройка pnlNumberNeurons
		{
			pnlNumberNeurons.setLayout(new BorderLayout());
			
			pnlNumberNeurons.add(spnNumberNeurons, BorderLayout.CENTER);
		}
		
		// Настройка компонента
		{
			setLayout(new GridBagLayout());
			
			add(pnlNumberLayers, new GridBagConstraints(
				0, 1, 1, 1, 1, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
			add(pnlNumberLayer, new GridBagConstraints(
				0, 2, 1, 1, 1, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
			add(pnlNumberNeurons, new GridBagConstraints(
				0, 3, 1, 1, 1, 1, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateSnmNumberLayers();
		updateCobNumberLayer();
		updateSnmNumberNeurons();
	}
	
	/**
	 * Обновляет snmNumberLayers.
	 */
	private void updateSnmNumberLayers() {
		IntModel numberLayers = hiddenLayer.getNumberLayers();
		
		int value = numberLayers.getValue();
		int minValue = numberLayers.getMinValue();
		int maxValue = numberLayers.getMaxValue();

		snmNumberLayers.setValue(value);
		snmNumberLayers.setMinimum(minValue);
		snmNumberLayers.setMaximum(maxValue);
	}
	
	/**
	 * Обновляет cobNumberLayer.
	 */
	private void updateCobNumberLayer() {
		int oldRowCount = cobNumberLayer.getMaximumRowCount();
		int newRowCount = hiddenLayer.getNumberLayers().getValue();
		cobNumberLayer.setMaximumRowCount(newRowCount);
		
		for (int i = oldRowCount; i < newRowCount; i++)
			cobNumberLayer.addItem(i + 1);
	}
	
	/**
	 * Обновляет snmNumberNeurons.
	 */
	private void updateSnmNumberNeurons() {
		int index = cobNumberLayer.getSelectedIndex();
		
		if (index >= 0) {
			IntModel layer = hiddenLayer.getLayers().get(index);
			
			int value = layer.getValue();
			int minValue = layer.getMinValue();
			int maxValue = layer.getMaxValue();
		
			snmNumberNeurons.setValue(value);
			snmNumberNeurons.setMinimum(minValue);
			snmNumberNeurons.setMaximum(maxValue);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void spnNumberLayersStateChanged(ChangeEvent e) {
		int value = (int) spnNumberLayers.getValue();
		hiddenLayer.setNumberLayers(value);
		
		updateCobNumberLayer();
	}
	
	private void cobNumberLayerActionPerformed(ActionEvent e) {
		updateSnmNumberNeurons();
	}
	
	private void spnNumberNeuronsStateChanged(ChangeEvent e) {
		int value = (int) spnNumberNeurons.getValue();
		int index = cobNumberLayer.getSelectedIndex();
		
		if (index >= 0) 
			hiddenLayer.getLayers().get(index).setValue(value);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private HiddenLayer hiddenLayer;
	
	private JPanel pnlNumberLayers;
	private SpinnerNumberModel snmNumberLayers;
	private JSpinner spnNumberLayers;
	
	private JPanel pnlNumberLayer;
	private JComboBox<Integer> cobNumberLayer;
	
	private JPanel pnlNumberNeurons;
	private SpinnerNumberModel snmNumberNeurons;
	private JSpinner spnNumberNeurons;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получить текущий скрытый слой.
	 * 
	 * @return Скрытый слой
	 */
	public HiddenLayer getHiddenLayer() {
		return hiddenLayer;
	}
	
	/**
	 * Установить новый скрытый слой.
	 * 
	 * @param hiddenLayer Скрытый слой
	 */
	public void setHiddenLayer(HiddenLayer hiddenLayer) {
		this.hiddenLayer = hiddenLayer;
		
		update();
	}
	
	//</editor-fold>
	
}
