/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.editornetwork;

import ann.model.KohonenNetworkModel;
import java.awt.*;
import jhelper.model.IntModel;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;

/**
 * Компонент для визуального редактирования модели сети Кохонена.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class EditorKohonenNetworkModel extends EditorModel {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public EditorKohonenNetworkModel() {
		this(new KohonenNetworkModel());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param model Модель сети Кохонена
	 */
	public EditorKohonenNetworkModel(KohonenNetworkModel model) {
		super(model);
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	@Override
	protected void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		pnlNumberInputs = new JPanel();
		snmNumberInputs = new SpinnerNumberModel();
		spnNumberInputs = new JSpinner();
		
		pnlNumberNeurons = new JPanel();
		snmNumberNeurons = new SpinnerNumberModel();
		spnNumberNeurons = new JSpinner();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка pnlNumberInputs
		{
			Border border = BorderFactory.createTitledBorder("Количество входов:");
			
			pnlNumberInputs.setBorder(border);
		}
		
		// Настройка spnNumberInputs
		spnNumberInputs.setModel(snmNumberInputs);
		
		// Настройка pnlNumberNeurons
		{
			Border border = BorderFactory.createTitledBorder("Количество нейронов:");
			
			pnlNumberNeurons.setBorder(border);
		}
		
		// Настройка spnNumberNeurons
		spnNumberNeurons.setModel(snmNumberNeurons);
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		snmNumberInputs.addChangeListener(this::spnNumberInputsStateChanged);
		snmNumberNeurons.addChangeListener(this::spnNumberNeuronsStateChanged);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlNumberInputs
		{
			pnlNumberInputs.setLayout(new BorderLayout());
			
			pnlNumberInputs.add(spnNumberInputs, BorderLayout.CENTER);
		}
		
		// Настройка pnlNumberNeurons
		{
			pnlNumberNeurons.setLayout(new BorderLayout());
			
			pnlNumberNeurons.add(spnNumberNeurons, BorderLayout.CENTER);
		}
		
		// Настройка компонента
		{
			setLayout(new GridBagLayout());
			
			add(pnlNumberInputs, new GridBagConstraints(
				0, 1, 1, 1, 1, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
			add(pnlNumberNeurons, new GridBagConstraints(
				0, 2, 1, 1, 1, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	@Override
	protected void update() {
		updateSnmNumberInputs();
		updateSnmNumberNeurons();
	}
	
	/**
	 * Обновляет snmNumberInputs.
	 */
	private void updateSnmNumberInputs() {
		IntModel numberInputs = getNetworkModel().getNumberInputs();
		
		int value = numberInputs.getValue();
		int minValue = numberInputs.getMinValue();
		int maxValue = numberInputs.getMaxValue();
		
		snmNumberInputs.setValue(value);
		snmNumberInputs.setMinimum(minValue);
		snmNumberInputs.setMaximum(maxValue);
	}
	
	/**
	 * Обновляет snmNumberNeurons.
	 */
	private void updateSnmNumberNeurons() {
		KohonenNetworkModel model = (KohonenNetworkModel) getNetworkModel();
		IntModel numberNeurons = model.getNumberNeurons();
		
		int value = numberNeurons.getValue();
		int minValue = numberNeurons.getMinValue();
		int maxValue = numberNeurons.getMaxValue();
		
		snmNumberNeurons.setValue(value);
		snmNumberNeurons.setMinimum(minValue);
		snmNumberNeurons.setMaximum(maxValue);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void spnNumberInputsStateChanged(ChangeEvent e) {
		int value = (int) spnNumberInputs.getValue();
		getNetworkModel().setNumberInputs(value);
	}
	
	private void spnNumberNeuronsStateChanged(ChangeEvent e) {
		int value = (int) spnNumberNeurons.getValue();
		KohonenNetworkModel model = (KohonenNetworkModel) getNetworkModel();
		model.setNumberNeurons(value);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JPanel pnlNumberInputs;
	private SpinnerNumberModel snmNumberInputs;
	private JSpinner spnNumberInputs;
	
	private JPanel pnlNumberNeurons;
	private SpinnerNumberModel snmNumberNeurons;
	private JSpinner spnNumberNeurons;
	
	//</editor-fold>
	
}
