/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.editornetwork;

import ann.model.SingleLayerPerceptronModel;
import java.awt.*;
import jhelper.model.IntModel;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;

/**
 * Компонент для визуального редактирования модели однослойного персептрона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class EditorSingleLayerPerceptronModel extends EditorModel {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public EditorSingleLayerPerceptronModel() {
		this(new SingleLayerPerceptronModel());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param model Модель однослойного персептрона
	 */
	public EditorSingleLayerPerceptronModel(SingleLayerPerceptronModel model) {
		super(model);
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	@Override
	protected void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		pnlNumberInputs = new JPanel();
		snmNumberInputs = new SpinnerNumberModel();
		spnNumberInputs = new JSpinner();
		
		pnlNumberOutputs = new JPanel();
		snmNumberOutputs = new SpinnerNumberModel();
		spnNumberOutputs = new JSpinner();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка pnlNumberInputs
		{
			Border border = BorderFactory.createTitledBorder("Количество входов:");
			
			pnlNumberInputs.setBorder(border);
		}
		
		// Настройка spnNumberInputs
		spnNumberInputs.setModel(snmNumberInputs);
		
		// Настройка pnlNumberOutputs
		{
			Border border = BorderFactory.createTitledBorder("Количество выходов:");
			
			pnlNumberOutputs.setBorder(border);
		}
		
		// Настройка spnNumberOutputs
		spnNumberOutputs.setModel(snmNumberOutputs);
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		snmNumberInputs.addChangeListener(this::spnNumberInputsStateChanged);
		snmNumberOutputs.addChangeListener(this::spnNumberOutputsStateChanged);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlNumberInputs
		{
			pnlNumberInputs.setLayout(new BorderLayout());
			
			pnlNumberInputs.add(spnNumberInputs, BorderLayout.CENTER);
		}
		
		// Настройка pnlNumberOutputs
		{
			pnlNumberOutputs.setLayout(new BorderLayout());
			
			pnlNumberOutputs.add(spnNumberOutputs, BorderLayout.CENTER);
		}
		
		// Настройка компонента
		{
			setLayout(new GridBagLayout());
			
			add(pnlNumberInputs, new GridBagConstraints(
				0, 1, 1, 1, 1, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
			add(pnlNumberOutputs, new GridBagConstraints(
				0, 2, 1, 1, 1, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	@Override
	protected void update() {
		updateSnmNumberInputs();
		updateSnmNumberOutputs();
	}
	
	/**
	 * Обновляет snmNumberInputs.
	 */
	private void updateSnmNumberInputs() {
		IntModel numberInputs = getNetworkModel().getNumberInputs();
		
		int value = numberInputs.getValue();
		int minValue = numberInputs.getMinValue();
		int maxValue = numberInputs.getMaxValue();
		
		snmNumberInputs.setValue(value);
		snmNumberInputs.setMinimum(minValue);
		snmNumberInputs.setMaximum(maxValue);
	}
	
	/**
	 * Обновляет snmNumberOutputs.
	 */
	private void updateSnmNumberOutputs() {
		IntModel numberOutputs = getNetworkModel().getNumberOutputs();
		
		int value = numberOutputs.getValue();
		int minValue = numberOutputs.getMinValue();
		int maxValue = numberOutputs.getMaxValue();
		
		snmNumberOutputs.setValue(value);
		snmNumberOutputs.setMinimum(minValue);
		snmNumberOutputs.setMaximum(maxValue);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void spnNumberInputsStateChanged(ChangeEvent e) {
		int value = (int) spnNumberInputs.getValue();
		getNetworkModel().setNumberInputs(value);
	}
	
	private void spnNumberOutputsStateChanged(ChangeEvent e) {
		int value = (int) spnNumberOutputs.getValue();
		getNetworkModel().setNumberOutputs(value);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JPanel pnlNumberInputs;
	private SpinnerNumberModel snmNumberInputs;
	private JSpinner spnNumberInputs;
	
	private JPanel pnlNumberOutputs;
	private SpinnerNumberModel snmNumberOutputs;
	private JSpinner spnNumberOutputs;
	
	//</editor-fold>
	
}
