/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.editornetwork;

import ann.hiddenlayer.HiddenLayer;
import cann.other.Tools;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

/**
 * Диалоговое окно для редактирования скрытого слоя.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class EditHiddenLayerDialog extends JDialog {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартное диалоговое окно.
	 */
	public EditHiddenLayerDialog() {
		this(new HiddenLayer(), (Frame) null, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param hiddenLayer Скрытый слой
	 */
	public EditHiddenLayerDialog(HiddenLayer hiddenLayer) {
		this(hiddenLayer, (Frame) null, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param hiddenLayer Скрытый слой
	 * @param owner Владелец
	 */
	public EditHiddenLayerDialog(HiddenLayer hiddenLayer, Frame owner) {
		this(hiddenLayer, owner, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param hiddenLayer Скрытый слой
	 * @param owner Владелец
	 * @param modal Модальность
	 */
	public EditHiddenLayerDialog(HiddenLayer hiddenLayer, Frame owner, boolean modal) {
		this(hiddenLayer, owner, "Редактор скрытого слоя", modal);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param hiddenLayer Скрытый слой
	 * @param owner Владелец
	 * @param title Заголовок
	 */
	public EditHiddenLayerDialog(HiddenLayer hiddenLayer, Frame owner, String title) {
		this(hiddenLayer, owner, title, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param hiddenLayer Скрытый слой
	 * @param owner Владелец
	 * @param title Заголовок
	 * @param modal Модальность
	 */
	public EditHiddenLayerDialog(HiddenLayer hiddenLayer, 
			Frame owner, String title, boolean modal) {
		super(owner, title, modal, null);
		this.hiddenLayer = hiddenLayer;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		pnlTop = new JPanel();
		editorHiddenLayer = new EditorHiddenLayer();
		
		pnlBottom = new JPanel();
		btnOK = new JButton();
		btnCancel = new JButton();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		// Настройка pnlTop
		{
			Border border = new EmptyBorder(10, 10, 0, 10);
			
			pnlTop.setBorder(border);
		}
		
		// Настройка editorHiddenLayer
		editorHiddenLayer.setHiddenLayer(hiddenLayer);
		
		// Настройка btnOK
		btnOK.setText("OK");
		
		// Настройка btnCancel
		btnCancel.setText("Отмена");
		
		// Настройка pnlBottom
		{
			Border border = new EmptyBorder(10, 10, 10, 10);
			pnlBottom.setBorder(border);
		}
		
		// Настройка диалогового окна
		{
			setIconImage(tools.getIcon("settings.png").getImage());
			setSize(250, 227);
			setResizable(false);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnOK.addActionListener(this::btnOKActionPerformed);
		btnCancel.addActionListener(this::btnCancelActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlTop
		{
			pnlTop.setLayout(new BorderLayout());
			
			pnlTop.add(editorHiddenLayer, BorderLayout.CENTER);
		}
		
		// Настройка pnlBottom
		{
			pnlBottom.setLayout(new GridBagLayout());
			
			pnlBottom.add(
				btnOK, new GridBagConstraints(1, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
			pnlBottom.add(
				btnCancel, new GridBagConstraints(2, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
		}
		
		// Настройка диалогового окна
		{
			setLayout(new BorderLayout());
			
			add(pnlTop, BorderLayout.CENTER);
			add(pnlBottom, BorderLayout.SOUTH);
		}
		
		setLocationRelativeTo(null);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет диалоговое окно.
	 */
	private void update() {
		updateEditorHiddenLayer();
	}
	
	/**
	 * Обновляет editorHiddenLayer.
	 */
	private void updateEditorHiddenLayer() {
		editorHiddenLayer.setHiddenLayer(hiddenLayer);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnOKActionPerformed(ActionEvent e) {
		dialogResult = true;
		dispose();
	}
	
	private void btnCancelActionPerformed(ActionEvent e) {
		dispose();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JPanel pnlTop;
	private HiddenLayer hiddenLayer;
	private EditorHiddenLayer editorHiddenLayer;
	private boolean dialogResult;
	
	private JPanel pnlBottom;
	private JButton btnOK;
	private JButton btnCancel;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получить текущий скрытый слой.
	 * 
	 * @return Скрытый слой
	 */
	public HiddenLayer getHiddenLayer() {
		return hiddenLayer;
	}
	
	/**
	 * Установить новый скрытый слой.
	 * 
	 * @param hiddenLayer Скрытый слой
	 */
	public void setHiddenLayer(HiddenLayer hiddenLayer) {
		this.hiddenLayer = hiddenLayer;
		
		update();
	}
	
	/**
	 * Получает текущее значение результата диалогового окна.
	 * 
	 * @return Результат диалогового окна
	 */
	public boolean isDialogResult() {
		return dialogResult;
	}
	
	//</editor-fold>
	
}
