/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.editornetwork;

import ann.model.*;
import ann.structure.NetworkType;
import javax.swing.JComponent;

/**
 * Компонент для визуального редактирования модели сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public abstract class EditorModel extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public EditorModel() {
		this(new SingleLayerPerceptronModel());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param networkModel Модель сети
	 */
	public EditorModel(NetworkModel networkModel) {
		this.networkModel = networkModel;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	protected abstract void init();
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	protected abstract void update();
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Получает компонент для изменения модели сети для заданного типа сети.
	 * 
	 * @param networkType Тип сети
	 * @return Редактор модели сети
	 */
	public static EditorModel getEditorModel(NetworkType networkType) {
		EditorModel editorModel = null;
		
		switch (networkType) {
			case SingleLayerPerceptron:
				editorModel = new EditorSingleLayerPerceptronModel();
				break;
			case MultiLayerPerceptron:
				editorModel = new EditorMultiLayerPerceptronModel();
				break;
			case HopfieldNetwork:
				editorModel = new EditorHopfieldNetworkModel();
				break;
			case HammingNetwork:
				editorModel = new EditorHammingNetworkModel();
				break;
			case KohonenNetwork:
				editorModel = new EditorKohonenNetworkModel();
				break;
			default:
				throw new IllegalArgumentException("Неизвестный тип сети");
		}
		
		return editorModel;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private NetworkModel networkModel;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущую модель сети.
	 * 
	 * @return Модель сети
	 */
	public NetworkModel getNetworkModel() {
		return networkModel;
	}
	
	/**
	 * Устанавливает новую модель сети.
	 * 
	 * @param networkModel Модель сети
	 */
	public void setNetworkModel(NetworkModel networkModel) {
		this.networkModel = networkModel;
		
		update();
	}
	
	//</editor-fold>
	
}
