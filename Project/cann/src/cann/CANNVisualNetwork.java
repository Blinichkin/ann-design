/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import ann.NeuralNetwork;
import ann.SingleLayerPerceptron;
import java.awt.BorderLayout;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Компонент для визуального представления структуры нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNVisualNetwork extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNVisualNetwork() {
		this(new SingleLayerPerceptron());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 */
	public CANNVisualNetwork(NeuralNetwork neuralNetwork) {
		this.neuralNetwork = neuralNetwork;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		pnlLeft = new JPanel();
		pnlRight = new JPanel();
		splitPane = new JSplitPane();
		editorNetwork = new CANNEditorNetwork();
		paintNetwork = new CANNNetworkView();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка pnlLeft
		{
			Border etchedBorder = BorderFactory.createEtchedBorder();
			Border titleBorder = BorderFactory.createTitledBorder(etchedBorder, "Структура сети");
			Border emptyBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
			Border border = BorderFactory.createCompoundBorder(emptyBorder, titleBorder);

			pnlLeft.setBorder(border);
		}
		
		// Настройка pnlRight
		{
			Border etchedBorder = BorderFactory.createEtchedBorder();
			Border titleBorder = BorderFactory.createTitledBorder(etchedBorder, "Просмотр сети");
			Border emptyBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
			Border border = BorderFactory.createCompoundBorder(emptyBorder, titleBorder);

			pnlRight.setBorder(border);
		}
		
		// Настройка splitPane
		{
			splitPane.setDividerLocation(200);
			splitPane.setOneTouchExpandable(true);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlLeft
		{
			pnlLeft.setLayout(new BorderLayout());
			pnlLeft.add(editorNetwork, BorderLayout.CENTER);
		}
		
		// Настройка pnlRight
		{
			pnlRight.setLayout(new BorderLayout());
			pnlRight.add(paintNetwork, BorderLayout.CENTER);
		}
		
		// Настройка splitPane
		{
			splitPane.setLeftComponent(pnlLeft);
			splitPane.setRightComponent(pnlRight);
		}
		
		// Настройка 
		{
			setLayout(new BorderLayout());
			add(splitPane, BorderLayout.CENTER);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateEditorNetwork();
		updatePaintNetwork();
	}
	
	/**
	 * Обновляет editorNetwork.
	 */
	private void updateEditorNetwork() {
		editorNetwork.setNeuralNetwork(neuralNetwork);
	}
	
	/**
	 * Обновляет paintNetwork.
	 */
	private void updatePaintNetwork() {
		paintNetwork.setNeuralNetwork(neuralNetwork);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private NeuralNetwork neuralNetwork;
	
	private JPanel pnlLeft;
	private JPanel pnlRight;
	private JSplitPane splitPane;
	private CANNEditorNetwork editorNetwork;
	private CANNNetworkView paintNetwork;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущую нейронную сеть.
	 * 
	 * @return Нейронная сеть
	 */
	public NeuralNetwork getNeuralNetwork() {
		return neuralNetwork;
	}

	/**
	 * Устанавливает новую нейронную сеть.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 */
	public void setNeuralNetwork(NeuralNetwork neuralNetwork) {
		this.neuralNetwork = neuralNetwork;
		
		update();
	}
	
	//</editor-fold>
	
}
