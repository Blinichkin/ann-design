package cann.other;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class Header extends JPanel {
	
	public Header() {
		
	}
	
	public Header(final JTabbedPane tabbedPane, JLabel lblTitle, final JComponent component) {
		this.tabbedPane = tabbedPane;
		this.lblTitle = lblTitle;
		this.component = component;
		
		init();
    }
	
	//<editor-fold desc="Инициализация">
	
	private void init() {
		initComponents();
		settingsComponents();
		createListeners();
		componentLayout();
	}
	
	private void initComponents() {
		btnClose = new JButton();
	}
	
	private void settingsComponents() {
		Tools tools = new Tools();
		setOpaque(false);
		
		{
			ImageIcon icon = new ImageIcon(tools.getIcon("close.png").getImage());
			btnClose.setIcon(icon);
			btnClose.setPreferredSize(new Dimension(16, 16));
		}
	}
	
	private void createListeners() {
		btnClose.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tabbedPane.remove(component);
			}
		});
	}
	
	private void componentLayout() {
		setLayout(new GridBagLayout());
		Insets insets = new Insets(1, 1, 1, 1);
		
		add(lblTitle, new GridBagConstraints(
			0, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, 
				new Insets(1, 1, 1, 1), 0, 0)
		);
		add(btnClose, new GridBagConstraints(
			1, 0, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				new Insets(1, 1, 1, 1), 0, 0)
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JTabbedPane tabbedPane;
	private JComponent component;
	
	private JLabel lblTitle;
	private JButton btnClose;
	
	//</editor-fold>
	
}
