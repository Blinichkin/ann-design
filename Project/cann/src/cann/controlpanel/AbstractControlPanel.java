/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.controlpanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;
import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

/**
 * Абстрактный компонент управления.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class AbstractControlPanel extends JComponent implements ICANNControlPanel {
	
	//<editor-fold desc="Конструкторы">
	
	public AbstractControlPanel() {
		actionListener = null;
		listenerList = new EventListenerList();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	public void addActionListener(ActionListener l) {
		listenerList.add(ActionListener.class, l);
	}
	
	public void removeActionListener(ActionListener l) {
		listenerList.remove(ActionListener.class, l);
	}
	
	public ChangeListener[] getChangeListeners() {
		return listenerList.getListeners(ChangeListener.class);
	}
	
	protected void fireActionPerformed(ActionEvent event) {
		Object[] listeners = listenerList.getListenerList();
		ActionEvent e = null;
		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == ActionListener.class) {
				if (e == null) {
					String actionCommand = event.getActionCommand();
					e = new ActionEvent(AbstractControlPanel.this,
							ActionEvent.ACTION_PERFORMED,
							actionCommand,
							event.getWhen(),
							event.getModifiers());
				}
				((ActionListener)listeners[i+1]).actionPerformed(e);
			}
		}
	}
	
	public <T extends EventListener> T[] getListeners(Class<T> listenerType) {
        return listenerList.getListeners(listenerType);
    }
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	protected ActionListener actionListener;
	protected EventListenerList listenerList;
	
	//</editor-fold>
	
}
