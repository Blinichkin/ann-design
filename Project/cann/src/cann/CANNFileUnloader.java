/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import cann.data.DataTable;
import cann.other.Tools;
import java.awt.*;
import java.awt.event.ActionEvent;
import jhelper.DataProcessor;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Компонент для выгрузки данных.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNFileUnloader extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNFileUnloader() {
		this(new DataTable());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param dataTable Таблица данных
	 */
	public CANNFileUnloader(DataTable dataTable) {
		this.dataTable = dataTable;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		tfPath = new JTextField();
		btnChoose = new JButton();
		btnUnload = new JButton();
		
		tableModel = new DefaultTableModel();
		tblData = new JTable();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		// Настройка btnChoose
		{
			btnChoose.setIcon(tools.getIcon("folder.png"));
			btnChoose.setToolTipText("Выбор файла");
		}
		
		// Настройка btnUnload
		{
			btnUnload.setIcon(tools.getIcon("save.png"));
			btnUnload.setToolTipText("Сохранение файла");
		}
		
		// Настройка tblData
		{
			tblData.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			tblData.setEnabled(false);
			tblData.setModel(tableModel);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnChoose.addActionListener(this::btnChooseActionPerformed);
		btnUnload.addActionListener(this::btnUnloadActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		{
			setLayout(new GridBagLayout());
			Insets insets = new Insets(2, 2, 2, 2);
			
			add(tfPath, new GridBagConstraints(
				0, 0, 1, 1, 1, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				insets, 0, 0)
			);
			add(btnChoose, new GridBagConstraints(
				1, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
			add(btnUnload, new GridBagConstraints(
				2, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				insets, 0, 0)
			);
		
			add(new JScrollPane(tblData), new GridBagConstraints(
				0, 1, 3, 1, 1, 1, 
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, 
				insets, 0, 0)
			);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateTblData();
	}
	
	/**
	 * Обновляет tblData.
	 */
	private void updateTblData() {
		if (dataTable != null)
			tableModel.setDataVector(dataTable.getData(), dataTable.getHeaders());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnChooseActionPerformed(ActionEvent e) {
		chooseFile();
		fileUnloading();
	}
	
	private void btnUnloadActionPerformed(ActionEvent e) {
		fileUnloading();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выбор файла.
	 */
	private void chooseFile() {
		JFileChooser fileChooser = new JFileChooser();
		int result = fileChooser.showOpenDialog(this);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			String fillPath = fileChooser.getSelectedFile().getPath();
			tfPath.setText(fillPath);
		}
    }
	
	/**
	 * Выгрузка файла.
	 */
	private void fileUnloading() {
		if (check()) return;
		
        String path = tfPath.getText();
		DataProcessor.serialization(path, dataTable);
		JOptionPane.showMessageDialog(this, "Файл сохранен", "Сообщение", 
				JOptionPane.INFORMATION_MESSAGE);
    }
	
	/**
	 * Проверка.
	 * 
	 * @return Результат
	 */
	private boolean check() {
		return checkPath();
	}
	
	/**
	 * Проверка названия файла.
	 * 
	 * @return Результат
	 */
	private boolean checkPath() {
		if (tfPath.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Ошибка! Задан пустой путь к папке", "Ошибка", 
					JOptionPane.ERROR_MESSAGE);
			return true;
		} else return false;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private DataTable dataTable;
	
	private JTextField tfPath;
	private JButton btnChoose;
	private JButton btnUnload;
	
	private DefaultTableModel tableModel;
	private JTable tblData;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущие данные сети.
	 * 
	 * @return Таблица данных
	 */
	public DataTable getDataTable() {
		return dataTable;
	}
	
	/**
	 * Устанавливает новые данные сети.
	 * 
	 * @param dataTable Таблица данных
	 */
	public void setDataTable(DataTable dataTable) {
		this.dataTable = dataTable;
		
		updateTblData();
	}
	
	//</editor-fold>
	
}
