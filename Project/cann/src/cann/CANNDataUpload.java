/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import cann.data.DataStore;
import javax.swing.JComponent;

/**
 * Абстрактный класс компонента загрузки данных для ИНС.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public abstract class CANNDataUpload extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает компонент используя стандартные значения.
	 */
	public CANNDataUpload() {
		this(new DataStore());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param dataStore Хранилище данных
	 */
	public CANNDataUpload(DataStore dataStore) {
		this.dataStore = dataStore;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	protected abstract void init();
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	protected abstract void update();
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private DataStore dataStore;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее хранилище данных.
	 * 
	 * @return Хранилище данных
	 */
	public DataStore getDataStore() {
		return dataStore;
	}

	/**
	 * Устанавливает новое хранилище данных.
	 * 
	 * @param dataStore Хранилище данных
	 */
	public void setDataStore(DataStore dataStore) {
		this.dataStore = dataStore;
		
		update();
	}
	
	//</editor-fold>
	
}
