/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import ann.structure.NetworkType;
import cann.other.Tools;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;

/**
 * Диалоговое окно выбора типа нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNNetworkDialog extends JDialog {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартное диалоговое окно.
	 */
	public CANNNetworkDialog() {
		this(NetworkType.HopfieldNetwork);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param networkType Тип сети
	 */
	public CANNNetworkDialog(NetworkType networkType) {
		this(networkType, (Frame) null, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param networkType Тип сети
	 * @param owner Владелец
	 */
	public CANNNetworkDialog(NetworkType networkType, Frame owner) {
		this(networkType, owner, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param networkType Тип сети
	 * @param owner Владелец
	 * @param modal Модальность
	 */
	public CANNNetworkDialog(NetworkType networkType, Frame owner, boolean modal) {
		this(networkType, owner, "Создание нейронной сети", modal);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param networkType Тип сети
	 * @param owner Владелец
	 * @param title Заголовок
	 */
	public CANNNetworkDialog(NetworkType networkType, Frame owner, String title) {
		this(networkType, owner, title, false);
	}
	
	/**
	 * Создает диалоговое окно используя полученные значения.
	 * 
	 * @param networkType Тип сети
	 * @param owner Владелец
	 * @param title Заголовок
	 * @param modal Модальность
	 */
	public CANNNetworkDialog(NetworkType networkType, Frame owner, String title, boolean modal) {
		super(owner, title, modal);
		this.networkType = networkType;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		splitPane = new JSplitPane();
	
		pnlLeft = new JPanel();
		lstNetworkType = new JList<>();
	
		pnlRight = new JPanel();
		lblNetworkDescription = new JTextArea();
		
		pnlBottom = new JPanel();
		btnOK = new JButton();
		btnCancel = new JButton();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		// Настройка splitPane
		splitPane.setOneTouchExpandable(true);
		
		// Настройка pnlLeft
		{
			Border emptyBorder = BorderFactory.createEmptyBorder(10, 16, 0, 16);
			Border titleBorder = BorderFactory.createTitledBorder("Выбор сети:");
			
			Border border = BorderFactory.createCompoundBorder(emptyBorder, titleBorder);
			
			pnlLeft.setBorder(border);
		}
		
		// Настройка lstNetworkType
		{
			DefaultListModel<NetworkType> model = new DefaultListModel<>();
			
			for (NetworkType networkType: NetworkType.values())
				model.addElement(networkType);
			
			lstNetworkType.setModel(model);
		}
		
		// Настройка pnlRight
		{
			Border titleBorder = BorderFactory.createTitledBorder("Описание:");
			Border emptyBorder = BorderFactory.createEmptyBorder(10, 16, 0, 16);
			Border border = BorderFactory.createCompoundBorder(emptyBorder, titleBorder);
			
			pnlRight.setBorder(border);
		}
		
		// Настройка lblNetworkDescription
		{
			lblNetworkDescription.setEditable(false);
			lblNetworkDescription.setLineWrap(true);
			lblNetworkDescription.setWrapStyleWord(true);
		}
		
		// Настройка pnlBottom
		{
			Border border = BorderFactory.createEmptyBorder(5, 5, 5, 5);
			pnlBottom.setBorder(border);
		}
		
		// Настройка btnОК
		btnOK.setText("ОК");
		
		// Настройка btnCancel
		btnCancel.setText("Отмена");
		
		// Настройка диалогового окна
		{
			setIconImage(tools.getIcon("network.png").getImage());
			setSize(640, 360);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		btnOK.addActionListener(this::btnOkActionPerformed);
		btnCancel.addActionListener(this::btnCancelActionPerformed);
		lstNetworkType.addListSelectionListener(this::lstNetworkTypeValueChanged);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlLeft
		{
			pnlLeft.setLayout(new BorderLayout());
			
			pnlLeft.add(lstNetworkType, BorderLayout.CENTER);
		}
		
		// Настройка pnlRight
		{
			pnlRight.setLayout(new BorderLayout());
			
			JScrollPane scrollPane = new JScrollPane(lblNetworkDescription);
			pnlRight.add(scrollPane, BorderLayout.CENTER);
		}
		
		// Настройка splitPane
		{
			splitPane.setLeftComponent(pnlLeft);
			
			splitPane.setRightComponent(pnlRight);
		}
		
		// Настройка pnlBottom
		{
			pnlBottom.setLayout(new GridBagLayout());
			
			pnlBottom.add(
				btnOK, new GridBagConstraints(1, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
			pnlBottom.add(
				btnCancel, new GridBagConstraints(2, 0, 1, 1, 0, 0, 
				GridBagConstraints.CENTER, GridBagConstraints.NONE, 
				new Insets(1, 1, 1, 1), 0, 0)
			);
		}
		
		// Настройка диалогового окна
		{
			setLayout(new BorderLayout());
			
			add(splitPane, BorderLayout.CENTER);
			add(pnlBottom, BorderLayout.SOUTH);
		}
		
		setLocationRelativeTo(null);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void btnOkActionPerformed(ActionEvent e) {
		saveNetwork();
		resultDialog = true;
		dispose();
	}
	
	private void btnCancelActionPerformed(ActionEvent e) {
		resultDialog = false;
		dispose();
	}
	
	public void lstNetworkTypeValueChanged(ListSelectionEvent e) {
		updateLblNetworkDescription();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет диалоговое окно.
	 */
	private void update() {
		updateLstNetworkType();
	}
	
	/**
	 * Обновляет lstNetworkType.
	 */
	private void updateLstNetworkType() {
		lstNetworkType.setSelectedValue(networkType, true);
	}
	
	/**
	 * Обновляет lblNetworkDescription.
	 */
	private void updateLblNetworkDescription() {
		NetworkType networkType = (NetworkType) lstNetworkType.getSelectedValue();
		
		switch (networkType) {
			case SingleLayerPerceptron:
				lblNetworkDescription.setText("Однослойный персептрон - это нейронная сеть, "
						+ "состоящая из одного слоя нейронов, где входные сигналы связаны "
						+ "последовательно с нейронами матрицей весовых коэффициентов. Однослойный "
						+ "персептрон является одним из первых ИНС. Данный тип сети был разработан "
						+ "Розенблаттом в 1959 г.");
				break;
			case MultiLayerPerceptron:
				lblNetworkDescription.setText("Многослойный персептрон – это нейронная сеть, "
						+ "имеющая один или несколько промежуточных слоев, через которые проходят "
						+ "входные сигналы и поступают в выходной слой, где преобразуются в "
						+ "выходные значения сети. Многослойные персептроны были предложены и "
						+ "исследованы в 1960-х годах.");
				break;
			case HopfieldNetwork:
				lblNetworkDescription.setText("Сеть Хопфилда – это нейронная сеть, состоящая из "
						+ "одного слоя нейронов с обратными связями. Данная сеть относится к группе"
						+ " автоассоциативных. Сеть разработана Хопфилдов в 1982 г.");
				break;
			case HammingNetwork:
				lblNetworkDescription.setText("Сеть Хэмминга - это нейронная сеть, которая по "
						+ "входному вектору, определяет его эталонный образец из памяти сети и "
						+ "выдаёт его номер. Модель сети Хэмминга введена Липпманом в 1987 г.");
				break;
			case KohonenNetwork:
				lblNetworkDescription.setText("Сеть Кохонена - это самоорганизующаяся нейронная "
						+ "сеть, которая позволяет группировать данные в классы близких объектов. "
						+ "Сеть предложена Кохоненом в 1984 г.");
				break;
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Сохраняет тип сети.
	 */
	private void saveNetwork() {
		networkType = (NetworkType) lstNetworkType.getSelectedValue();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private NetworkType networkType;
	private boolean resultDialog;
	
	private JSplitPane splitPane;
	
	private JPanel pnlLeft;
	private JList<NetworkType> lstNetworkType;
	
	private JPanel pnlRight;
	private JTextArea lblNetworkDescription;
	
	private JPanel pnlBottom;
	private JButton btnOK;
	private JButton btnCancel;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение типа сети.
	 * 
	 * @return 
	 */
	public NetworkType getNetworkType() {
		return networkType;
	}

	/**
	 * Устанавливает новое значение типа сети.
	 * 
	 * @param networkType Тип сети
	 */
	public void setNetworkType(NetworkType networkType) {
		this.networkType = networkType;
	}
	
	/**
	 * Возвращает результат диалогового окна.
	 * 
	 * @return Результат
	 */
	public boolean isResultDialog() {
		return resultDialog;
	}
	
	//</editor-fold>
	
}
