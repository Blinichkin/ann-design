/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann;

import ann.*;
import ann.model.NetworkModel;
import ann.structure.NetworkType;
import cann.editornetwork.EditorModel;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;

/**
 * Компонент для редактирования структуры нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class CANNEditorNetwork extends JComponent {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартный компонент.
	 */
	public CANNEditorNetwork() {
		this(new SingleLayerPerceptron());
	}
	
	/**
	 * Создает компонент используя полученные значения.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 */
	public CANNEditorNetwork(NeuralNetwork neuralNetwork) {
		this.neuralNetwork = neuralNetwork;
		
		init();
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		pnlNetworkType = new JPanel();
		lblNetworkType = new JLabel();
		
		pnlEditorModel = new JPanel();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		// Настройка pnlNetworkType
		{
			Border border = BorderFactory.createTitledBorder("Тип:");
			pnlNetworkType.setBorder(border);
		}
		
		// Настройка pnlEditorModel
		{
			Border border = BorderFactory.createTitledBorder("Модель:");
			pnlEditorModel.setBorder(border);
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		// Настройка pnlNetworkType
		{
			pnlNetworkType.setLayout(new BorderLayout());
			pnlNetworkType.add(lblNetworkType, BorderLayout.CENTER);
		}
		
		// Настройка pnlEditorModel
		pnlEditorModel.setLayout(new BorderLayout());
		
		// Настройка компонента
		{
			setLayout(new BorderLayout());
			
			add(pnlNetworkType, BorderLayout.NORTH);
			add(pnlEditorModel, BorderLayout.CENTER);
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновления">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateLblNetworkType();
		updateEditorModel();
	}
	
	/**
	 * Обновляет lblNetworkType.
	 */
	private void updateLblNetworkType() {
		String text = "<html>" + neuralNetwork.getNetworkType() + "</html>";
		lblNetworkType.setText(text);
	}
	
	/**
	 * Обновляет editorModel.
	 */
	private void updateEditorModel() {
		NetworkType networkType = neuralNetwork.getNetworkType();
		editorModel = EditorModel.getEditorModel(networkType);
		
		NetworkModel networkModel = neuralNetwork.getNetworkModel();
		editorModel.setNetworkModel(networkModel);
		
		addEditorModel();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Добавляет редактор модели сети.
	 */
	private void addEditorModel() {
		pnlEditorModel.add(editorModel, BorderLayout.CENTER);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private NeuralNetwork neuralNetwork;
	
	private JPanel pnlNetworkType;
	private JLabel lblNetworkType;
	
	private JPanel pnlEditorModel;
	private EditorModel editorModel;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущую нейронную сеть
	 * 
	 * @return Нейронная сеть
	 */
	public NeuralNetwork getNeuralNetwork() {
		return neuralNetwork;
	}
	
	/**
	 * Устанавливает новую нейронную сеть.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 */
	public void setNeuralNetwork(NeuralNetwork neuralNetwork) {
		this.neuralNetwork = neuralNetwork;
		
		update();
	}
	
	//</editor-fold>
	
}
