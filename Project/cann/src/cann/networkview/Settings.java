/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview;

import cann.networkview.settings.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Класс настроек.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class Settings implements Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает настройки используя стандартные значения.
	 */
	public Settings() {
		this(new Backdrop(),
			new Network(),
			new Connector(),
			new Neuron(),
			new Text(),
			SizeType.Custom
		);
	}
	
	/**
	 * Создает настройки используя полученные значения.
	 * 
	 * @param backdrop Настройки фона
	 * @param network Настройки сети
	 * @param connector Настройки соединителя
	 * @param neuron Настройки нейрона
	 * @param text Настройки текста
	 * @param sizeType Тип размера
	 */
	public Settings(Backdrop backdrop, Network network, Connector connector, Neuron neuron, 
			Text text, SizeType sizeType) {
		this.backdrop = backdrop;
		this.network = network;
		this.connector = connector;
		this.neuron = neuron;
		this.text = text;
		this.sizeType = sizeType;
	}
	
	/**
	 * Создает настройки используя полученные значения.
	 * 
	 * @param settings Настройки
	 */
	public Settings(Settings settings) {
		this(
			settings.getBackdrop(), 
			settings.getNetwork(),
			settings.getConnector(),
			settings.getNeuron(), 
			settings.getText(),
			settings.getSizeType()
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Settings settings = (Settings) super.clone();
		
		settings.setBackdrop((Backdrop) backdrop.clone());
		settings.setNetwork((Network) network.clone());
		settings.setConnector((Connector) connector.clone());
		settings.setNeuron((Neuron) neuron.clone());
		settings.setText((Text) text.clone());
		
		return settings;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		Settings settings = (Settings) object;
		
		return Objects.equals(backdrop, settings.getBackdrop())
				&& Objects.equals(network, settings.getNetwork())
				&& Objects.equals(connector, settings.getConnector())
				&& Objects.equals(neuron, settings.getNeuron())
				&& Objects.equals(text, settings.getText())
				&& sizeType == settings.getSizeType();
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(backdrop, network, connector, neuron, text, sizeType);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName() + "[" 
				+ backdrop.toString() + "," 
				+ network.toString() + "," 
				+ connector.toString() + ","
				+ neuron.toString() + "," 
				+ text.toString() + ","
				+ sizeType.toString() + "]";
	}
	
	//</editor-fold>

	//<editor-fold desc="Поля">
	
	private Backdrop backdrop;
	private Network network;
	private Connector connector;
	private Neuron neuron;
	private Text text;
	private SizeType sizeType;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение настроек фона.
	 * 
	 * @return Настройки фона
	 */
	public Backdrop getBackdrop() {
		return backdrop;
	}

	/**
	 * Устанавливает новое значение настроек фона.
	 * 
	 * @param backdrop Настройки фона
	 */
	public void setBackdrop(Backdrop backdrop) {
		this.backdrop = backdrop;
	}

	/**
	 * Получает текущее значение настроек сети.
	 * 
	 * @return Настройки сети
	 */
	public Network getNetwork() {
		return network;
	}

	/**
	 * Устанавливает новое значение настроек сети.
	 * 
	 * @param network Настройки сети
	 */
	public void setNetwork(Network network) {
		this.network = network;
	}
	
	/**
	 * Получает текущее значение настроек соединителя.
	 * 
	 * @return Настройки соединителя
	 */
	public Connector getConnector() {
		return connector;
	}

	/**
	 * Устанавливает новое значение настроек соединителя.
	 * 
	 * @param connector Настройки соединителя
	 */
	public void setConnector(Connector connector) {
		this.connector = connector;
	}
	
	/**
	 * Получает текущее значение настроек нейронов.
	 * 
	 * @return Настройки нейрона
	 */
	public Neuron getNeuron() {
		return neuron;
	}

	/**
	 * Устанавливает новое значение настроек нейронов.
	 * 
	 * @param neuron Настройки нейрона
	 */
	public void setNeuron(Neuron neuron) {
		this.neuron = neuron;
	}
	
	/**
	 * Получает текущее значение настроек текста.
	 * 
	 * @return Настройки текста
	 */
	public Text getText() {
		return text;
	}

	/**
	 * Устанавливает новое значение настроек текста.
	 * 
	 * @param text Настройки текста
	 */
	public void setText(Text text) {
		this.text = text;
	}
	
	/**
	 * Получает текущее значение типа размера.
	 * 
	 * @return Тип размера
	 */
	public SizeType getSizeType() {
		return sizeType;
	}

	/**
	 * Устанавливает новое значение типа размера.
	 * 
	 * @param sizeType Тип размера
	 */
	public void setSizeType(SizeType sizeType) {
		this.sizeType = sizeType;
	}
	
	//</editor-fold>

}
