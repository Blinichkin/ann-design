/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview;

import java.awt.Point;
import java.io.Serializable;
import java.util.Objects;

/**
 * Класс для хранения настроек позиционирования.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class ImageSettings implements Serializable, Cloneable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартные координаты.
	 */
	public ImageSettings() {
		center = new Point();
		offset = new Point();
		
		update();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	/**
	 * Обновляет компонент.
	 */
	private void update() {
		updateCoordsCenter();
	}
	
	/**
	 * Обновляет координаты центра.
	 */
	private void updateCoordsCenter() {
		center.x = width / 2 + offset.x;
		center.y = height / 2 + offset.y;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Настройки позиционирования
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		ImageSettings positioning = (ImageSettings) super.clone();
		
		positioning.setCenter((Point) center.clone());
		positioning.setOffset((Point) offset.clone());
		
		return positioning;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		ImageSettings positioning = (ImageSettings) object;
		
		return Objects.equals(this.center, positioning.getCenter())
				&& Objects.equals(this.offset, positioning.getOffset());
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(center, offset);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName() + "[" 
				+ center.toString() + "," 
				+ offset.toString() + "]";
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private int width;
	private int height;
	private Point center;
	private Point offset;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение ширины.
	 * 
	 * @return Ширина
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Устанавливает новое значение ширины.
	 * 
	 * @param width 
	 */
	public void setWidth(int width) {
		this.width = width;
		
		update();
	}

	/**
	 * Получает текущее значение высоты.
	 * 
	 * @return Высота
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Устанавливает новое значение высоты.
	 * 
	 * @param height Высота
	 */
	public void setHeight(int height) {
		this.height = height;
		
		update();
	}
	
	/**
	 * Получает текущие значения сдвига.
	 * 
	 * @return Значения сдвига
	 */
	public Point getOffset() {
		return offset;
	}
	
	/**
	 * Устанавливает новые значения сдвига.
	 * 
	 * @param offset Значения сдвига
	 */
	public void setOffset(Point offset) {
		this.offset = offset;
	}
	
	/**
	 * Получает текущие координаты центра.
	 * 
	 * @return Координаты центра
	 */
	public Point getCenter() {
		return center;
	}
	
	/**
	 * Устанавливает новые значения координат центра.
	 * 
	 * @param center Координаты центра
	 */
	public void setCenter(Point center) {
		this.center = center;
	}
	
	//</editor-fold>
	
}
