package cann.networkview.frame;

import ann.model.HopfieldNetworkModel;
import cann.networkview.ImageSettings;
import cann.networkview.PaintTools;
import cann.networkview.Settings;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;

public class HopfieldNetworkFrame extends NetworkFrame {
	
	//<editor-fold desc="Конструкторы">
	
	public HopfieldNetworkFrame() {
		this(new HopfieldNetworkModel(), new Settings(), new ImageSettings());
	}
	
	public HopfieldNetworkFrame(HopfieldNetworkModel model, Settings settings,
			ImageSettings imageSettings) {
		super(model, settings, imageSettings);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	@Override
	public void init() {
		int columnCount = 5;
		int rowCount = ((HopfieldNetworkModel) getNetworkModel()).getNumberNeurons().getValue();
		int maxNumberNeurons = getSettings().getNetwork().getNumberNeurons();
		
		createLimit();
		updateLimit();

        points = new Point[columnCount][];
		
		if (rowCount > maxNumberNeurons)
			rowCount = maxNumberNeurons;

        for (int i = 0; i < points.length; i++)
			points[i] = new Point[rowCount];
	}
	
	private void createLimit() {
		limit = new boolean[4];
	}
	
	private void updateLimit() {
		final HopfieldNetworkModel model = (HopfieldNetworkModel) getNetworkModel();
        int numberNeurons = model.getNumberNeurons().getValue();
		
		final int maxNumberNeurons = getSettings().getNetwork().getNumberNeurons();
		
		for (int i = 0; i < limit.length; i++) {
			if (i == 1 || i == 2)
				limit[i] = numberNeurons > maxNumberNeurons;
			else
				limit[i] = false;
		}
	}
	
	@Override
	public void calc() {
		int x, y;
        int interval = getSettings().getBackdrop().getGrid().getInterval(
				getSettings().getSizeType());
        int horizontalSpan = interval * getSettings().getNetwork().getHorizontalSpan();
        int verticalSpan = interval * getSettings().getNetwork().getVerticalSpan();
		
		int rowCount = ((HopfieldNetworkModel) getNetworkModel()).getNumberNeurons().getValue();
		int maxNumberNeurons = getSettings().getNetwork().getNumberNeurons();
		
		boolean maxInputs = false;
		
		if (rowCount > maxNumberNeurons)
			maxInputs = true;

        x = calcOffset(horizontalSpan, points.length);
		
        for (int i = 0; i < points.length; i++) {
            y = calcOffset(verticalSpan, points[i].length);

            for (int j = 0; j < points[i].length; j++) {
				if (maxInputs && (i == 1 || i == 2) && j == points[i].length - 2)
					points[i][j] = null;
				else {
					if (i == 1)
						points[i][j] = new Point(x, y - interval);
					else
						points[i][j] = new Point(x, y);
				}
				
                y += verticalSpan;
            }

            x += horizontalSpan;	
        }
	}
	
	@Override
	public void paint(BufferedImage image) {
		Graphics2D g2D = image.createGraphics();
		clearImage(g2D, getImageSettings());
		
		PaintTools.antialiasing(g2D, true);
		drawLinks(g2D);
		drawConnectors(g2D);
		drawNeurons(g2D);
		drawText(g2D);
		PaintTools.antialiasing(g2D, false);
	}
	
	private void drawLinks(Graphics2D g2D) {
		Point p = new Point();
		
		for (int i = 0; i < points.length - 1; i++) {
			for (int j = 0; j < points[i].length; j++) {
				switch (i) {
					case 0:
						if (points[i][j] != null && points[i + 2][j] != null) {
							p = calcCoord(points[i][j], points[i + 2][j], TypeGraphics.Neuron);
							drawArrow(g2D, points[i][j], p);
						}
						break;
					case 1:
						for (int k = 0; k < points[i + 1].length; k++) {
							if (j != k) {
								if (points[i][j] != null && points[i + 1][k] != null) {
									p = calcCoord(points[i][j], points[i + 1][k], TypeGraphics.Neuron);
									drawArrow(g2D, points[i][j], p);
								}
							}
						}	
						break;
					case 2:
						if (points[i][j] != null && points[i + 2][j] != null)
							drawArrow(g2D, points[i][j], points[i + 2][j]);
						break;
					case 3:
						if (points[i][j] != null && points[1][j] != null) {
							p.x = points[i][j].x;
							p.y = points[i][j].y - getSettings().getBackdrop().getGrid()
								.getInterval(getSettings().getSizeType());
							drawLine(g2D, points[i][j], p);
						
							drawLine(g2D, p, points[1][j]);
						}
						break;
				}
			}
		}
	}
	
	private void drawConnectors(Graphics2D g2D) {
		int column = 1;
		
		for (int i = 0; i < points[column].length; i++)
			if (points[column][i] != null)
				drawOval(g2D, points[column][i]);
	}
	
	private void drawNeurons(Graphics2D g2D) {
		int column = 2;
		
		for (int i = 0; i < points[column].length; i++)
			if (points[column][i] != null)
				drawNeuron(g2D, points[column][i]);
	}
	
	private void drawText(Graphics2D g2D) {
		int column;
		
		int rowCount = ((HopfieldNetworkModel) getNetworkModel()).getNumberNeurons().getValue();
		final int interval = getSettings().getBackdrop().getGrid()
				.getInterval(getSettings().getSizeType());
		
		column = 0;
		text(g2D, "x", column, rowCount, 0);
		
		column = points.length - 1;
		text(g2D, "y", column, rowCount, interval);
	}
	
	private void text(Graphics2D g2D, String t, int column, int maxNumberNeurons, int interval) {
		String text;
		
		for (int i = 0; i < points[column].length; i++) {
			if (limit[1]) {
				if (i == points[column].length - 2)
					text = "...";
				else if (i == points[column].length - 1)
					text = t + maxNumberNeurons;
				else
					text = t + (i + 1);
			} else text = t + (i + 1);
			
			Point p = points[column][i];
			p.x += interval;
			
			drawText(g2D, p, text);
		}
	}
	
	//</editor-fold>
	
}
