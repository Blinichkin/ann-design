/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.frame;

import ann.model.SingleLayerPerceptronModel;
import cann.networkview.ImageSettings;
import cann.networkview.Settings;
import cann.networkview.PaintTools;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;

/**
 * Каркас однослойного персептрона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class SingleLayerPerceptronFrame extends NetworkFrame 
		implements ISingleLayerPerceptronFrame {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает каркас однослойного персептрона используя стандартные значения.
	 */
	public SingleLayerPerceptronFrame() {
		this(new SingleLayerPerceptronModel(), new Settings(), new ImageSettings());
	}
	
	/**
	 * Создает каркас однослойного персептрона используя полученные значения.
	 * 
	 * @param model Модель однослойного персептрона
	 * @param settings Настройки отображения
	 * @param imageSettings Настройки изображения
	 */
	public SingleLayerPerceptronFrame(SingleLayerPerceptronModel model, Settings settings,
			ImageSettings imageSettings) {
		super(model, settings, imageSettings);
	}
	
	/**
	 * Создает каркас однослойного персептрона используя полученные значения.
	 * 
	 * @param frame Каркас однослойного персептрона
	 */
	public SingleLayerPerceptronFrame(SingleLayerPerceptronFrame frame) {
		this((SingleLayerPerceptronModel)frame.getNetworkModel(), frame.getSettings(), 
				frame.getImageSettings());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	@Override
	public void init() {
		final SingleLayerPerceptronModel model = (SingleLayerPerceptronModel) getNetworkModel();
        int numberInputs = model.getNumberInputs().getValue();
        int numberOutputs = model.getNumberOutputs().getValue();
		final int maxNumberNeurons = getSettings().getNetwork().getNumberNeurons();
		
		createLimit();
		updateLimit();
		
		if (limit[1])
			numberInputs = maxNumberNeurons;
		
		if (limit[2]) 
			numberOutputs = maxNumberNeurons;
		
		points = new Point[COLUMN_COUNT][];
		for (int i = 0; i < points.length; i++) {
			if (i < 2)
				points[i] = new Point[numberInputs];
			else
				points[i] = new Point[numberOutputs];
		}
	}
	
	private void createLimit() {
		limit = new boolean[COLUMN_COUNT];
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	private void updateLimit() {
		final SingleLayerPerceptronModel model = (SingleLayerPerceptronModel) getNetworkModel();
        int numberInputs = model.getNumberInputs().getValue();
        int numberOutputs = model.getNumberOutputs().getValue();
		final int maxNumberNeurons = getSettings().getNetwork().getNumberNeurons();
		
		for (int i = 0; i < limit.length; i++) {
			switch (i) {
				case 1:
					limit[i] = numberInputs > maxNumberNeurons;
					break;
				case 2:
					limit[i] = numberOutputs > maxNumberNeurons;
					break;
				default:
					limit[i] = false;
					break;
			}
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	@Override
	public void calc() {
        final int interval = getSettings().getBackdrop().getGrid()
				.getInterval(getSettings().getSizeType());
		final int horizontalSpan = interval * getSettings().getNetwork().getHorizontalSpan();
        final int verticalSpan = interval * getSettings().getNetwork().getVerticalSpan();

		int x, y;
        x = -(horizontalSpan + interval);
		
        for (int i = 0; i < points.length; i++) {
			y = calcOffset(verticalSpan, points[i].length);

            for (int j = 0; j < points[i].length; j++) {
				if (limit[i] && j == points[i].length - 2)
					points[i][j] = null;
				else
					points[i][j] = new Point(x, y);
				
                y += verticalSpan;
            }

            x += horizontalSpan;
        }
	}
	
	@Override
	public void paint(BufferedImage image) {
		Graphics2D g2D = image.createGraphics();
		clearImage(g2D, getImageSettings());
		
		PaintTools.antialiasing(g2D, true);
		drawLinks(g2D);
		drawConnectors(g2D);
		drawNeurons(g2D);
		drawText(g2D);
		PaintTools.antialiasing(g2D, false);
	}
	
	private void drawLinks(Graphics2D g2D) {
		Point p;

		for (int i = 0; i < points.length - 1; i++) {
			for (int j = 0; j < points[i].length; j++) {
				switch (i) {
					case 0:
						if (points[i][j] != null && points[i + 1][j] != null) {
							p = calcCoord(points[i][j], points[i + 1][j], TypeGraphics.Connector);
							drawArrow(g2D, points[i][j], p);
						}
						break;
					case 1:
						for (int k = 0; k < points[i + 1].length; k++) {
							if (points[i][j] != null && points[i + 1][k] != null) {
								p = calcCoord(points[i][j], points[i + 1][k], TypeGraphics.Neuron);
								drawArrow(g2D, points[i][j], p);
							}
						}	break;
					default:
						if (points[i][j] != null && points[i + 1][j] != null) {
							p = calcCoord(points[i][j], points[i + 1][j], TypeGraphics.Empty);
							drawArrow(g2D, points[i][j], p);
						}
						break;
				}
			}
		}
	}
	
	private void drawConnectors(Graphics2D g2D) {
		int column = 1;
		
		for (Point p : points[column])
			drawOval(g2D, p);
	}
	
	private void drawNeurons(Graphics2D g2D) {
		int column = 2;
		
		for (Point point : points[column])
			drawNeuron(g2D, point);
	}
	
	private void drawText(Graphics2D g2D) {
		final SingleLayerPerceptronModel model = (SingleLayerPerceptronModel) getNetworkModel();
        final int numberInputs = model.getNumberInputs().getValue();
        final int numberOutputs = model.getNumberOutputs().getValue();
		final int interval = getSettings().getBackdrop().getGrid()
				.getInterval(getSettings().getSizeType());
		
		int column = 0;
		text(g2D, "x", column, numberInputs, 0, 1);
		
		column = points.length - 1;
		text(g2D, "y", column, numberOutputs, interval, 2);
	}
	
	private void text(Graphics2D g2D, String t, int column, int maxNumberNeurons, int interval, int j) {
		String text;
		
		for (int i = 0; i < points[column].length; i++) {
			if (limit[j]) {
				if (i == points[column].length - 2)
					text = "...";
				else if (i == points[column].length - 1)
					text = t + maxNumberNeurons;
				else
					text = t + (i + 1);
			} else text = t + (i + 1);
			
			Point p = points[column][i];
			p.x += interval;
			
			drawText(g2D, p, text);
		}
	}
	
	//</editor-fold>
	
}
