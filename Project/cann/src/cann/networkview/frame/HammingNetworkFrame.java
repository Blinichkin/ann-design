/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.frame;

import ann.model.HammingNetworkModel;
import cann.networkview.ImageSettings;
import cann.networkview.PaintTools;
import cann.networkview.Settings;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;

/**
 * Каркас сети Хемминга.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class HammingNetworkFrame extends NetworkFrame implements IHammingNetworkFrame {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает каркас сети Хемминга используя стандартные значения.
	 */
	public HammingNetworkFrame() {
		this(new HammingNetworkModel(), new Settings(), new ImageSettings());
	}
	
	/**
	 * Создает каркас сети Хемминга используя полученные значения.
	 * 
	 * @param model Модель сети Хемминга
	 * @param settings Настройки отображения
	 * @param imageSettings Настройки изображения
	 */
	public HammingNetworkFrame(HammingNetworkModel model, Settings settings,
			ImageSettings imageSettings) {
		super(model, settings, imageSettings);
	}
	
	/**
	 * Создает каркас сети Хемминга используя полученные значения.
	 * 
	 * @param frame Каркас сети Хемминга
	 */
	public HammingNetworkFrame(HammingNetworkFrame frame) {
		this((HammingNetworkModel)frame.getNetworkModel(), frame.getSettings(), 
				frame.getImageSettings());
	}
	
	//</editor-fold>
	@Override
	public void init() {
		final HammingNetworkModel model = (HammingNetworkModel) getNetworkModel();
        int numberInputs = model.getNumberInputs().getValue();
        int numberOutputs = model.getNumberOutputs().getValue();
		final int maxNumberNeurons = getSettings().getNetwork().getNumberNeurons();
		
		createLimit();
		updateLimit();
		
		if (limit[1])
			numberInputs = maxNumberNeurons;
		
		if (limit[2]) 
			numberOutputs = maxNumberNeurons;

        points = new Point[COLUMN_COUNT][];

        for (int i = 0; i < points.length; i++) {
			if (i < 2)
				points[i] = new Point[numberInputs];
			else
				points[i] = new Point[numberOutputs];
		}
	}
	
	private void createLimit() {
		limit = new boolean[COLUMN_COUNT];
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Обновление">
	
	private void updateLimit() {
		final HammingNetworkModel model = (HammingNetworkModel) getNetworkModel();
        final int numberInputs = model.getNumberInputs().getValue();
        final int numberOutputs = model.getNumberOutputs().getValue();
		final int maxNumberNeurons = getSettings().getNetwork().getNumberNeurons();
		
		for (int i = 0; i < limit.length; i++) {
			if (i < 2)
				limit[i] = numberInputs > maxNumberNeurons;
			else
				limit[i] = numberOutputs > maxNumberNeurons;
		}
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">

	@Override
	public void calc() {
		int x, y;
        int interval = getSettings().getBackdrop().getGrid().getInterval(
				getSettings().getSizeType());
        int horizontalSpan = interval * getSettings().getNetwork().getHorizontalSpan();
        int verticalSpan = interval * getSettings().getNetwork().getVerticalSpan();

        x = calcOffset(horizontalSpan, points.length);
		
        for (int i = 0; i < points.length; i++) {
            y = calcOffset(verticalSpan, points[i].length);

            for (int j = 0; j < points[i].length; j++) {
				if (limit[i] && j == points[i].length - 2 && (i > 0 && i < points.length - 2))
					points[i][j] = null;
				else {
					if (i == 3)
						points[i][j] = new Point(x, y - interval);
					else
						points[i][j] = new Point(x, y);
				}
                y += verticalSpan;
            }

            x += horizontalSpan;	
        }
	}
	
	@Override
	public void paint(BufferedImage image) {
		Graphics2D g2D = image.createGraphics();
		clearImage(g2D, getImageSettings());
		
		PaintTools.antialiasing(g2D, true);
		
		Point p = new Point();
		for (int i = 0; i < points.length; i++) {
			for (int j = 0; j < points[i].length; j++) {
				if (i != points.length - 1) {
					if (i == 0) {
						if (points[i][j] != null && points[i + 1][j] != null) {
							p = calcCoord(points[i][j].x, points[i][j].y, 
									points[i + 1][j].x, points[i + 1][j].y, TypeGraphics.Connector);
							drawArrow(g2D, points[i][j], p);
						}
					}
					
					if (i == 1) {
						for (int k = 0; k < points[i + 1].length; k++) {
							if (points[i][j] != null && points[i + 1][k] != null) {
								p = calcCoord(points[i][j].x, points[i][j].y, 
										points[i + 1][k].x, points[i + 1][k].y,
										TypeGraphics.Neuron);
								drawArrow(g2D, points[i][j], p);
							}
						}
						
						drawOval(g2D, points[i][j]);
					}
					
					if (i == 2) {
						if (points[i][j] != null && points[i + 1][j] != null) {
							p = calcCoord(points[i][j].x, points[i][j].y, points[i + 2][j].x, 
									points[i + 2][j].y, TypeGraphics.Neuron);
							drawArrow(g2D, points[i][j], p);
							drawNeuron(g2D, points[i][j]);
						}
					}
					
					if (i == 3) {
						for (int k = 0; k < points[i + 1].length; k++) {
							if (j != k) {
								if (points[i][j] != null && points[i + 1][k] != null) {
									p = calcCoord(points[i][j].x, points[i][j].y, 
											points[i + 1][k].x, points[i + 1][k].y, 
											TypeGraphics.Neuron);
									drawArrow(g2D, points[i][j], p);
								}
							}
						}
						
						drawOval(g2D, points[i][j]);
					}
					
					if (i == 4) {
						if (points[i][j] != null && points[i + 2][j] != null) {
							drawArrow(g2D, points[i][j], points[i + 2][j]);
							drawNeuron(g2D, points[i][j]);
						}
					}
					
					if (i == 5) {
						if (points[i][j] != null && points[i - 2][j] != null) {
							p.x = points[i][j].x;
							p.y = points[i][j].y - getSettings().getBackdrop().getGrid()
								.getInterval(getSettings().getSizeType());
							drawLine(g2D, points[i][j], p);
						
							drawLine(g2D, p, points[i - 2][j]);
						}
					}
				}
			}
		}
		drawText(g2D);
		PaintTools.antialiasing(g2D, false);
	}
	
	private void drawText(Graphics2D g2D) {
		final HammingNetworkModel model = (HammingNetworkModel) getNetworkModel();
		final int numberInputs = model.getNumberInputs().getValue();
        final int numberOutputs = model.getNumberOutputs().getValue();
		final int interval = getSettings().getBackdrop().getGrid()
				.getInterval(getSettings().getSizeType());
		
		int column = 0;
		text(g2D, "x", column, numberInputs, 0, 1);
		
		column = points.length - 1;
		text(g2D, "y", column, numberOutputs, interval, 2);
	}
	
	private void text(Graphics2D g2D, String t, int column, int maxNumberNeurons, int interval, int j) {
		String text;
		
		for (int i = 0; i < points[column].length; i++) {
			if (limit[j]) {
				if (i == points[column].length - 2)
					text = "...";
				else if (i == points[column].length - 1)
					text = t + maxNumberNeurons;
				else
					text = t + (i + 1);
			} else text = t + (i + 1);
			
			Point p = points[column][i];
			p.x += interval;
			
			drawText(g2D, p, text);
		}
	}
	
	//</editor-fold>
	
}
