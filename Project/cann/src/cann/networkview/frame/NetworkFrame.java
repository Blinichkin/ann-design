/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.frame;

import ann.model.NetworkModel;
import ann.model.SingleLayerPerceptronModel;
import ann.structure.NetworkType;
import cann.networkview.ImageSettings;
import cann.networkview.Settings;
import cann.networkview.settings.Link;
import cann.networkview.settings.Text;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;

public abstract class NetworkFrame implements Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	public NetworkFrame() {
		this(new SingleLayerPerceptronModel(), new Settings(), new ImageSettings());
	}
	
	public NetworkFrame(NetworkModel networkModel, Settings settings, 
			ImageSettings imageSettings) {
		this.networkModel = networkModel;
		this.settings = settings;
		this.imageSettings = imageSettings;
		
		init();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	public abstract void init();
	
	public abstract void calc();
	
	public static NetworkFrame getNetworkFrame(NetworkType networkType) {
		switch (networkType) {
			case SingleLayerPerceptron:
				return new SingleLayerPerceptronFrame();
			case MultiLayerPerceptron:
				return new MultiLayerPerceptronFrame();
			case HopfieldNetwork:
				return new HopfieldNetworkFrame();
			case HammingNetwork:
				return new HammingNetworkFrame();
			case KohonenNetwork:
				return new KohonenNetworkFrame();
			default:
				return null;
		}
	}
	
	protected static int calcOffset(int distance, int length) {
		return -(length / 2) * distance + (length % 2 == 0 ? 1 : 0) * (distance / 2);
	}
	
	public abstract void paint(BufferedImage image);
	
	protected Point calcCoord(Point p1, Point p2, TypeGraphics typeGraphics) {
		return calcCoord(p1.x, p1.y, p2.x, p2.y, typeGraphics);
	}
	
	protected Point calcCoord(int x1, int y1, int x2, int y2, TypeGraphics typeGraphics) {
		int Rac = getIndent(typeGraphics) / 2;
		double Rab = Math.sqrt(Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2));
		double k = Rac / Rab;
				
		Point p = new Point();
		p.x = (int) (x2 + (x1 - x2) * k);
		p.y = (int) (y2 + (y1 - y2) * k);
		return p;				
	}
	
	private int getIndent(TypeGraphics typeGraphics) {
		int indent = 0;
		
		switch (typeGraphics) {
			case Connector:
				indent = settings.getConnector().getSize(getSettings().getSizeType());
				break;
			case Neuron:
				indent = settings.getNeuron().getSize(getSettings().getSizeType());
				break;
		}
		
		return indent;
	}
	
	protected void clearImage(Graphics2D g2D, ImageSettings imageSettings) {
		int width = imageSettings.getWidth();
		int height = imageSettings.getHeight();
		
		g2D.setBackground(new Color(0, 0, 0, 0));
		g2D.clearRect(0, 0, width, height);
	}
	
	protected void drawLine(Graphics2D g2D, Point begin, Point end) {
		Link link = settings.getNetwork().getLink();
        g2D.setColor(link.getColor());
        g2D.setStroke(new BasicStroke(link.getWidth(settings.getSizeType())));
		
		begin = calcCoords(begin);
		end = calcCoords(end);
		g2D.drawLine(begin.x, begin.y, end.x, end.y);
	}
	
	protected void drawArrow(Graphics2D g2D, Point begin, Point end) {
		drawLine(g2D, begin, end);
		Link link = settings.getNetwork().getLink();
        g2D.setColor(link.getColor());
        g2D.setStroke(new BasicStroke(link.getWidth(settings.getSizeType())));
		
		int w = settings.getBackdrop().getGrid().getInterval(settings.getSizeType()) / 8;
		int h = settings.getBackdrop().getGrid().getInterval(settings.getSizeType()) / 3;
		int x1, y1, x2, y2;
		
		begin = calcCoords(begin);
		end = calcCoords(end);
		
		x1 = begin.x;
		y1 = begin.y;
		x2 = end.x;
		y2 = end.y;
		
		double l = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
		
		double v1x = (x2 - x1) / l;
		double v1y = (y2 - y1) / l;
		
		double vox = x2 - v1x * h;
		double voy = y2 - v1y * h;
		
		double vnx = v1y;
		double vny = -v1x;
		
		x1 = x2;
		y1 = y2;
		x2 = (int) Math.round(vox + vnx * w);
		y2 = (int) Math.round(voy + vny * w);
		
		g2D.drawLine(x1, y1, x2, y2);
		
		x2 = (int) Math.round(vox - vnx * w);
		y2 = (int) Math.round(voy - vny * w);
		
		g2D.drawLine(x1, y1, x2, y2);
	}
	
	protected void drawOval(Graphics2D g2D, Point p) {
		if (p == null) return;
		
		final int size = settings.getConnector().getSize(settings.getSizeType());
		final Color color = settings.getConnector().getColor();
		final Point center = imageSettings.getCenter();
		
		int x, y;
		
		x = p.x + center.x - size / 2;
		y = p.y + center.y - size / 2;
		
		g2D.setColor(color);
		g2D.fillOval(x, y, size, size);
	}
	
	protected void drawNeuron(Graphics2D g2D, Point p) {
		if (p == null) return;
		
		final int size = settings.getNeuron().getSize(settings.getSizeType());
		final Color background = settings.getNeuron().getBackColor();
		final Color foreground = settings.getNeuron().getFrontColor();
		final Point center = imageSettings.getCenter();
		
		int x, y;
		int indent = size / 8;
		int width, height;
		
		x = p.x + center.x - size / 2;
		y = p.y + center.y - size / 2;
		width = size;
		height = size;
		g2D.setColor(foreground);
		g2D.fill(new Ellipse2D.Float(x, y, width, height));
		
		x = x + indent;
		y = y + indent;
		width -= indent * 2;
		height -= indent * 2;
		g2D.setColor(background);
		g2D.fill(new Ellipse2D.Float(x, y, width, height));
	}
	
	protected void drawText(Graphics2D g2D, Point p, String str) {
		if (!settings.getText().isVisible()) return;
		if (p == null) return;
		
		textSettings(g2D, settings);
		
		int interval = settings.getBackdrop().getGrid().getInterval(settings.getSizeType());
		FontMetrics fontMetrics = g2D.getFontMetrics();
		
		int x, y, indentX, indentY;
		
		indentY = (interval - fontMetrics.getHeight()) / 2 + fontMetrics.getAscent() + 1;
		indentX = (interval - fontMetrics.stringWidth(str)) / 2 + 1;
		
		p = calcCoords(p);
		x = (p.x - interval) + indentX;
		y = (p.y - interval) + indentY;
		
		g2D.drawString(str, x, y);
	}
	
	private void textSettings(Graphics2D g2D, Settings settings) {
		Text text = settings.getText();
		int size = text.getSize(settings.getSizeType());
		Color color = text.getColor(); 
		
		g2D.setFont(new Font(g2D.getFont().getFontName(), Font.PLAIN, size));
		g2D.setColor(color);
	}
	
	protected Point calcCoords(Point p) {
		Point center = imageSettings.getCenter();
		Point point = (Point) p.clone();
		point.x += center.x;
		point.y += center.y;
		
		return  point;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private NetworkModel networkModel;
	private Settings settings;
	private ImageSettings imageSettings;
	protected Point[][] points;
	protected boolean[] limit;
	
	//</editor-fold>
	
	//<editor-fold desc="Доступы">
	
	public NetworkModel getNetworkModel() {
		return networkModel;
	}
	
	public void setNetworkModel(NetworkModel networkModel) {
		this.networkModel = networkModel;
	}
	
	public Settings getSettings() {
		return settings;
	}
	
	public void setSettings(Settings settings) {
		this.settings = settings;
	}
	
	public ImageSettings getImageSettings() {
		return imageSettings;
	}

	public void setImageSettings(ImageSettings imageSettings) {
		this.imageSettings = imageSettings;
	}
	
	//</editor-fold>
	
}
