/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.settings;

import cann.networkview.SizeType;
import java.awt.Color;
import java.io.Serializable;
import java.util.Objects;

/**
 * Класс настроек нейрона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class Neuron implements INeuron, Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает настройки нейрона используя стандартные значения.
	 */
	public Neuron() {
		this(SIZE, BACK_COLOR, FRONT_COLOR);
	}
	
	/**
	 * Создает настройки нейрона используя полученные значения.
	 * 
	 * @param size Размер
	 * @param backColor Цвет заливки
	 * @param frontColor Цвет контура
	 */
	public Neuron(int size, Color backColor, Color frontColor) {
		this.size = size;
		this.backColor = backColor;
		this.frontColor = frontColor;
	}
	
	/**
	 * Создает настройки нейрона используя полученные значения.
	 * 
	 * @param neuron Настройки нейрона
	 */
	public Neuron(Neuron neuron) {
		this(neuron.getSize(), neuron.getBackColor(), neuron.getFrontColor());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">

	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Neuron neuron = (Neuron) super.clone();
		
		return neuron;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		Neuron neuron = (Neuron) object;
		
		return size == neuron.getSize()
				&& Objects.equals(backColor, neuron.getBackColor())
				&& Objects.equals(frontColor, neuron.getFrontColor());
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(size, backColor, frontColor);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName() + "[" 
				+ "size=" + size + "," 
				+ backColor.toString() + ","
				+ frontColor.toString() + "]";
	}
	
	//</editor-fold>

	//<editor-fold desc="Поля">
	
	private int size;
	private Color backColor;
	private Color frontColor;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">

	/**
	 * Получает текущее значение размера.
	 * 
	 * @return Размер
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * Получает текущее значение размера в зависимости от выбранного типа размера.
	 * 
	 * @param sizeType Тип размера
	 * @return Размер
	 */
	public int getSize(SizeType sizeType) {
		switch (sizeType) {
			case Tiny: 
				return Neuron.TINY_SIZE;
			case Small: 
				return Neuron.SMALL_SIZE;
			case Normal: 
				return Neuron.NORMAL_SIZE;
			case Big: 
				return Neuron.BIG_SIZE;
			case Huge: 
				return Neuron.HUGE_SIZE;
			default: 
				return getSize();
		}
	}

	/**
	 * Устанавливает новое значение размера.
	 * 
	 * @param size Размер
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * Получает текущее значение цвета заливки.
	 * 
	 * @return Цвет заливки
	 */
	public Color getBackColor() {
		return backColor;
	}

	/**
	 * Устанавливает новое значение цвета заливки.
	 * 
	 * @param backColor Цвет заливки
	 */
	public void setBackColor(Color backColor) {
		this.backColor = backColor;
	}

	/**
	 * Получает текущее значение цвета контура.
	 * 
	 * @return Цвет контура
	 */
	public Color getFrontColor() {
		return frontColor;
	}

	/**
	 * Устанавливает новое значение цвета контура.
	 * 
	 * @param frontColor Цвет контура
	 */
	public void setFrontColor(Color frontColor) {
		this.frontColor = frontColor;
	}
	
	//</editor-fold>

}
