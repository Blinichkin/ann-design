/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.settings;

import java.io.Serializable;
import java.util.Objects;

/**
 * Класс настроек сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class Network implements INetwork, Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает настройки сети используя стандартные значения.
	 */
	public Network() {
		this(new Link(), HORIZONTAL_SPAN, VERTICAL_SPAN, NUMBER_NEURONS);
	}
	
	/**
	 * Создает настройки сети используя полученные значения.
	 * 
	 * @param link Настройки связи
	 * @param horizontalSpan Промежуток между нейронами по горизонтали
	 * @param verticalSpan Промежуток между нейронами по вертикали
	 * @param numberNeurons Количество отображаемых нейронов в слое
	 */
	public Network(Link link, int horizontalSpan, int verticalSpan, int numberNeurons) {
		this.link = link;
		this.horizontalSpan = horizontalSpan;
		this.verticalSpan = verticalSpan;
		this.numberNeurons = numberNeurons;
	}
	
	/**
	 * Создает настройки сети используя полученные значения.
	 * 
	 * @param network Настройки сети
	 */
	public Network(Network network) {
		this (
			network.getLink(),
			network.getHorizontalSpan(),
			network.getVerticalSpan(),
			network.getNumberNeurons()
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Network network = (Network) super.clone();
		
		network.setLink((Link) link.clone());
		
		return network;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		Network network = (Network) object;
		
		return Objects.equals(link, network.getLink())
				&& horizontalSpan == network.getHorizontalSpan()
				&& verticalSpan == network.getVerticalSpan()
				&& numberNeurons == network.getNumberNeurons();
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(link, horizontalSpan, verticalSpan, numberNeurons);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName() + "[" 
				+ link.toString() + "," 
				+ "horizontalSpan=" + horizontalSpan + ","
				+ "verticalSpan=" + verticalSpan + ","
				+ "numberNeurons=" + numberNeurons + "]";
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private Link link;
	private int horizontalSpan;
	private int verticalSpan;
	private int numberNeurons;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">

	/**
	 * Получает текущие настройки связи.
	 * 
	 * @return Настройки связи
	 */
	public Link getLink() {
		return link;
	}

	/**
	 * Устанавливает новые настройки связи.
	 * 
	 * @param link Настройки связи
	 */
	public void setLink(Link link) {
		this.link = link;
	}

	/**
	 * Получает текущее значение промежутка между нейронами по горизонтали.
	 * 
	 * @return Промежуток
	 */
	public int getHorizontalSpan() {
		return horizontalSpan;
	}

	/**
	 * Устанавливает новое значение промежутка между нейронами по горизонтали.
	 * 
	 * @param horizontalSpan Промежуток
	 */
	public void setHorizontalSpan(int horizontalSpan) {
		this.horizontalSpan = horizontalSpan;
	}

	/**
	 * Получает текущее значение промежутка между нейронами по вертикали.
	 * 
	 * @return Промежуток
	 */
	public int getVerticalSpan() {
		return verticalSpan;
	}

	/**
	 * Устанавливает новое значение промежутка между нейронами по вертикали.
	 * 
	 * @param verticalSpan Промежуток
	 */
	public void setVerticalSpan(int verticalSpan) {
		this.verticalSpan = verticalSpan;
	}
	
	/**
	 * Получает текущее значение количества отображаемых нейронов в слое.
	 * 
	 * @return Количество нейронов
	 */
	public int getNumberNeurons() {
		return numberNeurons;
	}

	/**
	 * Устанавливает новое значение количества отображаемых нейронов в слое. 
	 * 
	 * @param numberNeurons Количество нейронов 
	 */
	public void setNumberNeurons(int numberNeurons) {
		this.numberNeurons = numberNeurons;
	}
	
	//</editor-fold>
	
}
