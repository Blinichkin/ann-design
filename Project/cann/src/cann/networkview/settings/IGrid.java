/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.settings;

import java.awt.Color;

/**
 * Интерфейс настроек сетки.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public interface IGrid {
	
	//<editor-fold desc="Константы">
	
	/** Стандартное значение видимости. */
	public static final boolean VISIBLE = true;
	
	/** Стандартное значение цвета. */
	public static final Color COLOR = new Color(150, 150, 150);
	
	/** Стандартное значение интервала. */
	public static final int INTERVAL = 32;
	
	/** Минимальное значение интервала. */
	public static final int MIN_INTERVAL = 8;
	
	/** Максимальное значение интервала. */
	public static final int MAX_INTERVAL = 128;
	
	/** Мелкое значение интервала. */
	public static final int TINY_INTERVAL = 8;
	
	/** Маленькое значение интервала. */
	public static final int SMALL_INTERVAL = 16;
	
	/** Обычное значение интервала. */
	public static final int NORMAL_INTERVAL = 32;
	
	/** Большое значение интервала. */
	public static final int BIG_INTERVAL = 64;
	
	/** Огромное значение интервала. */
	public static final int HUGE_INTERVAL = 128;
	
	/** Размер шага изменения интервала. */
	public static final int STEP_SIZE_INTERVAL = 1;
	
	/** Стандартное значение толщины. */
	public static final float THICKNESS = 1;
	
	/** Минимальное значение толщины. */
	public static final float MIN_THICKNESS = 0;
	
	/** Максимальное значение толщины. */
	public static final float MAX_THICKNESS = 10;
	
	/** Мелкое значение толщины. */
	public static final float TINY_THICKNESS = 0.5f;
	
	/** Маленькое значение толщины. */
	public static final float SMALL_THICKNESS = 0.75f;
	
	/** Обычное значение толщины. */
	public static final int NORMAL_THICKNESS = 1;
	
	/** Большое значение толщины. */
	public static final int BIG_THICKNESS = 2;
	
	/** Громадное значение толщины. */
	public static final float HUGE_THICKNESS = 3;
	
	/** Размер шага изменения толщины. */
	public static final float STEP_SIZE_THICKNESS = 0.5f;
	
	//</editor-fold>
	
}
