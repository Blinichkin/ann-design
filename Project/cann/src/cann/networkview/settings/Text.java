/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.settings;

import cann.networkview.SizeType;
import java.awt.Color;
import java.io.Serializable;
import java.util.Objects;

/**
 * Класс для хранения настроек текста.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class Text implements IText, Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает настройки для текста используя стандартные значения.
	 */
	public Text() {
		this(VISIBLE, SIZE, COLOR);
	}
	
	/**
	 * Создает настройки для текста используя полученные значения.
	 * 
	 * @param visible Видимость
	 * @param size Размер
	 * @param color Цвет
	 */
	public Text(boolean visible, int size, Color color) {
		this.visible = visible;
		this.size = size;
		this.color = color;
	}
	
	/**
	 * Создает настройки для текста используя полученные значения.
	 * 
	 * @param text Настройки текста
	 */
	public Text(Text text) {
		this(text.isVisible(), text.getSize(), text.getColor());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Text text = (Text) super.clone();
		
		return text;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		Text text = (Text) object;
		
		return visible == text.isVisible()
				&& size == text.getSize()
				&& Objects.equals(this.color, text.getColor());
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(visible, size, color);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName() + "[" 
				+ "visible=" + visible + ","
				+ "size=" + size + ","
				+ color.toString() + "]";
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private boolean visible;
	private int size;
	private Color color;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">

	/**
	 * Получает текущее значение видимости.
	 * 
	 * @return Видимость
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * Устанавливает новое значение видимости.
	 * 
	 * @param visible Видимость
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * Получает текущее значение размера.
	 * 
	 * @return Размер
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * Получает текущее значение размера в зависимости от выбранного типа размера.
	 * 
	 * @param sizeType Тип размера
	 * @return Размер
	 */
	public int getSize(SizeType sizeType) {
		switch (sizeType) {
			case Tiny: 
				return Text.TINY_SIZE;
			case Small: 
				return Text.SMALL_SIZE;
			case Normal: 
				return Text.NORMAL_SIZE;
			case Big: 
				return Text.BIG_SIZE;
			case Huge: 
				return Text.HUGE_SIZE;
			default: 
				return getSize();
		}
	}

	/**
	 * Устанавливает новое значение размера.
	 * 
	 * @param size Размер
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * Получает текущее значение цвета.
	 * 
	 * @return Цвет
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Устанавливает новое значение цвета.
	 * 
	 * @param color Цвет
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	//</editor-fold>
	
}
