/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.settings;

import cann.networkview.SizeType;
import java.awt.Color;
import java.io.Serializable;
import java.util.Objects;

/**
 * Класс настроек соединителя.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class Connector implements IConnector, Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">

	/**
	 * Создает настройки соединителя используя стандартные значения.
	 */
	public Connector() {
		this(SIZE, COLOR);
	}
	
	/**
	 * Создает настройки соединителя используя полученные значения.
	 * 
	 * @param size Размер
	 * @param color Цвет
	 */
	public Connector(int size, Color color) {
		this.size = size;
		this.color = color;
	}
	
	/**
	 * Создает настройки соединителя используя полученные значения.
	 * 
	 * @param connector Настройки соединителя
	 */
	public Connector(Connector connector) {
		this(connector.getSize(), connector.getColor());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Connector connector = (Connector) super.clone();
		
		return connector;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		Connector connector = (Connector) object;
		
		return size == connector.getSize() && Objects.equals(color, connector.getColor());
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(size, color);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName()+ "[" 
				+ "size=" + size + "," 
				+ color.toString() + "]";
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private int size;
	private Color color;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение размера.
	 * 
	 * @return Размер
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * Получает текущее значение размера в зависимости от выбранного типа размера.
	 * 
	 * @param sizeType Тип размера
	 * @return Размер
	 */
	public int getSize(SizeType sizeType) {
		switch (sizeType) {
			case Tiny: 
				return Connector.TINY_SIZE;
			case Small: 
				return Connector.SMALL_SIZE;
			case Normal: 
				return Connector.NORMAL_SIZE;
			case Big: 
				return Connector.BIG_SIZE;
			case Huge: 
				return Connector.HUGE_SIZE;
			default: 
				return getSize();
		}
	}
	
	/**
	 * Устанавливает новое значение размера.
	 * 
	 * @param size Размер
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
	/**
	 * Получает текущее значение цвета.
	 * 
	 * @return Цвет
	 */
	public Color getColor() {
		return color;
	}
	
	/**
	 * Устанавливает новое значение цвета.
	 * 
	 * @param color Цвет
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	//</editor-fold>
	
}
