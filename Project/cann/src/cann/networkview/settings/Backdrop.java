/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.settings;

import java.awt.Color;
import java.io.Serializable;
import java.util.Objects;

/**
 * Класс настроек фона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class Backdrop implements IBackdrop, Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает настройки фона используя стандартные значения.
	 */
	public Backdrop() {
		this(COLOR, new Grid());
	}
	
	/**
	 * Создает настройки фона используя полученные значения.
	 * 
	 * @param color Цвет фона
	 * @param grid Настройки сетки
	 */
	public Backdrop(Color color, Grid grid) {
		this.color = color;
		this.grid = grid;
	}
	
	/**
	 * Создает настройки фона используя полученные значения.
	 * 
	 * @param backdrop Настройки фона
	 */
	public Backdrop(Backdrop backdrop) {
		this(backdrop.getColor(), backdrop.getGrid());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Backdrop backdrop = (Backdrop) super.clone();
		
		backdrop.setGrid((Grid) grid.clone());
		
		return backdrop;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		Backdrop backdrop = (Backdrop) object;
		
		return Objects.equals(color, backdrop.getColor())
				&& Objects.equals(grid, backdrop.getGrid());
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(color, grid);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName()+ "[" 
				+ color.toString() + "," 
				+ grid.toString() + "]";
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private Color color;
	private Grid grid;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение цвета.
	 * 
	 * @return Цвет
	 */
	public Color getColor() {
		return color;
	}
	
	/**
	 * Устанавливает новое значение цвета.
	 * 
	 * @param color Цвет
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	/**
	 * Получает текущие настройки сетки.
	 * 
	 * @return Настройки сетки
	 */
	public Grid getGrid() {
		return grid;
	}
	
	/**
	 * Устанавливает новые настройки сетки.
	 * 
	 * @param grid Настройки сетки
	 */
	public void setGrid(Grid grid) {
		this.grid = grid;
	}
	
	//</editor-fold>
	
}
