/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.settings;

import cann.networkview.SizeType;
import java.awt.Color;
import java.io.Serializable;
import java.util.Objects;

/**
 * Класс настроек связи.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class Link implements ILink, Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает настройки связи используя стандартные значения.
	 */
	public Link() {
		this(COLOR, THICKNESS, VISIBLE);
	}
	
	/**
	 * Создает настройки связи используя полученные значения.
	 * 
	 * @param color Цвет
	 * @param width Ширина
	 * @param visible Видимость указателей
	 */
	public Link(Color color, int width, boolean visible) {
		this.color = color;
		this.width = width;
		this.visible = visible;
	}
	
	/**
	 * Создает настройки связи используя полученные значения.
	 * 
	 * @param link Настройки связи
	 */
	public Link(Link link) {
		this (
			link.getColor(), 
			link.getWidth(), 
			link.isVisible()
		);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Link link = (Link) super.clone();
		
		return link;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		Link link = (Link) object;
		
		return Objects.equals(color, link.getColor())
				&& width == link.getWidth()
				&& visible == link.isVisible();
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(color, width, visible);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName() + "[" 
				+ color.toString() + "," 
				+ "width=" + width + ","
				+ "visible=" + visible + "]";
	}
	
	//</editor-fold>

	//<editor-fold desc="Поля">
	
	private Color color;
	private int width;
	private boolean visible;

	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение цвета.
	 * 
	 * @return Цвет
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Устанавливает новое значение цвета.
	 * 
	 * @param color Цвет
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Получает текущее значение толщины.
	 * 
	 * @return Толщина
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Получает текущее значение толщины в зависимости от выбранного типа размера.
	 * 
	 * @param sizeType
	 * @return Толщина
	 */
	public float getWidth(SizeType sizeType) {
		switch (sizeType) {
			case Tiny: 
				return Link.TINY_THICKNESS;
			case Small: 
				return Link.SMALL_THICKNESS;
			case Normal: 
				return Link.NORMAL_THICKNESS;
			case Big: 
				return Link.BIG_THICKNESS;
			case Huge: 
				return Link.HUGE_THICKNESS;
			default: 
				return getWidth();
		}
	}

	/**
	 * Устанавливает новое значение толщины.
	 * 
	 * @param width Толщина
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Получает текущее значение видимости указателей.
	 * 
	 * @return Видимость указателей
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * Устанавливает новое значение видимости указателей.
	 * 
	 * @param visible Видимость указателей
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	//</editor-fold>
	
}
