/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.settings;

import java.awt.Color;

/**
 * Интерфейс настроек фона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public interface IBackdrop {
	
	//<editor-fold desc="Константы">
	
	/** Стандартное значение цвета. */
	public static final Color COLOR = new Color(240, 240, 240);
	
	//</editor-fold>
	
}
