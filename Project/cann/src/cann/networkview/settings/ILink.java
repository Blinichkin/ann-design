/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.settings;

import java.awt.Color;

/**
 * Интерфейс настроек связи.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public interface ILink {
	
	//<editor-fold desc="Константы">
	
	/** Стандартное значение видимости. */
	public static final boolean VISIBLE = true;
	
	/** Стандартное значение цвета. */
	public static final Color COLOR = Color.BLACK;
	
	/** Стандартное значение толщины. */
	public static final int THICKNESS = 3;
	
	/** Минимальное значение толщины. */
	public static final int MIN_THICKNESS = 1;
	
	/** Максимальное значение толщины. */
	public static final int MAX_THICKNESS = 10;
	
	/** Мелкое значение толщины. */
	public static final int TINY_THICKNESS = 1;
	
	/** Маленькое значение толщины. */
	public static final int SMALL_THICKNESS = 2;
	
	/** Обычное значение толщины. */
	public static final int NORMAL_THICKNESS = 3;
	
	/** Большое значение толщины. */
	public static final int BIG_THICKNESS = 4;
	
	/** Громадное значение толщины. */
	public static final int HUGE_THICKNESS = 5;
	
	/** Размер шага изменения толщины. */
	public static final int STEP_SIZE_THICKNESS = 1;
	
	//</editor-fold>
	
}
