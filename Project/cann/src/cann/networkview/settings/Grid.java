/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.settings;

import cann.networkview.SizeType;
import java.awt.Color;
import java.io.Serializable;
import java.util.Objects;

/**
 * Класс настроек сетки.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class Grid implements IGrid, Cloneable, Serializable {

	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает настройки сетки используя стандартные значения.
	 */
	public Grid() {
		this(VISIBLE, COLOR, INTERVAL, THICKNESS);
	}
	
	/**
	 * Создает настройки сетки используя полученные значения.
	 * 
	 * @param visible Видимость
	 * @param color Цвет
	 * @param interval Интервал
	 * @param width Ширина
	 */
	public Grid(boolean visible, Color color, int interval, float width) {
		this.visible = visible;
		this.color = color;
		this.interval = interval;
		this.thickness = width;
	}
	
	/**
	 * Создает настройки сетки используя полученные значения.
	 * 
	 * @param grid Настройки сетки
	 */
	public Grid(Grid grid) {
		this (
			grid.isVisible(), 
			grid.getColor(), 
			grid.getInterval(), 
			grid.getThickness()
		);
	}
	
	//</editor-fold>

	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Grid grid = (Grid) super.clone();
		
		return grid;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		Grid grid = (Grid) object;
		
		return visible == grid.isVisible()
				&& Objects.equals(this.color, grid.getColor())
				&& interval == grid.getInterval()
				&& thickness == grid.getThickness();
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(visible, color, interval, thickness);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName() + "[" 
				+ "visible=" + visible + "," 
				+ color.toString() + "," 
				+ "interval=" + interval + "," 
				+ "width=" + thickness + "]";
	}
	
	//</editor-fold>

	//<editor-fold desc="Поля">
	
	private boolean visible;
	private Color color;
	private int interval;
	private float thickness;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение видимости.
	 * 
	 * @return Видимость
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * Устанавливает новое значение видимости.
	 * 
	 * @param visible Видимость
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * Получает текущее значение цвета.
	 * 
	 * @return Цвет
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Устанавливает новое значение цвета.
	 * 
	 * @param color Цвет
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Получает текущее значение интервала.
	 * 
	 * @return Интервал
	 */
	public int getInterval() {
		return interval;
	}
	
	/**
	 * Получает текущее значение интервала в зависимости от выбранного типа размера.
	 *
	 * @param sizeType Тип размера
	 * @return Интервал
	 */
	public int getInterval(SizeType sizeType) {
		switch (sizeType) {
			case Tiny: 
				return Grid.TINY_INTERVAL;
			case Small: 
				return Grid.SMALL_INTERVAL;
			case Normal: 
				return Grid.NORMAL_INTERVAL;
			case Big: 
				return Grid.BIG_INTERVAL;
			case Huge: 
				return Grid.HUGE_INTERVAL;
			default: 
				return getInterval();
		}
	}

	/**
	 * Устанавливает новое значение интервала.
	 * 
	 * @param interval Интервал
	 */
	public void setInterval(int interval) {
		this.interval = interval;
	}

	/**
	 * Получает текущее значение толщины.
	 * 
	 * @return Толщина
	 */
	public float getThickness() {
		return thickness;
	}
	
	/**
	 * Получает текущее значение толщины в зависимости от выбранного типа размера.
	 *
	 * @param sizeType Тип размера
	 * @return Толщина
	 */
	public float getThickness(SizeType sizeType) {
		switch (sizeType) {
			case Tiny: 
				return Grid.TINY_THICKNESS;
			case Small: 
				return Grid.SMALL_THICKNESS;
			case Normal: 
				return Grid.NORMAL_THICKNESS;
			case Big: 
				return Grid.BIG_THICKNESS;
			case Huge: 
				return Grid.HUGE_THICKNESS;
			default: 
				return getThickness();
		}
	}

	/**
	 * Устанавливает новое значение толщины.
	 * 
	 * @param thickness Толщина
	 */
	public void setThickness(float thickness) {
		this.thickness = thickness;
	}
	
	//</editor-fold>
	
}
