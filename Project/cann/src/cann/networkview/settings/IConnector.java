/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview.settings;

import java.awt.Color;

/**
 * Интерфейс настроек соединителя.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public interface IConnector {
	
	//<editor-fold desc="Константы">
	
	/** Стандартное значение размера. */
	public static final int SIZE = 8;
	
	/** Минимальное значение размера. */
	public static final int MIN_SIZE = 2;
	
	/** Максимальное значение размера. */
	public static final int MAX_SIZE = 32;
	
	/** Мелкое значение размера. */
	public static final int TINY_SIZE = 2;
	
	/** Маленькое значение размера. */
	public static final int SMALL_SIZE = 4;
	
	/** Обычное значение размера. */
	public static final int NORMAL_SIZE = 8;
	
	/** Большое значение размера. */
	public static final int BIG_SIZE = 16;
	
	/** Огромное значение размера. */
	public static final int HUGE_SIZE = 32;
	
	/** Размер шага изменения размера. */
	public static final int STEP_SIZE_SIZE = 1;
	
	/** Стандартное значение цвета. */
	public static final Color COLOR = new Color(0, 0, 0);
	
	//</editor-fold>
	
}
