/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview;

import cann.networkview.settings.Grid;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;

/**
 * Класс для отрисовки фона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class Background {
	
	/**
	 * Рисует фон на полученном изображении.
	 * 
	 * @param image Изображение
	 * @param settings Настройки
	 * @param imageSettings Настройки изображения
	 */
	public static void paint(BufferedImage image, Settings settings, ImageSettings imageSettings) {
		Graphics2D g2D = image.createGraphics();
		fillBackground(g2D, settings, imageSettings);
		paintGrid(g2D, settings, imageSettings);
	}
	
	/**
	 * Заливает фон.
	 * 
	 * @param g2D Графика
	 */
	private static void fillBackground(Graphics2D g2D, Settings settings, 
			ImageSettings imageSettings) {
		int width = imageSettings.getWidth();
		int height = imageSettings.getHeight();
		Color color = settings.getBackdrop().getColor();
		
		g2D.setColor(color);
		g2D.fillRect(0, 0, width, height);
	}
	
	/**
	 * Рисует сетку.
	 * 
	 * @param g2D Графика
	 */
	private static void paintGrid(Graphics2D g2D, Settings settings, ImageSettings imageSettings) {
		Grid grid = settings.getBackdrop().getGrid();
		if (!grid.isVisible()) return;
		
		int interval = grid.getInterval(settings.getSizeType());
		Point center = imageSettings.getCenter();
		int offsetX = center.x % interval;
		int offsetY = center.y % interval;
		int width = imageSettings.getWidth();
		int height = imageSettings.getHeight();
		int x1, y1, x2, y2;
		
		adjustsBrush(g2D, settings);
		PaintTools.antialiasing(g2D, true);
		
		y1 = 0; y2 = height;
		for (int i = 0; i < width; i += interval) {
			x1 = i + offsetX;
			x2 = i + offsetX;
			g2D.drawLine(x1, y1, x2, y2);
		}
		
		x1 = 0; x2 = width;
		for (int i = 0; i < height; i += interval) {
			y1 = i + offsetY;
			y2 = i + offsetY;
			g2D.drawLine(x1, y1, x2, y2);
		}
		
		PaintTools.antialiasing(g2D, false);
	}
	
	/**
	 * Настраивает кисть.
	 * 
	 * @param g2D Графика
	 * @param settings Настройки
	 */
	private static void adjustsBrush(Graphics2D g2D, Settings settings) {
		Grid grid = settings.getBackdrop().getGrid();
		Color color = grid.getColor();
		float widthGrid = grid.getThickness(settings.getSizeType());
		
		g2D.setColor(color);
		g2D.setStroke(new BasicStroke(widthGrid));
	}
	
}
