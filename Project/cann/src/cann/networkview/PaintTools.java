/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview;

import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 * Инструменты для рисования.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class PaintTools {
	
	/**
	 * Сглаживает.
	 * 
	 * @param g2D Графика
	 * @param value Переключатель
	 */
	public static void antialiasing(Graphics2D g2D, boolean value) {
		if (value) 
			g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
					RenderingHints.VALUE_ANTIALIAS_ON);
		else 
			g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
					RenderingHints.VALUE_ANTIALIAS_OFF);
	}
	
}
