/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cann.networkview;

import java.io.Serializable;

/**
 * Перечисление типов размеров.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public enum SizeType implements Serializable {
	
	//<editor-fold desc="Перечисление">
	
	Custom("Свой"),
	Tiny("Мелкий"),
	Small("Маленький"),
	Normal("Обычный"),
	Big("Большой"),
	Huge("Огромный");
	
	//</editor-fold>
	
	//<editor-fold defaultstate="collapsed" desc="Конструкторы">
	
	/**
	 * Создает тип размера.
	 * 
	 * @param name Название размера
	 */
	private SizeType(String name) {
		this.name = name;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private final String name;
	
	//</editor-fold>
	
	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее название размера.
	 * 
	 * @return Название
	 */
	public String getName() {
		return name;
	}
	
	//</editor-fold>
	
}
