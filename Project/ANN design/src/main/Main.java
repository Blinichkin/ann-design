/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package main;

import cann.CANNWorkNetwork;
import cann.other.Tools;
import java.awt.*;
import java.awt.event.*;
import java.util.logging.*;
import javax.swing.*;

/**
 * Главный класс программы.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class Main extends JFrame {

	//<editor-fold desc="Конструкторы">
	
	public Main() {
		init();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация формы.
	 */
	private void init() {
		initComponents();
		settingComponents();
		createListeners();
		componentLayout();
	}
	
	/**
	 * Инициализация компонентов.
	 */
	private void initComponents() {
		menuBar = new JMenuBar();
		
		mFile = new JMenu();
		mCreate = new JMenu();
		miCreateData = new JMenuItem();
		miCreateNetwork = new JMenuItem();
		miOpen = new JMenuItem();
		miSave = new JMenuItem();
		miSaveAs = new JMenuItem();
		miSaveAll = new JMenuItem();
		miClose = new JMenuItem();
		miCloseAll = new JMenuItem();
		miExit = new JMenuItem();
	
		mHelp = new JMenu();
		miAbout = new JMenuItem();
		
		workNetwork = new CANNWorkNetwork();
	}
	
	/**
	 * Настройка компонентов.
	 */
	private void settingComponents() {
		Tools tools = new Tools();
		
		//Настройка mFile
		mFile.setText("Файл");
		
		//Настройка mCreate
		mCreate.setText("Создать");
		
		//Настройка miCreateData
		{
			KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK);
			
			miCreateData.setText("Данные");
			miCreateData.setAccelerator(keyStroke);
		}
		
		//Настройка miCreateNetwork
		{
			KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK 
					| ActionEvent.SHIFT_MASK);
			
			miCreateNetwork.setText("Сеть");
			miCreateNetwork.setAccelerator(keyStroke);
		}
		
		//Настройка miOpen
		{
			KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK);
			
			miOpen.setText("Открыть");
			miOpen.setAccelerator(keyStroke);
		}
		
		//Настройка miSave
		{
			KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK);
			
			miSave.setText("Сохранить");
			miSave.setAccelerator(keyStroke);
		}
		
		//Настройка miSaveAs
		{
			KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK 
					| ActionEvent.SHIFT_MASK);
			
			miSaveAs.setText("Сохранить как ...");
			miSaveAs.setAccelerator(keyStroke);
		}
		
		//Настройка miSaveAll
		{
			KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.ALT_MASK 
					| ActionEvent.SHIFT_MASK);
			
			miSaveAll.setText("Сохранить все");
			miSaveAll.setAccelerator(keyStroke);
		}
		
		//Настройка miClose
		{
			KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.ALT_MASK);
			
			miClose.setText("Закрыть");
			miClose.setAccelerator(keyStroke);
		}
		
		//Настройка miCloseAll
		{
			KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.ALT_MASK 
					| ActionEvent.SHIFT_MASK);
			
			miCloseAll.setText("Закрыть все");
			miCloseAll.setAccelerator(keyStroke);
		}
		
		//Настройка miExit
		{
			KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.ALT_MASK);
			
			miExit.setText("Выход");
			miExit.setAccelerator(keyStroke);
		}
		
		//Настройка mHelp
		mHelp.setText("Справка");
		
		//Настройка miAbout
		miAbout.setText("О программе");
		
		// Настройка формы
		{
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			setExtendedState(MAXIMIZED_BOTH);
			setIconImage(tools.getIcon("network.png").getImage());
			setSize(960, 540);
			setTitle("Конструктор искусственных нейронных сетей");
		}
	}
	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		miCreateData.addActionListener(this::miCreateDataActionPerformed);
		miCreateNetwork.addActionListener(this::miCreateNetworkActionPerformed);
		miOpen.addActionListener(this::miOpenActionPerformed);
		miSave.addActionListener(this::miSaveActionPerformed);
		miSaveAs.addActionListener(this::miSaveAsActionPerformed);
		miSaveAll.addActionListener(this::miSaveAllActionPerformed);
		miClose.addActionListener(this::miCloseActionPerformed);
		miCloseAll.addActionListener(this::miCloseAllActionPerformed);
		miExit.addActionListener(this::miExitActionPerformed);
		miAbout.addActionListener(this::miAboutActionPerformed);
	}
	
	/**
	 * Расположение компонентов.
	 */
	private void componentLayout() {
		setLayout(new BorderLayout());
		
		mCreate.add(miCreateData);
		mCreate.add(miCreateNetwork);
		
		mFile.add(mCreate);
		mFile.add(miOpen);
		mFile.addSeparator();
		mFile.add(miSave);
		mFile.add(miSaveAs);
		mFile.add(miSaveAll);
		mFile.addSeparator();
		mFile.add(miClose);
		mFile.add(miCloseAll);
		mFile.addSeparator();
		mFile.add(miExit);
		
		mHelp.add(miAbout);
		
		menuBar.add(mFile);
		menuBar.add(mHelp);
		
		setJMenuBar(menuBar);
		
		getContentPane().add(workNetwork, BorderLayout.CENTER);
		setLocationRelativeTo(null);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void miCreateDataActionPerformed(ActionEvent e) {
		workNetwork.createData();
	}
	
	private void miCreateNetworkActionPerformed(ActionEvent e) {
		workNetwork.createNetwork();
	}
	
	private void miOpenActionPerformed(ActionEvent e) {
		workNetwork.open();
	}
	
	private void miSaveActionPerformed(ActionEvent e) {
		workNetwork.save();
	}
	
	private void miSaveAsActionPerformed(ActionEvent e) {
		workNetwork.saveAs();
	}
	
	private void miSaveAllActionPerformed(ActionEvent e) {
		workNetwork.saveAll();	
	}
	
	private void miCloseActionPerformed(ActionEvent e) {
		workNetwork.close();
	}
	
	private void miCloseAllActionPerformed(ActionEvent e) {
		workNetwork.closeAll();
	}
	
	private void miExitActionPerformed(ActionEvent e) {
		dispose();
	}
	
	private void miAboutActionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(this, 
				"ANN design - конструктор искусственных нейронных сетей."
				+ "\nАвтор: Блиничкин Денис Юрьевич.", 
				"О программе", JOptionPane.INFORMATION_MESSAGE);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	public static void main(String args[]) {
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Metal".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedLookAndFeelException ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Main().setVisible(true);
			}
		});
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private JMenuBar menuBar;
	
	private JMenu mFile;
	private JMenu mCreate;
	private JMenuItem miCreateData;
	private JMenuItem miCreateNetwork;
	private JMenuItem miOpen;
	private JMenuItem miSave;
	private JMenuItem miSaveAs;
	private JMenuItem miSaveAll;
	private JMenuItem miClose;
	private JMenuItem miCloseAll;
	private JMenuItem miExit;
	
	private JMenu mHelp;
	private JMenuItem miAbout;
	
	private CANNWorkNetwork workNetwork;
	
	//</editor-fold>
	
}
