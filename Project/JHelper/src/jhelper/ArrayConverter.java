package jhelper;

public class ArrayConverter {
	
	public static double[][] intToDouble(int[][] objects) {
		int rowCount = objects.length;
		int columnCount = objects[0].length;
		
		double[][] values = new double[rowCount][columnCount];
		
		for (int i = 0; i < rowCount; i++)
			for (int j = 0; j < columnCount; j++)
				values[i][j] = objects[i][j];
		
		return values;
	}
	
	public static Object[][] intToObject(int[][] objects) {
		int rowCount = objects.length;
		int columnCount = objects[0].length;
		
		Object[][] values = new Object[rowCount][columnCount];
		
		for (int i = 0; i < rowCount; i++)
			for (int j = 0; j < columnCount; j++)
				values[i][j] = objects[i][j];
		
		return values;
	}
	
	public static Object[][] doubleToObject(double[][] objects) {
		int rowCount = objects.length;
		int columnCount = objects[0].length;
		
		Object[][] values = new Object[rowCount][columnCount];
		
		for (int i = 0; i < rowCount; i++)
			for (int j = 0; j < columnCount; j++)
				values[i][j] = objects[i][j];
		
		return values;
	}
	
	public static int[][] objectToInt(Object[][] objects) {
		int rowCount = objects.length;
		int columnCount = objects[0].length;
		
		int[][] values = new int[rowCount][columnCount];
		
		for (int i = 0; i < rowCount; i++)
			for (int j = 0; j < columnCount; j++)
				values[i][j] = Integer.parseInt(objects[i][j].toString());
		
		return values;
	}
	
	
	public static double[][] objectToDouble(Object[][] objects) {
		int rowCount = objects.length;
		int columnCount = objects[0].length;
		
		double[][] values = new double[rowCount][columnCount];
		
		for (int i = 0; i < rowCount; i++)
			for (int j = 0; j < columnCount; j++)
				values[i][j] = Double.parseDouble(objects[i][j].toString());
		
		return values;
	}
}
