package jhelper;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataProcessor {
	
	public static void serialization(String filePath, Object object) {
		FileOutputStream fileOutputStream;
		ObjectOutputStream objectOutputStream;
		
		try {
			fileOutputStream = new FileOutputStream(filePath);
			objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(object);
		} catch (IOException ex) {
			Logger.getLogger(DataProcessor.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public static Object deserialization(String filePath) {
		FileInputStream fileInputStream;
		ObjectInputStream objectInputStream;
		Object object = null;
			
		try {
			fileInputStream = new FileInputStream(filePath);
			objectInputStream = new ObjectInputStream(fileInputStream);
			object = objectInputStream.readObject();
		} catch (IOException | ClassNotFoundException ex) {
			Logger.getLogger(DataProcessor.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return object;
	}
	
}
