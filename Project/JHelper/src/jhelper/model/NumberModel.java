package jhelper.model;

import java.io.Serializable;
import java.util.Objects;

public class NumberModel extends AbstractNumberModel implements Cloneable, Serializable {
	
	//<editor-fold desc="Поля">
	
	protected Number value;
	protected Number minValue;
	protected Number maxValue;
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 *
	 * @return Настройки фона
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		NumberModel model = (NumberModel) super.clone();
		
		return model;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		NumberModel model = (NumberModel) object;
		
		return value == model.value
				&& minValue == model.minValue
				&& maxValue == model.maxValue;
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(value, minValue, maxValue);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName()+ "[" 
				+ "value=" + value + ","
				+ "minValue=" + minValue + "," 
				+ "maxValue=" + maxValue + "]";
	}
	
	//</editor-fold>
	
}
