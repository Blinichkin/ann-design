/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jhelper.model;

/**
 * Класс модели целого типа данных с задаваемой границей.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class IntModel extends NumberModel implements IIntModel {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает модель целого типа данных со стандартными значениями.
	 */
	public IntModel() {
		this(DEFAULT_VALUE);
	}
	
	/**
	 * Создает модель целого типа данных используя полученные значения.
	 * 
	 * @param value Значение
	 */
	public IntModel(int value) {
		this(value, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	/**
	 * Создает модель целого типа данных используя полученные значения.
	 * 
	 * @param minValue Минимум
	 * @param maxValue Максимум
	 */
	public IntModel(int minValue, int maxValue) {
		this(minValue, minValue, maxValue);
	}
	
	/**
	 * Создает модель целого типа данных используя полученные значения.
	 * 
	 * @param value Значение
	 * @param minValue Минимум
	 * @param maxValue Максимум
	 */
	public IntModel(int value, int minValue, int maxValue) {
		checkModel(value, minValue, maxValue);
		
		this.value = value;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	/**
	 * Создает модель целого типа данных используя полученные значения.
	 * 
	 * @param model Модель целого типа данных
	 */
	public IntModel(IntModel model) {
		this(model.getValue(), model.getMinValue(), model.getMaxValue());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Проверка модели.
	 * 
	 * @param value Значение
	 * @param minValue Минимум
	 * @param maxValue Максимум
	 */
	private void checkModel(int value, int minValue, int maxValue) {
		if (minValue > value || value > maxValue)
            throw new IllegalArgumentException("Нарушение условия: minValue <= value <= maxValue");
	}
	
	/**
	 * Проверка диапазона.
	 * 
	 * @param minValue Минимум
	 * @param maxValue Максимум
	 */
	private void checkRange(int minValue, int maxValue) {
		if (minValue > maxValue) {
            throw new IllegalArgumentException("Нарушение условия: minValue <= maxValue");
        }
	}
	
	/**
	 * Проверка значения.
	 */
	private void checkValue() {
		int value = this.value.intValue();
		int minValue = this.minValue.intValue();
		int maxValue = this.maxValue.intValue();
		
		if (value < minValue)
			value = minValue;
		
		if (value > maxValue)
			value = maxValue;
	}
	
	/**
	 * Увеличение значения на 1.
	 */
	public void inc() {
		inc(1);
	}
	
	/**
	 * Увеличение значения на x.
	 * @param x Число
	 */
	public void inc(int x) {
		setValue(value.intValue() + x);
	}
	
	/**
	 * Уменьшение значения на 1.
	 */
	public void dec() {
		dec(1);
	}
	
	/**
	 * Уменьшение значения на x.
	 * @param x Число
	 */
	public void dec(int x) {
		setValue(value.intValue() - x);
	}
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение.
	 * 
	 * @return Значение
	 */
	public int getValue() {
		return value.intValue();
	}
	
	/**
	 * Устанавливает новое значение.
	 * 
	 * @param value Значение
	 */
	public void setValue(int value) {
		checkModel(value, minValue.intValue(), maxValue.intValue());
		this.value = value;
		fireStateChanged();
	}
	
	/**
	 * Получает текущее значение минимума.
	 * 
	 * @return Минимум
	 */
	public int getMinValue() {
		return minValue.intValue();
	}
	
	/**
	 * Устанавливает новое значение минимума.
	 * 
	 * @param minValue Минимум
	 */
	public void setMinValue(int minValue) {
		checkRange(minValue, maxValue.intValue());
		this.minValue = minValue;
		checkValue();
		fireStateChanged();
	}
	
	/**
	 * Получает текущее значение максимума.
	 * 
	 * @return Максимум
	 */
	public int getMaxValue() {
		return maxValue.intValue();
	}
	
	/**
	 * Устанавливает новое значение максимума.
	 * 
	 * @param maxValue Максимум
	 */
	public void setMaxValue(int maxValue) {
		checkRange(minValue.intValue(), maxValue);
		this.maxValue = maxValue;
		checkValue();
		fireStateChanged();
	}
	
	//</editor-fold>
	
}
