/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jhelper.model;

/**
 * Класс модели вещественного типа данных с задаваемой границей.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class DoubleModel extends NumberModel implements IDoubleModel {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает модель вещественного типа данных со стандартными значениями.
	 */
	public DoubleModel() {
		this(DEFAULT_VALUE);
	}
	
	/**
	 * Создает модель вещественного типа данных используя полученные значения.
	 * 
	 * @param value Значение
	 */
	public DoubleModel(int value) {
		this(value, Double.MIN_VALUE, Double.MAX_VALUE);
	}
	
	/**
	 * Создает модель вещественного типа данных используя полученные значения.
	 * 
	 * @param minValue Минимум
	 * @param maxValue Максимум
	 */
	public DoubleModel(int minValue, int maxValue) {
		this(minValue, minValue, maxValue);
	}
	
	/**
	 * Создает модель вещественного типа данных используя полученные значения.
	 * 
	 * @param value Значение
	 * @param minValue Минимум
	 * @param maxValue Максимум
	 */
	public DoubleModel(double value, double minValue, double maxValue) {
		checkModel(value, minValue, maxValue);
		
		this.value = value;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	/**
	 * Создает модель вещественного типа данных используя полученные значения.
	 * 
	 * @param model Модель целого типа данных
	 */
	public DoubleModel(DoubleModel model) {
		this(model.getValue(), model.getMinValue(), model.getMaxValue());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Проверка модели.
	 * 
	 * @param value Значение
	 * @param minValue Минимум
	 * @param maxValue Максимум
	 */
	private void checkModel(double value, double minValue, double maxValue) {
		if (minValue > value || value > maxValue) {
            throw new IllegalArgumentException("Нарушение условия: minValue <= value <= maxValue");
        }
	}
	
	/**
	 * Проверка диапазона.
	 * 
	 * @param minValue Минимум
	 * @param maxValue Максимум
	 */
	private void checkRange(double minValue, double maxValue) {
		if (minValue > maxValue) {
            throw new IllegalArgumentException("Нарушение условия: minValue <= maxValue");
        }
	}
	
	/**
	 * Проверка значения.
	 */
	private void checkValue() {
		double value = this.value.doubleValue();
		double minValue = this.minValue.doubleValue();
		double maxValue = this.maxValue.doubleValue();
		
		if (value < minValue)
			value = minValue;
		
		if (value > maxValue)
			value = maxValue;
	}
	
	/**
	 * Увеличение значения на 1.
	 */
	public void inc() {
		inc(1);
	}
	
	/**
	 * Увеличение значения на x.
	 * @param x Число
	 */
	public void inc(double x) {
		setValue(value.doubleValue() + x);
	}
	
	/**
	 * Уменьшение значения на 1.
	 */
	public void dec() {
		dec(1);
	}
	
	/**
	 * Уменьшение значения на x.
	 * @param x Число
	 */
	public void dec(double x) {
		setValue(value.doubleValue() - x);
	}
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение.
	 * 
	 * @return Значение
	 */
	public double getValue() {
		return value.doubleValue();
	}
	
	/**
	 * Устанавливает новое значение.
	 * 
	 * @param value Значение
	 */
	public void setValue(double value) {
		checkModel(value, minValue.intValue(), maxValue.intValue());
		this.value = value;
		fireStateChanged();
	}
	
	/**
	 * Получает текущее значение минимума.
	 * 
	 * @return Минимум
	 */
	public double getMinValue() {
		return minValue.doubleValue();
	}
	
	/**
	 * Устанавливает новое значение минимума.
	 * 
	 * @param minValue Минимум
	 */
	public void setMinValue(double minValue) {
		checkRange(minValue, maxValue.intValue());
		this.minValue = minValue;
		checkValue();
		fireStateChanged();
	}
	
	/**
	 * Получает текущее значение максимума.
	 * 
	 * @return Максимум
	 */
	public double getMaxValue() {
		return maxValue.doubleValue();
	}
	
	/**
	 * Устанавливает новое значение максимума.
	 * 
	 * @param maxValue Максимум
	 */
	public void setMaxValue(double maxValue) {
		checkRange(minValue.intValue(), maxValue);
		this.maxValue = maxValue;
		checkValue();
		fireStateChanged();
	}
	
	//</editor-fold>
	
}
