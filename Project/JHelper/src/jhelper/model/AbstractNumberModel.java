package jhelper.model;

import java.util.EventListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

public class AbstractNumberModel {
	
	//<editor-fold desc="Конструкторы">
	
	public AbstractNumberModel() {
		changeEvent = null;
		listenerList = new EventListenerList();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	public void addChangeListener(ChangeListener l) {
		listenerList.add(ChangeListener.class, l);
	}
	
	public void removeChangeListener(ChangeListener l) {
		listenerList.remove(ChangeListener.class, l);
	}
	
	public ChangeListener[] getChangeListeners() {
		return listenerList.getListeners(ChangeListener.class);
	}
	
	protected void fireStateChanged() {
		Object[] listeners = listenerList.getListenerList();
		for (int i = listeners.length - 2; i >= 0; i -=2 ) {
			if (listeners[i] == ChangeListener.class) {
				if (changeEvent == null)
					changeEvent = new ChangeEvent(this);
				
				((ChangeListener)listeners[i+1]).stateChanged(changeEvent);
			}
		}
	}
	
	public <T extends EventListener> T[] getListeners(Class<T> listenerType) {
        return listenerList.getListeners(listenerType);
    }
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private transient ChangeEvent changeEvent;
	protected EventListenerList listenerList;
	
	//</editor-fold>
	
}
