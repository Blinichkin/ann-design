/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann;

import ann.model.MultiLayerPerceptronModel;
import ann.structure.NetworkType;
import ann.training.TrainingSettings;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import jhelper.ArrayConverter;
import jhelper.model.IntModel;

/**
 * Класс многослойного персептрона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class MultiLayerPerceptron extends NeuralNetwork implements Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает многослойный персептрон используя стандартные значения.
	 */
	public MultiLayerPerceptron() {
        this(new MultiLayerPerceptronModel());
    }

	/**
	 * Создает многослойный персептрон используя полученные значения.
	 * 
	 * @param model Модель однослойного персептрона
	 */
    public MultiLayerPerceptron(MultiLayerPerceptronModel model) {
        super(NetworkType.MultiLayerPerceptron, model);
    }
	
	/**
	 * Создает многослойный персептрон используя полученные значения.
	 * 
	 * @param network Многослойный персептрон
	 */
	public MultiLayerPerceptron(MultiLayerPerceptron network) {
		super(network);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		MultiLayerPerceptron mlp = (MultiLayerPerceptron) super.clone();
		
		mlp.setW(w.clone());
		
		return mlp;
	}
	
	/**
	 * Обучение многослойного персептрона.
	 *
	 * @param x Входные данные
	 * @param y Выходные данные
	 * @param trainingSettings Настройки обучения
	 */
	public void training(final double[][] x, final double[][] y, 
			final TrainingSettings trainingSettings) {
		initWeight();
		settingWeights(x, y, trainingSettings);
	}
	
	/**
	 * Инициализация весовых коэффициентов.
	 */
	private void initWeight() {
		createWeights();
		randomWeights();
	}
	
	/**
	 * Создание массива весовый коэффициетов.
	 */
	private void createWeights() {
		final MultiLayerPerceptronModel model = (MultiLayerPerceptronModel) getNetworkModel();
		final int numberInputs = model.getNumberInputs().getValue();
		final int numberOutputs = model.getNumberOutputs().getValue();
		final int numberHiddenLayers = model.getHiddenLayer().getNumberLayers().getValue();
		final ArrayList<IntModel> numberNeurons = model.getHiddenLayer().getLayers();
		
        w = new double[numberHiddenLayers + 1][][];

        if (w.length == 1)
            w[0] = new double[numberInputs][numberOutputs];
        else {
            for (int i = 0; i < w.length; i++) {
                if (i == 0)
                    w[i] = new double[numberInputs][numberNeurons.get(i).getValue()];
                else if (i == w.length - 1)
                    w[i] = new double[numberNeurons.get(i - 1).getValue()][numberOutputs];
                else
                    w[i] = new double[numberNeurons.get(i - 1).getValue()]
							[numberNeurons.get(i).getValue()];
            }
        }
    }

	/**
	 * Заполнение массива весовых коэффициетов случайными значениями [-0.5, 0.5].
	 */
    private void randomWeights() {
        for (int i = 0; i < w.length; i++)
            for (int j = 0; j < w[i].length; j++)
                for (int k = 0; k < w[i][j].length; k++)
                    w[i][j][k] = Math.random();
	}
	
	/**
	 * Создание массива сети.
	 * 
	 * @return Массив
	 */
	private double[][] createArray() {
		final MultiLayerPerceptronModel model = (MultiLayerPerceptronModel) getNetworkModel();
		final int numberOutputs = model.getNumberOutputs().getValue();
		final int numberHiddenLayers = model.getHiddenLayer().getNumberLayers().getValue();
		final ArrayList<IntModel> numberNeurons = model.getHiddenLayer().getLayers();
		
        double[][] array = new double[numberHiddenLayers + 1][];

        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1)
                array[i] = new double[numberOutputs];
            else
                array[i] = new double[numberNeurons.get(i).getValue()];
        }

        return array;
    }
	
	/**
	 * Настройка весовых коэффициентов.
	 * 
	 * @param x Входные данные
	 * @param y Выходные данные
	 * @param trainingSettings Настройки обучения
	 */
	private void settingWeights(final double[][] x, final double[][] y, 
			final TrainingSettings trainingSettings) {
		final double a = trainingSettings.getA().getValue();
		
		f = createArray();
        d = createArray();

        while (true) {
            error = 0;

            for (int i = 0; i < x.length; i++) {
                signalHandling(x[i]);
                calculationError(y[i]);
                weightAlignment(x[i], a);
            }

            if (error < trainingSettings.getError().getValue())
                break;
        }
    }
	
	/**
	 * Распространение сигнала.
	 * 
	 * @param x Слой
	 */
	private void signalHandling(double[] x) {
        double sum;

        for (int i = 0; i < f.length; i++) {
            for (int j = 0; j < f[i].length; j++) {
                sum = 0;

                for (int k = 0; k < w[i].length; k++) {
                    if (i == 0)
                        sum += x[k] * w[i][k][j];
                    else
                        sum += f[i - 1][k] * w[i][k][j];
                }

				if (sum > 0.5)
					f[i][j] = 1;
				else
					f[i][j] = 0;
            }
        }
    }

	/**
	 * Вычисление ошибки.
	 * 
	 * @param y Выходные данные
	 */
    private void calculationError(double[] y) {
        double sum;

        for (int i = d.length - 1; i >= 0; i--) {
            for (int j = 0; j < d[i].length; j++) {
                if (i == d.length - 1) {
                    d[i][j] = y[j] - f[i][j];
                    error += Math.abs(d[i][j]);
                } else {
                    sum = 0;

                    for (int k = 0; k < d[i + 1].length; k++)
                        sum += d[i + 1][k] * w[i + 1][j][k];

                    d[i][j] = sum;
                }
            }
        }
    }

	/**
	 * Выравнивание весовых коэффициетов.
	 * 
	 * @param x Слой
	 * @param a Шаг
	 */
    private void weightAlignment(double[] x, double a) {
        for (int i = 0; i < w.length; i++) {
            for (int j = 0; j < w[i].length; j++) {
                for (int k = 0; k < w[i][j].length; k++) {
                    if (i == 0) {
                        w[i][j][k] += d[i][k] * x[j] * a;
                    } else {
                        w[i][j][k] += d[i][k] * f[i - 1][j] * a;
                    }
                }
            }
        }
    }
	
	@Override
	public Object[][] calc(Object[][] x) {
		double[][] inputData = ArrayConverter.objectToDouble(x);
		int[][] outputData = calc(inputData);
		return ArrayConverter.intToObject(outputData);
	}
	
	/**
	 * Выполняет вычисление.
	 * 
	 * @param x Входные данные
	 * @return Выходные данные
	 */
	public int[][] calc(int[][] x) {
		double[][] inputData = ArrayConverter.intToDouble(x);
		return calc(inputData);
	}
	
	/**
	 * Выполняет вычисление.
	 * 
	 * @param x Входные данные
	 * @return Выходные данные
	 */
	public int[][] calc(double [][] x) {
		final int numberOutputs = getNetworkModel().getNumberOutputs().getValue();
		final int numberExamples = x.length;
		int[][] y = new int[numberExamples][numberOutputs];
		
		for (int i = 0; i < x.length; i++) {
			signalHandling(x[i]);
			for (int j = 0; j < f[f.length - 1].length; j++)
				y[i][j] = (int) f[f.length - 1][j];
		}

		return y;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private double[][][] w;
	private double[][] f;
    private double[][] d;
    private double error;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущие значения весовых коэффициетов.
	 * 
	 * @return Весовые коэффициенты
	 */
	public double[][][] getW() {
		return w;
	}

	/**
	 * Устанавливает новые значения весовых коэффициентов.
	 * 
	 * @param w Весовые коэффициенты
	 */
	public void setW(double[][][] w) {
		this.w = w;
	}
	
	//</editor-fold>
	
}
