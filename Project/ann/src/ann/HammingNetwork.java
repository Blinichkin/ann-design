/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann;

import ann.model.HammingNetworkModel;
import ann.structure.NetworkType;
import java.io.Serializable;
import jhelper.ArrayConverter;

/**
 * Класс сети Хэмминга.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class HammingNetwork extends NeuralNetwork implements Cloneable, Serializable {

	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает сеть Хэмминга используя стандартные значения.
	 */
	public HammingNetwork() {
        this(new HammingNetworkModel());
    }

	/**
	 * Создает сеть Хэмминга используя полученные значения.
	 * 
	 * @param model Модель сети Хэмминга
	 */
    public HammingNetwork(HammingNetworkModel model) {
        super(NetworkType.HammingNetwork, model);
    }
	
	/**
	 * Создает сеть Хэмминга используя полученные значения.
	 * 
	 * @param network Сеть Хэмминга
	 */
	public HammingNetwork(HammingNetwork network) {
		super(network);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		HammingNetwork hamming = (HammingNetwork) super.clone();
		
		hamming.setW(w.clone());
		
		return hamming;
	}
	
	private double Func(double t, double x) {
        if (x >= t) 
			return t;
        else if (x <= 0) 
			return 0;
        else 
			return x;
    }
	
	/**
	 * Инициализация весовых коэффициентов.
	 *
	 * @param data Данные сети
	 */
	public void initWeights(final int[][] data) {
		final int numberInputs = getNetworkModel().getNumberInputs().getValue();
		final int numberOutputs = getNetworkModel().getNumberOutputs().getValue();
		
		w = new double[numberInputs][numberOutputs];
        for (int i = 0; i < numberInputs; i++)
            for (int j = 0; j < numberOutputs; j++)
                w[i][j] = data[j][i] / 2.0;
	}
	
	@Override
	public Object[][] calc(Object[][] x) {
		final int[][] inputData = ArrayConverter.objectToInt(x);
		final int[][] outputData = calc(inputData);
		return ArrayConverter.intToObject(outputData);
	}
	
	/**
	 * Выполняет вычисление.
	 *
	 * @param x Входные данные
	 * @return Выходные данные
	 */
	public int[][] calc(final int[][] x) {
		final int numberInputs = getNetworkModel().getNumberInputs().getValue();
		final int numberOutputs = getNetworkModel().getNumberOutputs().getValue();
		final double t = numberInputs / 2.0;
        final double e = 1.0 / numberOutputs;
		
		double[] y1 = new double[numberOutputs];
        double[] y2 = new double[numberOutputs];
        double[] yp = new double[numberOutputs];
		
		double sum;
		for (int i = 0; i < numberOutputs; i++) {
            sum = 0;
            for (int j = 0; j < numberInputs; j++)
                sum += x[0][j] * w[j][i];
			
            y1[i] = sum + t;
            y2[i] = y1[i];
        }

        boolean b;
        do {
            b = false;
			
            for (int j = 0; j < numberOutputs; j++) {
                sum = 0;
                for (int k = 0; k < numberOutputs; k++)
                    if (k != j) sum += y2[k];
				
                yp[j] = Func(t, y2[j] - e * sum);
            }

                
            for (int j = 0; j < numberOutputs; j++) {
                if (yp[j] != y2[j]) {
                    y2[j] = yp[j];
                    b = true;
                }
            }
		} while (b);
			
		int[][] y = new int[1][numberOutputs];
		
		for (int i = 0; i < numberOutputs; i++) {
			y[0][i] = (int) yp[i];
		}
		
		return y;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private double[][] w;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущие значения весовых коэффициетов.
	 * 
	 * @return Весовые коэффициенты
	 */
	public double[][] getW() {
		return w;
	}
	
	/**
	 * Устанавливает новые значения весовых коэффициентов.
	 * 
	 * @param w Весовые коэффициенты
	 */
	public void setW(double[][] w) {
		this.w = w;
	}
	
	//</editor-fold>
	
}
