/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann;

/**
 * Класс содержащий активационные функции.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class ActivationFunction {

    /**
     * Линейная функция.
     * Область значений: (-inf, inf).
     *
     * @param k Параметр наклона
     * @param x Значение
     * @return Результат функции
     */
    public static double linear(double k, double x) {
        return k * x;
    }

    /**
     * Полулинейная функция.
     * Область значений: (0, inf).
     *
     * @param k Параметр наклона
     * @param x Значение
     * @return Результат функции
     */
    public static double semilinear(double k, double x) {
        return x > 0 ? k * x : 0;
    }

    /**
     * Линейная функция с насыщением.
     * Область значений: (-1, 1).
     *
     * @param x Значение
     * @return Результат функции
     */
    public static double saturation(double x) {
        return x >= 1 ? 1 : x <= -1 ? -1 : x;
    }

    /**
     * Логическая функция.
     * Область значений: (0, 1).
     *
     * @param k Параметр наклона
     * @param x Значение
     * @return Результат функции
     */
    public static double logic(double k, double x) {
        return 1.0 / (1 + Math.exp(-k * x));
    }

    /**
     * Гиперболический тангенс.
     * Область значений: (-1, 1).
     *
     * @param k Параметр наклона
     * @param x Значение
     * @return Результат функции
     */
    public static double tanh(double k, double x) {
        double a = Math.exp(k * x);
        double b = Math.exp(-k * x);
        return (a - b) / (a + b);
    }

    /**
     * Рациональная функция.
     * Область значений: (-1, 1).
     *
     * @param k Параметр наклона
     * @param x Значение
     * @return Результат функции
     */
    public static double rational(double k, double x) {
        return x / (k + Math.abs(x));
    }

    /**
     * Синусоидальная функция.
     * Область значений: (-1, 1).
     *
     * @param x Значение
     * @return Результат функции
     */
    public static double sin(double x) {
        return Math.sin(x);
    }

    /**
     * Экспоненциальная функция.
     * Область значений: (0, inf).
     *
     * @param k Параметр наклона
     * @param x Значение
     * @return Результат функции
     */
    public static double exponential(double k, double x) {
        return Math.exp(-k * x);
    }

    /**
     * Пороговая функция.
     * Область значений: (0, 1).
     *
     * @param x Значение
     * @return Результат функции
     */
    public static int threshold(double x) {
        return x >= 0 ? 1 : 0;
    }
	
	/**
     * Пороговая функция.
     * Область значений: (0, 1).
     *
	 * @param t Пороговое значение
     * @param x Значение
     * @return Результат функции
     */
    public static int threshold(double t, double x) {
        return x >= t ? 1 : 0;
    }

    /**
     * Модульная функция.
     * Область значений: (0, inf).
     *
     * @param x Значение
     * @return Результат функции
     */
    public static double modul(double x) {
        return Math.abs(x);
    }


    /**
     * Знаковая функция.
     * Область значений: (-1, 1).
     *
     * @param x Значение
     * @return Результат функции
     */
    public static int sign(double x) {
        return x > 0 ? 1 : -1;
    }

    /**
     * Квадратичная функция.
     * Область значений: (0, inf).
     *
     * @param x Значение
     * @return Результат функции
     */
    public static double quadratic(double x) {
        return Math.pow(x, 2);
    }
	
	/**
	 * Шаговая функция.
	 * Область значений: (-1, 1).
	 * 
     * @param x Значение
     * @return Результат функции
     */
	public static int step(double x) {
		if (x < 0)
            return -1;
		
        if (x > 0)
            return 1;
        
		return 0;
	}

    public static double PiecewiseLinear(double value) {
        double range = 0.5;
        return value >= range ? 1 : value <= -range ? 0 : Math.abs(value);
    }

    public static double Signum(double value) {
        return value > 0 ? 1 : value < 0 ? -1 : 0;
    }

    public static double Sigmoid(double value) {
        return 1.0 / (1 + Math.exp(-value));
    }
	
}
