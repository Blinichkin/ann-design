/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann.training;

import java.io.Serializable;
import java.util.Objects;
import jhelper.model.DoubleModel;
import jhelper.model.IntModel;
import javax.swing.event.ChangeEvent;

/**
 * Класс настроек обучения нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class TrainingSettings extends AbstractTrainingSettings implements Serializable, Cloneable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает стандартные настройки обучения сети.
	 */
	public TrainingSettings() {
		a = new DoubleModel(A, MIN_A, MAX_A);
		error = new DoubleModel(ERROR, MIN_ERROR, MAX_ERROR);
		numberEpochs = new IntModel(NUMBER_EPOCHS, MIN_NUMBER_EPOCHS, MAX_NUMBER_EPOCHS);
		
		init();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		createListeners();
	}

	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		a.addChangeListener(this::aStateChanged);
		error.addChangeListener(this::errorStateChanged);
		numberEpochs.addChangeListener(this::numberEpochsStateChanged);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void aStateChanged(ChangeEvent e) {
		fireStateChanged();
	}
	
	private void errorStateChanged(ChangeEvent e) {
		fireStateChanged();
	}
	
	private void numberEpochsStateChanged(ChangeEvent e) {
		fireStateChanged();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		TrainingSettings trainingSettings = (TrainingSettings) super.clone();
		
		trainingSettings.setA((DoubleModel) a.clone());
		trainingSettings.setError((DoubleModel) error.clone());
		trainingSettings.setNumberEpochs((IntModel) numberEpochs.clone());
		
		return trainingSettings;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		TrainingSettings trainingSettings = (TrainingSettings) object;
		
		return Objects.equals(a, trainingSettings.getA())
				&& Objects.equals(error, trainingSettings.getError())
				&& Objects.equals(numberEpochs, trainingSettings.getNumberEpochs());
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(a, error, numberEpochs);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName()+ "[" 
				+ a.toString() + ","
				+ error.toString() + ","
				+ numberEpochs.toString() + "]";
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">

	private DoubleModel a;
	private DoubleModel error;	
	private IntModel numberEpochs;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение шага.
	 * 
	 * @return Шаг
	 */
	public DoubleModel getA() {
		return a;
	}
	
	/**
	 *	Устанавливает новое значение шага.
	 * 
	 * @param a Шаг
	 */
	public void setA(double a) {
		this.a.setValue(a);
	}
	
	/**
	 *	Устанавливает новое значение шага.
	 * 
	 * @param a Шаг
	 */
	public void setA(DoubleModel a) {
		this.a = a;
		
		fireStateChanged();
	}
	
	/**
	 * Получает текущее значение допустимой ошибки.
	 * 
	 * @return Ошибка
	 */
	public DoubleModel getError() {
		return error;
	}
	
	/**
	 * Устанавливает новое значение допустимой ошибки.
	 * 
	 * @param error Ошибка
	 */
	public void setError(double error) {
		this.error.setValue(error);
	}
	
	/**
	 * Устанавливает новое значение допустимой ошибки.
	 * 
	 * @param error Ошибка
	 */
	public void setError(DoubleModel error) {
		this.error = error;
		
		fireStateChanged();
	}
	
	/**
	 * Получает текущее значение количества эпох.
	 * 
	 * @return Количество эпох
	 */
	public IntModel getNumberEpochs() {
		return numberEpochs;
	}
	
	/**
	 * Устанавливает новое значение количества эпох.
	 * 
	 * @param numberEpochs Количество эпох
	 */
	public void setNumberEpochs(int numberEpochs) {
		this.numberEpochs.setValue(numberEpochs);
	}
	
	/**
	 * Устанавливает новое значение количества эпох.
	 * 
	 * @param numberEpochs Количество эпох
	 */
	public void setNumberEpochs(IntModel numberEpochs) {
		this.numberEpochs = numberEpochs;
		
		fireStateChanged();
	}
	
	//</editor-fold>
	
}
