/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann.training;

/**
 * Интерфейс для класса настроек обучения нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public interface ITrainingSettings {
	
	//<editor-fold desc="Константы">
	
	/** Стандартное значение шага */
	public static final double A = 0.1;
	
	/** Минимальное значение шага */
	public static final double MIN_A = 0;
	
	/** Максимальное значение шага */
	public static final double MAX_A = 1;
	
	/** Стандартное значение ошибки */
	public static final double ERROR = 0.001;
	
	/** Минимальное значение ошибки */
	public static final double MIN_ERROR = 0;
	
	/** Максимальное значение ошибки */
	public static final double MAX_ERROR = 1;
	
	/** Стандартное значение количества эпох */
	public static final int NUMBER_EPOCHS = 10000;
	
	/** Минимальное значение количества эпох */
	public static final int MIN_NUMBER_EPOCHS = 0;
	
	/** Максимальное значение количества эпох */
	public static final int MAX_NUMBER_EPOCHS = Integer.MAX_VALUE;
	
	//</editor-fold>
	
}
