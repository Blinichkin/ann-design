/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann;

import ann.exception.NetworkTypeException;
import ann.model.NetworkModel;
import ann.structure.NetworkType;
import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;

/**
 * Класс нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public abstract class NeuralNetwork extends AbstractNeuralNetwork 
		implements Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">

	/**
	 * Создает нейронную сеть используя полученные значения.
	 * 
	 * @param networkType Тип сети
	 * @param networkModel Модель сети
	 */
	public NeuralNetwork(NetworkType networkType, NetworkModel networkModel) {
        this.networkType = networkType;
        this.networkModel = networkModel;
		
		init();
    }
	
	/**
	 * Создает нейронную сеть используя полученные значения.
	 * 
	 * @param neuralNetwork Нейронная сеть
	 */
	public NeuralNetwork(NeuralNetwork neuralNetwork) {
		this(neuralNetwork.getNetworkType(), neuralNetwork.getNetworkModel());
	}

    //</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		createListeners();
	}

	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		networkModel.addChangeListener(this::networkModelStateChanged);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void networkModelStateChanged(ChangeEvent e) {
		fireStateChanged();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Создает нейронную сеть заданного типа.
	 * 
	 * @param networkType Тип сети
	 * @return Нейронная сеть
	 */
	public static NeuralNetwork create(final NetworkType networkType) {
		switch (networkType) {
			case SingleLayerPerceptron:
				return new SingleLayerPerceptron();
			case MultiLayerPerceptron:
				return new MultiLayerPerceptron();
			case HopfieldNetwork:
				return new HopfieldNetwork();
			case HammingNetwork:
				return new HammingNetwork();
			case KohonenNetwork:
				return new KohonenNetwork();
			default: {
				try {
					throw new NetworkTypeException("Тип сети не найден");
				} catch (NetworkTypeException ex) {
					Logger.getLogger(NeuralNetwork.class.getName()).log(Level.SEVERE, null, ex);
					return null;
				}
			}
		}
	}
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		NeuralNetwork neuralNetwork = (NeuralNetwork) super.clone();
		
		neuralNetwork.setNetworkModel((NetworkModel) networkModel.clone());
		
		return neuralNetwork;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		NeuralNetwork neuralNetwork = (NeuralNetwork) object;
		
		return networkType == neuralNetwork.getNetworkType()
				&& Objects.equals(networkModel, neuralNetwork.getNetworkModel());
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(networkType, networkModel);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName()+ "[" 
				+ networkType + "," 
				+ networkModel.toString() + "]";
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private final NetworkType networkType;
	private NetworkModel networkModel;
	
	//</editor-fold>
	
	//<editor-fold desc="Доступы">

	/**
	 * Получает текущий тип сети.
	 * 
	 * @return Тип сети
	 */
    public NetworkType getNetworkType() {
        return networkType;
    }

	/**
	 * Получает текущую модель сети.
	 * 
	 * @return Модель сети
	 */
    public NetworkModel getNetworkModel() {
        return networkModel;
    }

	/**
	 * Устанавливает новую модель сети.
	 * 
	 * @param networkModel Модель сети
	 */
    public void setNetworkModel(NetworkModel networkModel) {
        this.networkModel = networkModel;
		
		fireStateChanged();
    }

    //</editor-fold>

}
