/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann.hiddenlayer;

import java.io.Serializable;
import java.util.*;
import jhelper.model.IntModel;
import javax.swing.event.ChangeEvent;

/**
 * Класс скрытого слоя.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class HiddenLayer extends AbstractHiddenLayer implements Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает скрытый слой используя стандартные значения.
	 */
	public HiddenLayer() {
		this(MIN_NUMBER_LAYERS);
	}
	
	/**
	 * Создает скрытый слой используя полученные значения.
	 * 
	 * @param numberLayers Количество слоев
	 */
	public HiddenLayer(int numberLayers) {
		this(new IntModel(numberLayers, MIN_NUMBER_LAYERS, MAX_NUMBER_LAYERS));
	}
	
	/**
	 * Создает скрытый слой используя полученные значения.
	 * 
	 * @param numberLayers Количество слоев
	 */
	public HiddenLayer(IntModel numberLayers) {
		this(numberLayers, new ArrayList<IntModel>());
	}
	
	/**
	 * Создает скрытый слой используя полученные значения.
	 * 
	 * @param layers Списочный массив слоев
	 */
	public HiddenLayer(ArrayList<IntModel> layers) {
		this(new IntModel(layers.size(), MIN_NUMBER_LAYERS, MAX_NUMBER_LAYERS), layers);
	}
	
	/**
	 * Создает скрытый слой используя полученные значения.
	 * 
	 * @param numberLayers Количество слоев
	 * @param layers Списочный массив слоев
	 */
	public HiddenLayer(IntModel numberLayers, ArrayList<IntModel> layers) {
		this.numberLayers = numberLayers;
		this.layers = layers;
		
		init();
		checkLayers();
	}
	
	/**
	 * Создает скрытый слой используя полученные значения.
	 * 
	 * @param hiddenLayer Скрытый слой
	 */
	public HiddenLayer(HiddenLayer hiddenLayer) {
		this(hiddenLayer.getNumberLayers(), hiddenLayer.getLayers());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		createListeners();
	}

	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		numberLayers.addChangeListener(this::numberLayersStateChanged);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void numberLayersStateChanged(ChangeEvent e) {
		fireStateChanged();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		HiddenLayer hiddenLayer = (HiddenLayer) super.clone();
		
		hiddenLayer.setNumberLayers((IntModel) numberLayers.clone());
		hiddenLayer.setLayers((ArrayList<IntModel>) layers.clone());
		
		return hiddenLayer;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		HiddenLayer hiddenLayer = (HiddenLayer) object;
		
		return Objects.equals(numberLayers, hiddenLayer.getNumberLayers())
				&& Objects.equals(layers, hiddenLayer.getLayers());
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(numberLayers, layers);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName()+ "[" 
				+ numberLayers.toString() + ","
				+ layers.toString() + "]";
	}
	
	/**
	 * Конвертирование массива целых чисел в списочный массив целых чисел с задаваемой границей.
	 * 
	 * @param array Массив целых чисел
	 * @return Списочный массив целых чисел
	 */
	public static ArrayList<IntModel> convert(int[] array) {
		ArrayList<IntModel> arrayList = new ArrayList<>();
		
		for (int i = 0; i < array.length; i++)
			arrayList.add(new IntModel(array[i], MIN_NUMBER_NEURONS, MAX_NUMBER_NEURONS));
		
		return  arrayList;
	}
	
	/**
	 * Конвертирование списочного массива целых чисел в списочный массив целых чисел с задаваемой 
	 * границей.
	 * 
	 * @param array Списочный массив целых чисел
	 * @return Списочный массив целых чисел
	 */
	public static ArrayList<IntModel> convert(ArrayList<Integer> array) {
		ArrayList<IntModel> arrayList = new ArrayList<>();
		
		for (int i = 0; i < array.size(); i++)
			arrayList.add(new IntModel(array.get(i), MIN_NUMBER_NEURONS, MAX_NUMBER_NEURONS));
		
		return  arrayList;
	}
	
	/**
	 * Проверка слоев.
	 */
	private void checkLayers() {
		if (numberLayers.getValue() == layers.size()) return;
		
		if (layers.size() > numberLayers.getValue())
			deleteLayers();
		
		if (layers.size() < numberLayers.getValue())
			addLayers();
	}
	
	/**
	 * Добавление слоев до заданного значения количества слоев.
	 */
	private void addLayers() {
		while (layers.size() < numberLayers.getValue())
			layers.add(new IntModel(MIN_NUMBER_NEURONS, MIN_NUMBER_NEURONS, MAX_NUMBER_NEURONS));
	}
	
	/**
	 * Удаление слоев до заданного значения количества слоев.
	 */
	private void deleteLayers() {
		while (layers.size() > numberLayers.getValue())
			layers.remove(layers.size() - 1);
	}
	
	/**
	 * Проверка количества слоев.
	 */
	private void checkNumberLayers() {
		if (numberLayers.getValue() == layers.size()) return;
		
		numberLayers.setValue(layers.size());
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private IntModel numberLayers;
	private ArrayList<IntModel> layers;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущее значение количества слоев.
	 * 
	 * @return Количество слоев
	 */
	public IntModel getNumberLayers() {
		return numberLayers;
	}
	
	/**
	 * Устанавливает новое значение количества слоев.
	 * 
	 * @param numberLayers Количество слоев
	 */
	public void setNumberLayers(int numberLayers) {
		this.numberLayers.setValue(numberLayers);
		
		checkLayers();
	}

	/**
	 * Устанавливает новое значение количества слоев.
	 * 
	 * @param numberLayers Количество слоев
	 */
	public void setNumberLayers(IntModel numberLayers) {
		this.numberLayers = numberLayers;
		
		checkLayers();
	}
	
	/**
	 * Получает текущий списочный массив слоев.
	 * 
	 * @return Списочный массив слоев
	 */
	public ArrayList<IntModel> getLayers() {
		return layers;
	}

	/**
	 * Устанавливает новый списочный массив слоев.
	 * 
	 * @param layers Списочный массив слоев
	 */
	public void setLayers(ArrayList<IntModel> layers) {
		this.layers = layers;
		
		checkNumberLayers();
	}
	
	//</editor-fold>
	
}
