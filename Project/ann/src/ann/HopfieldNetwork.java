/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann;

import ann.model.HopfieldNetworkModel;
import ann.structure.NetworkType;
import java.io.Serializable;
import jhelper.ArrayConverter;

/**
 * Класс сети Хопфилда.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class HopfieldNetwork extends NeuralNetwork implements Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает сеть Хопфилда используя стандартные значения.
	 */
	public HopfieldNetwork() {
        this(new HopfieldNetworkModel());
    }

	/**
	 * Создает сеть Хопфилда используя полученные значения.
	 * 
	 * @param model Модель сети Хопфилда
	 */
    public HopfieldNetwork(HopfieldNetworkModel model) {
        super(NetworkType.HopfieldNetwork, model);
    }
	
	/**
	 * Создает сеть Хопфилда используя полученные значения.
	 * 
	 * @param network Сеть Хопфилда
	 */
	public HopfieldNetwork(HopfieldNetwork network) {
		super(network);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		HopfieldNetwork hopfield = (HopfieldNetwork) super.clone();
		
		hopfield.setW(w.clone());
		
		return hopfield;
	}
	
	/**
	 * Инициализация весовых коэффициентов.
	 *
	 * @param data Данные сети
	 */
	public void initWeights(final int[][] data) {
		final HopfieldNetworkModel model = (HopfieldNetworkModel) getNetworkModel();
		final int count = model.getNumberNeurons().getValue();
		
		w = new int[count][count];
		for (int i = 0; i < count; i++) {
            for (int j = 0; j < count; j++) {
				w[i][j] = 0;
				
                if (i != j) {
                    for (int k = 0; k < data.length; k++)
                        w[i][j] += data[k][i] * data[k][j];
                }
            }
        }
	}
	
	@Override
	public Object[][] calc(Object[][] x) {
		final int[][] inputData = ArrayConverter.objectToInt(x);
		final int[][] outputData = calc(inputData);
		return ArrayConverter.intToObject(outputData);
	}
	
	/**
	 * Выполняет вычисление.
	 *
	 * @param x Входные данные
	 * @return Выходные данные
	 */
	public int[][] calc(final int[][] x) {
		final HopfieldNetworkModel model = (HopfieldNetworkModel) getNetworkModel();
		final int count = model.getNumberNeurons().getValue();
		final int numberType = x.length;
		
		int sum;
		boolean error;
		
		int[] s = new int[count];
		int[][] y = x.clone();
		
		for (int t = 0; t < numberType; t++) {
			error = true;
			
			while(error) {				
				for (int j = 0; j < count; j++) {
                    sum = 0;
                    for (int i = 0; i < count; i++)
                        sum += w[i][j] * y[t][i];
					
                    s[j] = ActivationFunction.step(sum);

                    if (s[j] == y[t][j])
                        error = false;
                    else
                        y[t][j] = s[j];
                }
			}
		}
		
		return y;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private int[][] w;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущие значения весовых коэффициетов.
	 * 
	 * @return Весовые коэффициенты
	 */
	public int[][] getW() {
		return w;
	}
	
	/**
	 * Устанавливает новые значения весовых коэффициентов.
	 * 
	 * @param w Весовые коэффициенты
	 */
	public void setW(int[][] w) {
		this.w = w;
	}
	
	//</editor-fold>

}
