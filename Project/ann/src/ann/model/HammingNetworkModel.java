/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann.model;

import jhelper.model.IntModel;
import java.io.Serializable;

/**
 * Класс модели сети Хэмминга.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class HammingNetworkModel extends NetworkModel 
		implements IHammingNetwork, Cloneable, Serializable {

    //<editor-fold desc="Конструкторы">

	/**
	 * Создает стандартную модель сети Хэмминга.
	 */
    public HammingNetworkModel() {
        this(NUMBER_INPUTS, NUMBER_OUTPUTS);
    }

	/**
	 * Создает модель сети Хэмминга используя полученные значения.
	 *
	 * @param numberInputs Количество входов
	 * @param numberOutputs Количество выходов
	 */
	public HammingNetworkModel(int numberInputs, int numberOutputs) {
        this(new IntModel(numberInputs, MIN_NUMBER_INPUTS, MAX_NUMBER_INPUTS), 
				new IntModel(numberOutputs, MIN_NUMBER_OUTPUTS, MAX_NUMBER_OUTPUTS));
    }
	
	/**
	 * Создает модель сети Хэмминга используя полученные значения.
	 *
	 * @param numberInputs Количество входов
	 * @param numberOutputs Количество выходов
	 */
	public HammingNetworkModel(IntModel numberInputs, IntModel numberOutputs) {
        super(numberInputs, numberOutputs);
    }
	
	/**
	 * Создает модель сети Хэмминга используя полученные значения.
	 *
	 * @param model Модель сети Хэмминга
	 */
	public HammingNetworkModel(HammingNetworkModel model) {
		this(model.getNumberInputs(), model.getNumberOutputs());
	}

    //</editor-fold>
	
}
