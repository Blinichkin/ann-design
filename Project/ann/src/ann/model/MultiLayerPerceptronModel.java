/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann.model;

import jhelper.model.IntModel;
import ann.hiddenlayer.HiddenLayer;
import java.io.Serializable;
import java.util.Objects;
import javax.swing.event.ChangeEvent;

/**
 * Класс модели многослойного персептрона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class MultiLayerPerceptronModel extends NetworkModel 
		implements IMultiLayerPerceptronModel, Cloneable, Serializable {

    //<editor-fold desc="Конструкторы">

	/**
	 * Создает стандартную модель многослойного персептрона.
	 */
    public MultiLayerPerceptronModel() {
        this(NUMBER_INPUTS, NUMBER_OUTPUTS, new HiddenLayer());
    }

	/**
	 * Создает модель многослойного персептрона используя полученные значения.
	 *
	 * @param numberInputs Количество входов
	 * @param numberOutputs Количество выходов
	 * @param hiddenLayer Скрытый слой
	 */
	public MultiLayerPerceptronModel(int numberInputs, int numberOutputs, HiddenLayer hiddenLayer) {
        this(new IntModel(numberInputs, MIN_NUMBER_INPUTS, MAX_NUMBER_INPUTS), 
				new IntModel(numberOutputs, MIN_NUMBER_OUTPUTS, MAX_NUMBER_OUTPUTS), hiddenLayer);
    }
	
	/**
	 * Создает модель многослойного персептрона используя полученные значения.
	 *
	 * @param numberInputs Количество входов
	 * @param numberOutputs Количество выходов
	 * @param hiddenLayer Скрытый слой
	 */
	public MultiLayerPerceptronModel(IntModel numberInputs, IntModel numberOutputs, 
			HiddenLayer hiddenLayer) {
        super(numberInputs, numberOutputs);
        this.hiddenLayer = hiddenLayer;
		
		init();
    }
	
	/**
	 * Создает модель многослойного персептрона используя полученные значения.
	 *
	 * @param model Модель многослойного персептрона
	 */
	public MultiLayerPerceptronModel(MultiLayerPerceptronModel model) {
		this(model.getNumberInputs(), model.getNumberOutputs(), model.getHiddenLayer());
	}

    //</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		createListeners();
	}

	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		hiddenLayer.addChangeListener(this::hiddenLayerStateChanged);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void hiddenLayerStateChanged(ChangeEvent e) {
		fireStateChanged();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		MultiLayerPerceptronModel model = (MultiLayerPerceptronModel) super.clone();
		
		model.setHiddenLayer((HiddenLayer) hiddenLayer.clone());
		
		return model;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!super.equals(object))
			return false;
		
		MultiLayerPerceptronModel model = (MultiLayerPerceptronModel) object;
		
		return Objects.equals(hiddenLayer, model.getHiddenLayer());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), hiddenLayer);
	}

	@Override
	public String toString() {
		return super.toString() + "[" 
				+ hiddenLayer.toString() + "]";
	}
	
	//</editor-fold>

    //<editor-fold desc="Поля">

    private HiddenLayer hiddenLayer;

    //</editor-fold>

    //<editor-fold desc="Доступы">

	/**
	 * Получает текущий скрытый слой.
	 * 
	 * @return Скрытый слой
	 */
    public HiddenLayer getHiddenLayer() {
        return hiddenLayer;
    }

	/**
	 * Устанавливает новый скрытый слой.
	 * 
	 * @param hiddenLayer Скрытый слой
	 */
    public void setHiddenLayer(HiddenLayer hiddenLayer) {
        this.hiddenLayer = hiddenLayer;
		
		fireStateChanged();
    }

    //</editor-fold>

}
