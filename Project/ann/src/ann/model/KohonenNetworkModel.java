/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann.model;

import jhelper.model.IntModel;
import java.io.Serializable;
import java.util.Objects;
import javax.swing.event.ChangeEvent;

/**
 * Класс модели сети Кохонена.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class KohonenNetworkModel extends NetworkModel 
		implements IKohonenNetworkModel, Cloneable, Serializable {
	
    //<editor-fold desc="Конструкторы">

	/**
	 * Создает стандартную модель сети Кохонена.
	 */
    public KohonenNetworkModel() {
        this(NUMBER_INPUTS, NUMBER_NEURONS);
    }

	/**
	 * Создает модель сети Кохонена используя полученные значения.
	 *
	 * @param numberInputs Количество входов
	 * @param numberNeurons Количество нейронов
	 */
	public KohonenNetworkModel(int numberInputs, int numberNeurons) {
        this(new IntModel(numberInputs, MIN_NUMBER_INPUTS, MAX_NUMBER_INPUTS), 
				new IntModel(numberNeurons, MIN_NUMBER_NEURONS, MAX_NUMBER_NEURONS));
    }
	
	/**
	 * Создает модель сети Кохонена используя полученные значения.
	 *
	 * @param numberInputs Количество входов
	 * @param numberNeurons Количество нейронов
	 */
	public KohonenNetworkModel(IntModel numberInputs, IntModel numberNeurons) {
        super(numberInputs, 
				new IntModel(MIN_NUMBER_OUTPUTS, MIN_NUMBER_OUTPUTS, MAX_NUMBER_OUTPUTS));
        this.numberNeurons = numberNeurons;
		
		init();
    }
	
	/**
	 * Создает модель сети Кохонена используя полученные значения.
	 *
	 * @param model Модель сети Кохонена
	 */
	public KohonenNetworkModel(KohonenNetworkModel model) {
		this(model.getNumberInputs(), model.getNumberNeurons());
	}

    //</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		createListeners();
	}

	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		numberNeurons.addChangeListener(this::numberNeuronsStateChanged);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void numberNeuronsStateChanged(ChangeEvent e) {
		fireStateChanged();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">

	@Override
	public Object clone() throws CloneNotSupportedException {
		HopfieldNetworkModel model = (HopfieldNetworkModel) super.clone();
		
		model.setNumberNeurons((IntModel) numberNeurons.clone());
		
		return model;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!super.equals(object)) 
			return false;
		
		KohonenNetworkModel model = (KohonenNetworkModel) object;
		
		return Objects.equals(numberNeurons, model.getNumberNeurons());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), numberNeurons);
	}

	@Override
	public String toString() {
		return super.toString() + "[" 
				+ numberNeurons.toString() + "]";
	}
	
	//</editor-fold>

    //<editor-fold desc="Поля">

    private IntModel numberNeurons;

    //</editor-fold>

    //<editor-fold desc="Доступы">

    /**
	 * Получает текущее значение количества нейронов.
	 *
	 * @return Количество нейронов
	 */
	public IntModel getNumberNeurons() {
        return numberNeurons;
    }

	/**
	 * Устанавливает новое значение количества нейронов.
	 *
	 * @param numberNeurons Количество нейронов
	 */
	public void setNumberNeurons(int numberNeurons) {
        this.numberNeurons.setValue(numberNeurons);
    }
	
	/**
	 * Устанавливает новое значение количества нейронов.
	 *
	 * @param numberNeurons Количество нейронов
	 */
	public void setNumberNeurons(IntModel numberNeurons) {
		this.numberNeurons = numberNeurons;
		
		fireStateChanged();
	}

    //</editor-fold>

}
