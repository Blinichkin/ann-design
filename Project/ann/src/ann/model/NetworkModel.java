/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann.model;

import jhelper.model.IntModel;
import java.io.Serializable;
import java.util.Objects;
import javax.swing.event.ChangeEvent;

/**
 * Класс модели нейронной сети.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public abstract class NetworkModel extends AbstractNetworkModel implements Cloneable, Serializable {
	
    //<editor-fold desc="Конструкторы">

	/**
	 * Создает стандартную модель сети.
	 */
    public NetworkModel() {
        this(new IntModel(), new IntModel());
    }

	/**
	 * Создает модель сети используя полученные значения.
	 *
	 * @param numberInputs Количество входов
	 * @param numberOutputs Количество выходов
	 */
	public NetworkModel(IntModel numberInputs, IntModel numberOutputs) {
        this.numberInputs = numberInputs;
        this.numberOutputs = numberOutputs;
		
		init();
    }
	
	/**
	 * Создает модель сети на основе существующей.
	 *
	 * @param networkModel Модель сети
	 */
	public NetworkModel(NetworkModel networkModel) {
		this(networkModel.getNumberInputs(), networkModel.getNumberOutputs());
	}

    //</editor-fold>
	
	//<editor-fold desc="Инициализация">
	
	/**
	 * Инициализация.
	 */
	private void init() {
		createListeners();
	}

	
	/**
	 * Создание слушателей.
	 */
	private void createListeners() {
		numberInputs.addChangeListener(this::numberInputsStateChanged);
		numberOutputs.addChangeListener(this::numberOutputsStateChanged);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="События">
	
	private void numberInputsStateChanged(ChangeEvent e) {
		fireStateChanged();
	}
	
	private void numberOutputsStateChanged(ChangeEvent e) {
		fireStateChanged();
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	/**
	 * Выполняет полное копирование объекта.
	 * 
	 * @return Копия объекта
	 * @throws java.lang.CloneNotSupportedException Поддержка клонирования
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		NetworkModel networkModel = (NetworkModel) super.clone();
		
		networkModel.setNumberInputs((IntModel) numberInputs.clone());
		networkModel.setNumberOutputs((IntModel) numberOutputs.clone());
		
		return networkModel;
	}
	
	/**
	 * Выполняет полное сравнение объектов.
	 * 
	 * @param object Объект для сравнения
	 * @return Возвращает true, если объекты равны, иначе false
	 */
	@Override
	public boolean equals(Object object) {
		if (this == object) 
			return true;
		
		if (object == null) 
			return false;
		
		if (getClass() != object.getClass()) 
			return false;
		
		NetworkModel networkModel = (NetworkModel) object;
		
		return Objects.equals(numberInputs, networkModel.getNumberInputs())
				&& Objects.equals(numberOutputs, networkModel.getNumberOutputs());
	}
	
	/**
	 * Возвращает хеш-код объекта.
	 * 
	 * @return Хеш-код
	 */
	@Override
	public int hashCode() {
		return Objects.hash(numberInputs, numberOutputs);
	}
	
	/**
	 * Возвращает символьную строку, представляющую значение объекта.
	 * 
	 * @return Значение объекта
	 */
	@Override
	public String toString() {
		return getClass().getName()+ "[" 
				+ numberInputs.toString() + "," 
				+ numberOutputs.toString() + "]";
	}
	
	//</editor-fold>

    //<editor-fold desc="Поля">

    private IntModel numberInputs;
    private IntModel numberOutputs;

    //</editor-fold>

    //<editor-fold desc="Доступы">

	/**
	 * Получает текущее значение количества входов.
	 *
	 * @return Количество входов
	 */
	public IntModel getNumberInputs() {
		return numberInputs;
	}
	
	/**
	 * Устанавливает текущее значение количества входов.
	 *
	 * @param numberInputs Количество входов
	 */
	public void setNumberInputs(int numberInputs) {
		this.numberInputs.setValue(numberInputs);
	}

	/**
	 * Устанавливает текущее значение количества входов.
	 *
	 * @param numberInputs Количество входов
	 */
	public void setNumberInputs(IntModel numberInputs) {
		this.numberInputs = numberInputs;
		fireStateChanged();
	}

	/**
	 * Получает текущее значение количества выходов.
	 *
	 * @return Количество выходов
	 */
	public IntModel getNumberOutputs() {
		return numberOutputs;
	}
	
	/**
	 * Устанавливает текущее значение количества выходов.
	 *
	 * @param numberOutputs Количество выходов
	 */
	public void setNumberOutputs(int numberOutputs) {
		this.numberOutputs.setValue(numberOutputs);
	}

	/**
	 * Устанавливает текущее значение количества выходов.
	 *
	 * @param numberOutputs Количество выходов
	 */
	public void setNumberOutputs(IntModel numberOutputs) {
		this.numberOutputs = numberOutputs;
		fireStateChanged();
	}

    //</editor-fold>

}
