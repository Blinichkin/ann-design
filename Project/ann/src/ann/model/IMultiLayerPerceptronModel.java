/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann.model;

/**
 * Интерфейс модели многослойного персептрона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public interface IMultiLayerPerceptronModel {
	
	//<editor-fold desc="Константы">
	
	/** Стандартное значение количества входов. */
	public static final int NUMBER_INPUTS = 1;
	
	/** Минимальное значение количества входов. */
	public static final int MIN_NUMBER_INPUTS = 1;
	
	/** Максимальное значение количества входов. */
	public static final int MAX_NUMBER_INPUTS = Integer.MAX_VALUE;
	
	/** Стандартное значение количества выходов. */
	public static final int NUMBER_OUTPUTS = 1;
	
	/** Минимальное значение количества выходов. */
	public static final int MIN_NUMBER_OUTPUTS = 1;
	
	/** Максимальное значение количества выходов. */
	public static final int MAX_NUMBER_OUTPUTS = Integer.MAX_VALUE;
	
	//</editor-fold>
	
}
