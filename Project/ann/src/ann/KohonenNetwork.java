/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann;

import ann.model.KohonenNetworkModel;
import ann.structure.NetworkType;
import jhelper.ArrayConverter;

/**
 * Класс сети Кохонена.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class KohonenNetwork extends NeuralNetwork {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает сеть Кохонена используя стандартные значения.
	 */
	public KohonenNetwork() {
		this(new KohonenNetworkModel());
	}
	
	/**
	 * Создает сеть Кохонена используя полученные значения.
	 * 
	 * @param model Модель сети Кохонена
	 */
	public KohonenNetwork(KohonenNetworkModel model) {
		super(NetworkType.KohonenNetwork, model);
	}
	
	/**
	 * Создает сеть Кохонена используя полученные значения.
	 * 
	 * @param network Сеть Кохонена
	 */
	public KohonenNetwork(KohonenNetwork network) {
		super(network);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		KohonenNetwork kohonen = (KohonenNetwork) super.clone();
		
		kohonen.setW(w.clone());
		
		return kohonen;
	}
	
	/**
	 * Инициализация весовых коэффициентов.
	 */
	public void initWeights() {
		final KohonenNetworkModel model = (KohonenNetworkModel) getNetworkModel();
		final int numberInputs = model.getNumberInputs().getValue();
		final int numberNeurons = model.getNumberNeurons().getValue();
		
		w = new double[numberInputs][numberNeurons];
        for (int i = 0; i < numberInputs; i++)
            for (int j = 0; j < numberNeurons; j++)
                w[i][j] = 1.0 / Math.sqrt(numberInputs);
	}
	
	@Override
	public Object[][] calc(Object[][] x) {
		final double[][] inputData = ArrayConverter.objectToDouble(x);
		final int[][] outputData = calc(inputData);
		return ArrayConverter.intToObject(outputData);
	}
	
	/**
	 * Выполняет вычисление.
	 * 
	 * @param x Входные данные
	 * @return Выходные данные
	 */
	public int[][] calc(final int[][] x) {
		final double[][] inputData = ArrayConverter.intToDouble(x);
		final int[][] outputData = calc(inputData);
		return outputData;
	}
	
	/**
	 * Выполняет вычисление.
	 * 
	 * @param x Входные данные
	 * @return Выходные данные
	 */
	public int[][] calc(final double[][] x) {
		final KohonenNetworkModel model = (KohonenNetworkModel) getNetworkModel();
		final int numberInputs = model.getNumberInputs().getValue();
		final int numberOutputs = model.getNumberOutputs().getValue();
		final int numberNeurons = model.getNumberNeurons().getValue();
		final int numberExamples = x.length;
		
		int q;
		double sum = 0;
		double[][] patterns = new double[numberExamples][numberInputs];
		double[] d = new double[numberNeurons];
		int[][] y = new int[numberExamples][numberOutputs];
		
		for (int i = 0; i < numberExamples; i++) {
			sum = 0;
			
			for (int j = 0; j < numberInputs; j++)
				sum += Math.pow(x[i][j], 2);
			
			for (int j = 0; j < numberInputs; j++)
				patterns[i][j] = x[i][j] / Math.sqrt(sum);
		}

		double a = 1;
		for (int kol = 0; kol < 1500; kol++, a -= 1.0 / 1500) {
			for (int t = 0; t < numberExamples; t++) {
				for (int j = 0; j < numberNeurons; j++) {
					sum = 0;
					for (int i = 0; i < numberInputs; i++)
						sum += Math.pow(patterns[t][i] - w[i][j], 2);
					
					d[j] = Math.sqrt(sum);
				}

				if (d[0] < d[1] && d[0] < d[2])
					q = 0;
				else if (d[1] < d[2]) 
					q = 1;
				else 
					q = 2;

				y[t][0] = q + 1;
				for (int i = 0; i < numberInputs; i++)
					w[i][q] += a * (patterns[t][i] - w[i][q]);
			}
		}
		
		return y;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private double[][] w;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущие значения весовых коэффициетов.
	 * 
	 * @return Весовые коэффициенты
	 */
	public double[][] getW() {
		return w;
	}
	
	/**
	 * Устанавливает новые значения весовых коэффициентов.
	 * 
	 * @param w Весовые коэффициенты
	 */
	public void setW(double[][] w) {
		this.w = w;
	}
	
	//</editor-fold>
	
}
