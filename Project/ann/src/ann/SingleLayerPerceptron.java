/*
 * Copyright 2018 Blinichkin Denis Yurievich.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ann;

import ann.model.SingleLayerPerceptronModel;
import ann.structure.NetworkType;
import ann.training.TrainingSettings;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import jhelper.ArrayConverter;

/**
 * Класс однослойного персептрона.
 * 
 * @version 1.00
 * @author Блиничкин Денис Юрьевич
 */
public class SingleLayerPerceptron extends NeuralNetwork implements Cloneable, Serializable {
	
	//<editor-fold desc="Конструкторы">
	
	/**
	 * Создает однойслойный персептрон используя стандартные значения.
	 */
	public SingleLayerPerceptron() {
        this(new SingleLayerPerceptronModel());
    }

	/**
	 * Создает однослойный персептрон используя полученные значения.
	 * 
	 * @param model Модель однослойного персептрона
	 */
    public SingleLayerPerceptron(SingleLayerPerceptronModel model) {
        super(NetworkType.SingleLayerPerceptron, model);
    }
	
	/**
	 * Создает однослойный персептрон используя полученные значения.
	 * 
	 * @param network Однослойный персептрон
	 */
	public SingleLayerPerceptron(SingleLayerPerceptron network) {
		super(network);
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Методы">
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		SingleLayerPerceptron slp = (SingleLayerPerceptron) super.clone();
		
		slp.setW(w.clone());
		
		return slp;
	}
	
	/**
	 * Обучение однослойного персептрона.
	 *
	 * @param x Входные данные
	 * @param y Выходные данные
	 * @param trainingSettings Настройки обучения
	 */
	public void training(final double[][] x, final double[][] y, 
			final TrainingSettings trainingSettings) {
		initWeight();
		settingWeights(x, y, trainingSettings);
	}
	
	/**
	 * Инициализация весовых коэффициентов.
	 */
	private void initWeight() {
		final int numberInputs = getNetworkModel().getNumberInputs().getValue();
		final int numberOutputs = getNetworkModel().getNumberOutputs().getValue();
		
		w = new double[numberInputs][numberOutputs];
		for (int i = 0; i < numberInputs; i++)
			for (int j = 0; j < numberOutputs; j++)
				w[i][j] = getWeight();
	}
	
	/**
	 * Получение случайного значения весового коэффициента [-0.5, 0.5].
	 * 
	 * @return Случайное число
	 */
	private double getWeight() {
		double value = -0.5 + (0.5 - -0.5) * Math.random();
		value = new BigDecimal(value).setScale(16, RoundingMode.HALF_EVEN).doubleValue();
		return value;
	}
	
	/**
	 * Настройка весовых коэффициентов.
	 * 
	 * @param x Входные данные
	 * @param y Выходные данные
	 * @param trainingSettings Настройки обучения
	 */
	private void settingWeights(final double[][] x, final double[][] y, 
			final TrainingSettings trainingSettings) {
		final int numberInputs = getNetworkModel().getNumberInputs().getValue();
		final int numberOutputs = getNetworkModel().getNumberOutputs().getValue();
		final int numberExamples = x.length;
		final int numberEpochs = trainingSettings.getNumberEpochs().getValue();
		final double a = trainingSettings.getA().getValue();
		final double T = 1.0;
		
        int error, result;
        double d, sum;

        for (int e = 0; e < numberEpochs; e++) {
            error = 0;

            for (int i = 0; i < numberExamples; i++) {
                for (int k = 0; k < numberOutputs; k++) {
                    sum = 0;
                    for (int j = 0; j < numberInputs; j++)
                        sum += x[i][j] * w[j][k];
					
					result = ActivationFunction.threshold(T, sum);

                    d = y[i][k] - result;

                    if (d != 0) {
                        error++;
                        for (int j = 0; j < numberInputs; j++)
                            w[j][k] += a * d * x[i][j];
                    }
                }
            }

            if (error == 0) break;
        }
    }
	
	@Override
	public Object[][] calc(final Object[][] x) {
		double[][] inputData = ArrayConverter.objectToDouble(x);
		int[][] outputData = calc(inputData);
		return ArrayConverter.intToObject(outputData);
	}
	
	/**
	 * Выполняет вычисление.
	 * 
	 * @param x Входные данные
	 * @return Выходные данные
	 */
	public int[][] calc(int[][] x) {
		double[][] inputData = ArrayConverter.intToDouble(x);
		return calc(inputData);
	}
	
	/**
	 * Выполняет вычисление.
	 * 
	 * @param x Входные данные
	 * @return Выходные данные
	 */
	public int[][] calc(double[][] x) {
		final int numberInputs = getNetworkModel().getNumberInputs().getValue();
		final int numberOutputs = getNetworkModel().getNumberOutputs().getValue();
		final int numberExamples = x.length;
		final double T = 1.0;
		
		int[][] y = new int[numberExamples][numberOutputs];
		
		double sum;
		for (int i = 0; i < numberExamples; i++) {
			for (int k = 0; k < numberOutputs; k++) {
				sum = 0;
				for (int j = 0; j < numberInputs; j++)
					sum += x[i][j] * w[j][k];

				y[i][k] = ActivationFunction.threshold(T, sum);
			}   
        }
		
		return y;
	}
	
	//</editor-fold>
	
	//<editor-fold desc="Поля">
	
	private double[][] w;
	
	//</editor-fold>

	//<editor-fold desc="Доступы">
	
	/**
	 * Получает текущие значения весовых коэффициетов.
	 * 
	 * @return Весовые коэффициенты
	 */
	public double[][] getW() {
		return w;
	}
	
	/**
	 * Устанавливает новые значения весовых коэффициентов.
	 * 
	 * @param w Весовые коэффициенты
	 */
	public void setW(double[][] w) {
		this.w = w;
	}
	
	//</editor-fold>
	
}
